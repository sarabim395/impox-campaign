export const types = {
  display: {
    large: {
      fontVariationSettings: "'wght' 600",
      fontSize: "64px",
      lineHeight: "80px",
      letterSpacing: "-0.04em",
    },
    medium: {
      fontVariationSettings: "'wght' 500",
      fontSize: "48px",
      lineHeight: "56px",
      letterSpacing: "-0.04em",
    },
    small: {
      fontVariationSettings: "'wght' 600",
      fontSize: "40px",
      lineHeight: "44px",
      letterSpacing: "-0.04em",
    },
  },
  headline: {
    large: {
      fontVariationSettings: "'wght' 400",
      fontSize: "40px",
      lineHeight: "48px",
      letterSpacing: "-0.04em",
    },
    medium: {
      fontVariationSettings: "'wght' 600",
      fontSize: "36px",
      lineHeight: "48px",
      letterSpacing: "-0.04em",
    },
    small: {
      fontVariationSettings: "'wght' 500",
      fontSize: "28px",
      lineHeight: "40px",
      letterSpacing: "-0.04em",
    },
  },
  title: {
    large: {
      fontVariationSettings: "'wght' 700",
      fontSize: "24px",
      lineHeight: "32px",
      letterSpacing: "-0.04em",
    },
    medium: {
      fontVariationSettings: "'wght' 600",
      fontSize: "19px",
      lineHeight: "28px",
      letterSpacing: "-0.04em",
    },
    small: {
      fontVariationSettings: "'wght' 500",
      fontSize: "16px",
      lineHeight: "32px",
      letterSpacing: "-0.04em",
    },
  },
  body: {
    large: {
      fontVariationSettings: "'wght' 400",
      fontSize: "16px",
      lineHeight: "32px",
      letterSpacing: "-0.04em",
    },
    medium: {
      fontVariationSettings: "'wght' 400",
      fontSize: "14px",
      lineHeight: "24px",
      letterSpacing: "-0.04em",
    },
    small: {
      fontVariationSettings: "'wght' 300",
      fontSize: "13px",
      lineHeight: "26px",
      letterSpacing: "-0.04em",
    },
  },
  label: {
    large: {
      fontVariationSettings: "'wght' 600",
      fontSize: "15px",
      lineHeight: "24px",
      letterSpacing: "-0.04em",
    },
    medium: {
      fontVariationSettings: "'wght' 400",
      fontSize: "13px",
      lineHeight: "22px",
      letterSpacing: "-0.04em",
    },
    small: {
      fontVariationSettings: "'wght' 600",
      fontSize: "12px",
      lineHeight: "16px",
      letterSpacing: "-0.04em",
    },
  },
};

export const checkType = (type, size, mobileSize, MobileType) => {
  let TextStyle = types[type][size];
  let MobileStyle = types[MobileType][mobileSize];
  if (typeof window !== "undefined") {
    let w = window.innerWidth;
    if (w > 768) {
      return TextStyle;
    } else {
      return MobileStyle;
    }
  } else {
    return TextStyle;
  }
};
