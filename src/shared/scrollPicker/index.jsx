import styles from './ScrollPicker.module.css';
import 'swiper/css';
import { Swiper, SwiperSlide } from 'swiper/react';
import Typography from '@/shared/Typography'

const ScrollPicker = ({ data, dateHandler, value, slidesPerView = '3', textColor = 'OnBackground' }) => {
  if (data[data.length - 1] !== value) {
    if (data[data.length - 1] == "فروردین") {
      data.push("اردیبهشت")
    } else if (data[data.length - 1] == "اردیبهشت") {
      data.push("خرداد")
    } else if (data[data.length - 1] == "خرداد") {
      data.push("تیر")
    } else if (data[data.length - 1] == "تیر") {
      data.push("مرداد")
    } else if (data[data.length - 1] == "مرداد") {
      data.push("شهریور")
    } else if (data[data.length - 1] == "شهریور") {
      data.push("مهر")
    } else if (data[data.length - 1] == "آبان") {
      data.push("آذر")
    } else if (data[data.length - 1] == "آذر") {
      data.push("دی")
    } else if (data[data.length - 1] == "دی") {
      data.push("بهمن")
    } else if (data[data.length - 1] == "بهمن") {
      data.push("اسفند")
    }
  }

  const initialSlide = data?.indexOf(`${value}`);

  return (
    <div className={styles.main} style={{ height: slidesPerView === '3' ? '112px' : '180px' }}>
      <Swiper
        slidesPerView={slidesPerView}
        direction="vertical"
        centeredSlides={true}
        initialSlide={initialSlide}
        onSlideChange={(e) => dateHandler(data[e.realIndex])}
        onSwiper={(e) => dateHandler(data[e.realIndex])}
      >
        {data?.map((item, index) => {
          return (
            <SwiperSlide key={index}>
              <div>
                <Typography>
                  {item}
                </Typography>
              </div>
            </SwiperSlide>
          )
        })}
      </Swiper>
    </div>
  );
};

export default ScrollPicker;
