import { useEffect, useRef, useState } from 'react';
import { toFarsiNumber, convertNumbers2English } from '@/utils'

import styles from './CustomInput.module.css';
import TextField from '@mui/material/TextField';

import { useTheme } from '@emotion/react';


const CustomInput = ({
  value = '',
  name,
  placeholder = '',
  textType = 'body',
  textSize = 'small',
  align = 'right',
  autoFocus = false,
  textColor,
  borderColor,
  borderRadius,
  backgroundColor = 'Background',
  hasError,
  errorMessage,
  errorMessageFontSize = '12px',
  maxLength,
  padding,
  margin,
  disabled = false,
  className,
  onChangeValue,
  ...rest
}) => {
  const theme = useTheme();

  const [inputValue, setInputValue] = useState({ value: value, selectionStart: 0, selectionEnd: 0 });

  const valueHandler = (e) => {
    console.log(e)
    onChangeValue(convertNumbers2English(e.target.value));
    setInputValue({
      value: toFarsiNumber(e.target.value),
      selectionStart: e.target.selectionStart,
      selectionEnd: e.target.selectionEnd,
    });
  };

  const ref = useRef(null);
  useEffect(() => {
    ref.current.selectionStart = 2;

    ref.current.childNodes[0].childNodes[0].childNodes[0].setSelectionRange(
      inputValue.selectionStart,
      inputValue.selectionEnd,
    );
  }, [inputValue]);

  return (
    <div className={styles.main} style={{ margin }} ref={ref}>
      <TextField
        name={name}
        maxRows={5}
        fullWidth
        className={className}
        disabled={disabled}
        value={inputValue.value}
        onChange={valueHandler}
        placeholder={placeholder}
        autoFocus={autoFocus}
        dir={align === 'right' ? 'rtl' : 'ltr'}
        {...rest}
        inputProps={{ maxLength }}
        sx={{
          '& fieldSet': {
            borderStyle: 'solid',
            borderWidth: '1px',
            borderColor: theme.palette.colors[borderColor],
            borderRadius,
          },
          '& input': {
            backgroundColor: theme.palette.colors[backgroundColor],
            color: theme.palette.colors[textColor],
            caretColor: '#EC407A',
            padding,
          },
          '& input:-webkit-autofill': {
            borderRadius,
            WebkitBoxShadow: `0 0 0 1000px ${theme.palette.colors[backgroundColor]} inset`,
          },
        }}
      />

      {hasError && (
        <span
          className={styles.errorMessage}
          style={{ fontSize: errorMessageFontSize, color: theme.palette.colors.Primary }}
        >
          {errorMessage}
        </span>
      )}
    </div>
  );
};

export default CustomInput;
