import { Typography as MuiTypography } from '@mui/material';
import { toFarsiNumber } from '@/utils/index'
import { checkType } from '../types';
import { CheckColor } from '@/themes/color'
import { useMemo } from 'react';


const Typography = ({
    children,
    color,
    colorType = 'primary' || 'secondary' || 'error' || 'neutral' || 'neutral_variants',
    type = "body" || 'headline' || 'display' || 'label' || 'title',
    size = 'large' || 'medium' || 'small',
    farsi = true,
    MobileType = "body" || 'headline' || 'display' || 'label' || 'title',
    mobileSize = 'large' || 'medium' || 'small',
    ...rest
}) => {
    const isColorValid = (color) => {
        return (
            color === 'main' ||
            color === 'OnPrimary' ||
            color === 'PrimaryContainer' ||
            color === 'OnSecondary' ||
            color === 'SecondaryContainer' ||
            color === 'OnSecondaryContainer' ||
            color === 'OnError' ||
            color === 'ErrorContainer' ||
            color === 'OnErrorContainer' ||
            color === 'OnBackground' ||
            color === 'Surface' ||
            color === 'OnSurface' ||
            color === 'SurfaceVariant' ||
            color === 'OnSurfaceVariant' ||
            color === 'Outline'
        );
    };
    const TextColor = useMemo(() => (isColorValid(color) ? CheckColor(colorType, color) : color), [color]);
    return (
        <MuiTypography style={checkType(type, size, mobileSize, MobileType)} color={TextColor} {...rest} >{farsi == true ? toFarsiNumber(children) : children}</MuiTypography>
    )
}

export default Typography   