import { createTheme } from '@mui/material/styles';
import { faIR } from '@mui/material/locale';
import palette from './palette';
import darkpallete from './palette-dark';
import typography from './typography';
import shadows from './shadows';
import shape from './shape';
import overrides from './overrides';

export const lightTheme = createTheme(
    {
        direction: 'rtl',
        shape,
        overrides,
        palette,
        shadows,
        typography,
        props: {
            MuiTextField: {
                InputLabelProps: {
                    shrink: true,
                },
            },
            MuiAutocomplete: {
                disablePortal: true,
            },
        },
        components: {
            MuiButton: {
                defaultProps: {
                    variant: "contained"
                },
                styleOverrides: {
                    sizeSmall: {
                        fontSize: 14,
                        padding: "8px 16px",
                        fontVariationSettings: "'wght' 400",
                        letterSpacing: '-0.04em'
                    },
                    sizeMedium: {
                        padding: "8px 24px",
                        fontSize: 16,
                        fontVariationSettings: "'wght' 600",
                        letterSpacing: '-0.04em'
                    },
                    sizeLarge: {
                        padding: "8px 24px",
                        fontSize: 24,
                        fontVariationSettings: "'wght' 700",
                        letterSpacing: '-0.04em'
                    },
                },
                variants: [
                    {
                        props: { variant: "primary" },
                        style: {
                            background: palette.colors.Primary_Container,
                            color: palette.colors.On_Primary,
                            "&:hover": {
                                background: '#B80F55',
                                color: palette.colors.On_Primary,
                            },
                            "&:focus": {
                                background: '#8F003F',
                                color: palette.colors.On_Primary,
                            },
                            "&:active": {
                                background: '#66002B',
                                color: palette.colors.On_Primary,
                            },
                            "&:disabled": {
                                background: '#D9D9D9',
                                color: palette.colors.Background,
                            }
                        }
                    },
                    {
                        props: { variant: "surface" },
                        style: {
                            background: palette.colors.Surface,
                            color: palette.colors.On_Surface,
                            "&:hover": {
                                background: '#E7E0EB',
                                color: palette.colors.On_Surface,
                            },
                            "&:focus": {
                                background: '#CAC4CF',
                                color: palette.colors.OnBackground,
                            },
                            "&:active": {
                                background: '#AFA9B4',
                                color: palette.colors.Background,
                            },
                            "&:disabled": {
                                background: '#D9D9D9',
                                color: palette.colors.Background,
                            }
                        }
                    },
                    {
                        props: { variant: "error" },
                        style: {
                            background: palette.colors.Error,
                            color: palette.colors.OnError,
                            "&:hover": {
                                background: '#DE3730',
                                color: palette.colors.OnError,
                            },
                            "&:focus": {
                                background: '#93000A',
                                color: palette.colors.OnError,
                            },
                            "&:active": {
                                background: '#690005',
                                color: palette.colors.OnError,
                            },
                            "&:disabled": {
                                background: '#FFDAD6',
                                color: palette.colors.OnError,
                            }
                        }
                    },
                    {
                        props: { variant: "base" },
                        style: {
                            background: palette.colors.OnBackground,
                            color: palette.colors.Background,
                            "&:hover": {
                                background: '#313033',
                                color: palette.colors.Background,
                            },
                            "&:focus": {
                                background: '#48464A',
                                color: palette.colors.Background,
                            },
                            "&:active": {
                                background: '#79767A',
                                color: palette.colors.Background,
                            },
                            "&:disabled": {
                                background: '#D9D9D9',
                                color: palette.colors.Background,
                            }
                        }
                    },
                ]
            },
        }
    },
    faIR
);
export const DarkTheme = createTheme(
    {
        direction: 'rtl',
        shape,
        overrides,
        palette: darkpallete,
        shadows,
        typography,
        props: {
            MuiTextField: {
                InputLabelProps: {
                    shrink: true,
                },
            },
            MuiAutocomplete: {
                disablePortal: true,

            },
        },
        components: {
            MuiButton: {
                defaultProps: {
                    variant: "contained"
                },
                styleOverrides: {
                    sizeSmall: {
                        fontSize: 14,
                        padding: "8px 16px",
                        fontVariationSettings: "'wght' 400",
                        letterSpacing: '-0.04em'

                    },
                    sizeMedium: {
                        padding: "8px 24px",
                        fontSize: 16,
                        fontVariationSettings: "'wght' 600",
                        letterSpacing: '-0.04em'
                    },
                    sizeLarge: {
                        padding: "8px 24px",
                        fontSize: 24,
                        fontVariationSettings: "'wght' 700",
                        letterSpacing: '-0.04em'
                    },
                },
                variants: [
                    {
                        props: { variant: "primary" },
                        style: {
                            background: palette.colors.Primary_Container,
                            color: palette.colors.On_Primary,
                            "&:hover": {
                                background: '#FD4E87',
                                color: palette.colors.On_Primary,
                            },
                            "&:focus": {
                                background: '#FF85A4',
                                color: palette.colors.On_Primary_Container,
                            },
                            "&:active": {
                                background: '#FFB1C2',
                                color: palette.colors.Surface,
                            },
                            "&:disabled": {
                                background: '#313033',
                                color: '#48464A',
                            }
                        }
                    },
                    {
                        props: { variant: "surface" },
                        style: {
                            background: palette.colors.Surface,
                            color: palette.colors.OnBackground,
                            "&:hover": {
                                background: '#313033',
                                color: palette.colors.OnBackground,
                            },
                            "&:focus": {
                                background: '#48464A',
                                color: palette.colors.OnBackground,
                            },
                            "&:active": {
                                background: '#605D62',
                                color: palette.colors.OnBackground,
                            },
                            "&:disabled": {
                                background: '#313033',
                                color: palette.colors.Background,
                            }
                        }
                    },
                    {
                        props: { variant: "error" },
                        style: {
                            background: palette.colors.Error,
                            color: palette.colors.OnError,
                            "&:hover": {
                                background: '#FF897D',
                                color: palette.colors.OnError,
                            },
                            "&:focus": {
                                background: '#DE3730',
                                color: palette.colors.OnError,
                            },
                            "&:active": {
                                background: '#BA1A1A',
                                color: palette.colors.On_Error_Container,
                            },
                            "&:disabled": {
                                background: '#410002',
                                color: palette.colors.Background,
                            }
                        }
                    },
                    {
                        props: { variant: "base" },
                        style: {
                            background: palette.colors.OnBackground,
                            color: palette.colors.Background,
                            "&:hover": {
                                background: '#E6E1E6',
                                color: palette.colors.Background,
                            },
                            "&:focus": {
                                background: '#AEAAAE',
                                color: palette.colors.Background,
                            },
                            "&:active": {
                                background: '#48464A',
                                color: palette.colors.OnBackground,
                            },
                            "&:disabled": {
                                background: '#313033',
                                color: palette.colors.Background,
                            }
                        }
                    },
                ]
            }
        }
    },
    faIR
);

