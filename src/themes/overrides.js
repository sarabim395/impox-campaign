import palette from './palette';

const overrides = {
  MuiCssBaseline: {
    '@global': {
      a: {
        textDecoration: 'none',
        color: palette.primary.main.color,
      },
      code: {
        color: palette.primary.main.color,
      },
      '::-webkit-scrollbar': {
        width: '8px',
        height: '8px',
      },

    },
  },
  MuiTextField: {
    root: {
      '& .MuiOutlinedInput-root': {

        '& fieldset': {
          borderRadius: 100,
        },
      },
    },
  },
  MuiButton: {
    contained: {
      backgroundColor: palette.primary.main,
      color: palette.primary.contrastText,
      "&:hover": {
        backgroundColor: palette.primary.hover,
      },
      "&:focused": {
        backgroundColor: palette.primary.focused
      },
      "&:selected": {
        backgroundColor:  palette.primary.press
      },
    },
    outlined: {
      color: palette.primary.main.color,
      "&:hover": {
        backgroundColor: palette.primary.main.backgroundColor,
      },
    },
  },
  MuiSlider:{
    backgroundColor:'green'
  }	
};

export default overrides;
