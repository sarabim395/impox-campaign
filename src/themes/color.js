export const TonalPalletes = {
    primary: {
        0: "#000000",
        10: "#3F0018",
        20: "#66002B",
        30: "#8F003F",
        40: "#B80F55",
        50: "#DB326E",
        60: "#FD4E87",
        70: "#FF85A4",
        80: "#FFB1C2",
        90: "#FFD9DF",
        95: "#FFECEE",
        99: "#FFFBFF",
        100: "#FFFFFF"
    },
    secondary: {
        0: "#000000",
        10: "#261A00",
        20: "#3F2E00",
        30: "#5B4300",
        40: "#795900",
        50: "#977100",
        60: "#B78A00",
        70: "#D8A300",
        80: "#F9BD10",
        90: "#FFEFD4",
        95: "#FFEEDE",
        99: "#FFFBFF",
        100: "#FFFFFF"
    },
    error: {
        0: "#000000",
        10: "#410002",
        20: "#690005",
        30: "#93000A",
        40: "#BA1A1A",
        50: "#DE3730",
        60: "#FF5449",
        70: "#FF897D",
        80: "#FFB4AB",
        90: "#FFDAD6",
        95: "#FFEDEA",
        99: "#FFFBFF",
        100: "#FFFFFF"
    },
    neutral: {
        0: "#000000",
        10: "#1C1B1E",
        20: "#313033",
        30: "#48464A",
        40: "#605D62",
        50: "#79767A",
        60: "#938F94",
        70: "#AEAAAE",
        80: "#CAC5CA",
        90: "#E6E1E6",
        95: "#F4EFF4",
        99: "#FFFBFF",
        100: "#FFFFFF"
    },
    neutral_variants: {
        0: "#000000",
        10: "#1D1A22",
        20: "#322F38",
        30: "#49454E",
        40: "#615D66",
        50: "#7A757F",
        60: "#948F99",
        70: "#AFA9B4",
        80: "#CAC4CF",
        90: "#E7E0EB",
        95: "#F5EEFA",
        99: "#FFFBFF",
        100: "#FFFFFF"
    }

};
export const Light = {
    primary: {
        main: {
            backgroundColor: TonalPalletes.primary[40],
            color: TonalPalletes.primary[60]
        },
        OnPrimary: {
            backgroundColor: TonalPalletes.primary[80],
            color: TonalPalletes.primary[0]
        },
        PrimaryContainer: {
            backgroundColor: TonalPalletes.primary[90],
            color: TonalPalletes.primary[10]
        },
        OnPrimaryContainer: {
            backgroundColor: TonalPalletes.primary[10],
            color: TonalPalletes.primary[90]
        }
    },
    secondary: {
        main: {
            backgroundColor: TonalPalletes.secondary[40],
            color: TonalPalletes.secondary[60]
        },
        OnSecondary: {
            backgroundColor: TonalPalletes.secondary[100],
            color: TonalPalletes.secondary[0]
        },
        SecondaryContainer: {
            backgroundColor: TonalPalletes.secondary[90],
            color: TonalPalletes.secondary[10]
        },
        OnSecondaryContainer: {
            backgroundColor: TonalPalletes.secondary[10],
            color: TonalPalletes.secondary[90]
        }
    },
    error: {
        main: {
            backgroundColor: TonalPalletes.error[40],
            color: TonalPalletes.error[60]
        },
        OnError: {
            backgroundColor: TonalPalletes.error[100],
            color: TonalPalletes.error[0]
        },
        ErrorContainer: {
            backgroundColor: TonalPalletes.error[90],
            color: TonalPalletes.error[10]
        },
        OnErrorContainer: {
            backgroundColor: TonalPalletes.error[10],
            color: TonalPalletes.error[90]
        }
    },
    neutral: {
        main: {
            backgroundColor: TonalPalletes.neutral[99],
            color: TonalPalletes.neutral[10]
        },
        OnBackground: {
            backgroundColor: TonalPalletes.neutral[10],
            color: TonalPalletes.neutral[90]
        },
        Surface: {
            backgroundColor: TonalPalletes.neutral[99],
            color: TonalPalletes.neutral[10]
        },
        OnSurface: {
            backgroundColor: TonalPalletes.neutral[10],
            color: TonalPalletes.neutral[90]
        }
    },
    neutral_variants: {
        SurfaceVariant: {
            backgroundColor: TonalPalletes.neutral_variants[90],
            color: TonalPalletes.neutral_variants[10]
        },
        OnSurfaceVariant: {
            backgroundColor: TonalPalletes.neutral_variants[30],
            color: TonalPalletes.neutral_variants[70]
        },
        Outline: {
            backgroundColor: TonalPalletes.neutral_variants[50],
            color: TonalPalletes.neutral_variants[50]
        },

    }
}
export const Dark = {
    primary: {
        main: {
            backgroundColor: TonalPalletes.primary[80],
            color: TonalPalletes.primary[20]
        },
        OnPrimary: {
            backgroundColor: TonalPalletes.primary[20],
            color: TonalPalletes.primary[80]
        },
        PrimaryContainer: {
            backgroundColor: TonalPalletes.primary[30],
            color: TonalPalletes.primary[70]
        },
        OnPrimaryContainer: {
            backgroundColor: TonalPalletes.primary[90],
            color: TonalPalletes.primary[10]
        }
    },
    secondary: {
        main: {
            backgroundColor: TonalPalletes.secondary[80],
            color: TonalPalletes.secondary[20]
        },
        OnSecondary: {
            backgroundColor: TonalPalletes.secondary[20],
            color: TonalPalletes.secondary[80]
        },
        SecondaryContainer: {
            backgroundColor: TonalPalletes.secondary[30],
            color: TonalPalletes.secondary[70]
        },
        OnSecondaryContainer: {
            backgroundColor: TonalPalletes.secondary[90],
            color: TonalPalletes.secondary[10]
        }
    },
    error: {
        main: {
            backgroundColor: TonalPalletes.error[80],
            color: TonalPalletes.error[20]
        },
        OnError: {
            backgroundColor: TonalPalletes.error[20],
            color: TonalPalletes.error[80]
        },
        ErrorContainer: {
            backgroundColor: TonalPalletes.error[30],
            color: TonalPalletes.error[70]
        },
        OnErrorContainer: {
            backgroundColor: TonalPalletes.error[90],
            color: TonalPalletes.error[10]
        }
    },
    neutral: {
        Background: {
            backgroundColor: TonalPalletes.neutral[10],
            color: TonalPalletes.neutral[90]
        },
        OnBackground: {
            backgroundColor: TonalPalletes.neutral[90],
            color: TonalPalletes.neutral[10]
        },
        Surface: {
            backgroundColor: TonalPalletes.neutral[10],
            color: TonalPalletes.neutral[90]
        },
        OnSurface: {
            backgroundColor: TonalPalletes.neutral[80],
            color: TonalPalletes.neutral[20]
        }
    },
    neutral_variants: {
        SurfaceVariant: {
            backgroundColor: TonalPalletes.neutral_variants[30],
            color: TonalPalletes.neutral_variants[70]
        },
        OnSurfaceVariant: {
            backgroundColor: TonalPalletes.neutral_variants[80],
            color: TonalPalletes.neutral_variants[20]
        },
        Outline: {
            backgroundColor: TonalPalletes.neutral_variants[60],
            color: TonalPalletes.neutral_variants[40]
        },

    }
}

export const CheckColor = (type, color) => {
    let TextColor = Light[type]?.[color]
    return TextColor?.backgroundColor
}