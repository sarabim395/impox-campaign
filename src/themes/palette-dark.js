export const TonalPalletes = {
  primary: {
    0: "#000000",
    10: "#00026C",
    20: "#0004AA",
    30: "#0D16E3",
    40: "#3540F9",
    50: "#5964FF",
    60: "#7B85FF",
    70: "#9DA4FF",
    80: "#BEC2FF",
    90: "#E0E0FF",
    95: "#F1EFFF",
    99: "#FFFBFF",
    100: "#FFFFFF"
  },
  secondary: {
    0: "#000000",
    10: "#2A1700",
    20: "#462A00",
    30: "#653E00",
    40: "#855400",
    50: "#A66A00",
    60: "#C98100",
    70: "#ED9900",
    80: "#FFB95D",
    90: "#FFDDB7",
    95: "#FFEEDE",
    99: "#FFFBFF",
    100: "#FFFFFF"
  },
  error: {
    0: "#000000",
    10: "#410002",
    20: "#690005",
    30: "#93000A",
    40: "#BA1A1A",
    50: "#DE3730",
    60: "#FF5449",
    70: "#FF897D",
    80: "#FFB4AB",
    90: "#FFDAD6",
    95: "#FFEDEA",
    99: "#FFFBFF",
    100: "#FFFFFF"
  },
  neutral: {
    0: "#000000",
    10: "#1B1B1F",
    20: "#303034",
    30: "#47464A",
    40: "#5F5E62",
    50: "#78767A",
    60: "#929094",
    70: "#ADAAAF",
    80: "#C8C5CA",
    90: "#E5E1E6",
    95: "#F3EFF4",
    99: "#FFFBFF",
    100: "#FFFFFF"
  },
  neutral_variants: {
    0: "#000000",
    10: "#1B1B23",
    20: "#303038",
    30: "#46464F",
    40: "#5E5D67",
    50: "#777680",
    60: "#91909A",
    70: "#ACAAB4",
    80: "#C7C5D0",
    90: "#E3E1EC",
    95: "#F2EFFA",
    99: "#FFFBFF",
    100: "#FFFFFF"
  }

};
const pallete = {
  mode: 'dark',
  Background: '#1B1B23',

  primary: {
    main: TonalPalletes.primary[80],
    OnPrimary: {
      backgroundColor: TonalPalletes.primary[20],
      color: TonalPalletes.primary[80]
    },
    PrimaryContainer: {
      backgroundColor: TonalPalletes.primary[30],
      color: TonalPalletes.primary[70]
    },
    OnPrimaryContainer: {
      backgroundColor: TonalPalletes.primary[90],
      color: TonalPalletes.primary[10]
    }
  },
  secondary: {
    main: TonalPalletes.secondary[80],
    OnSecondary: {
      backgroundColor: TonalPalletes.secondary[20],
      color: TonalPalletes.secondary[80]
    },
    SecondaryContainer: {
      backgroundColor: TonalPalletes.secondary[30],
      color: TonalPalletes.secondary[70]
    },
    OnSecondaryContainer: {
      backgroundColor: TonalPalletes.secondary[90],
      color: TonalPalletes.secondary[10]
    }
  },
  error: {
    main: TonalPalletes.error[80],
    OnError: {
      backgroundColor: TonalPalletes.error[20],
      color: TonalPalletes.error[80]
    },
    ErrorContainer: {
      backgroundColor: TonalPalletes.error[30],
      color: TonalPalletes.error[70]
    },
    OnErrorContainer: {
      backgroundColor: TonalPalletes.error[90],
      color: TonalPalletes.error[10]
    }
  },
  neutral: {
    Background: {
      backgroundColor: TonalPalletes.neutral[10],
      color: TonalPalletes.neutral[90]
    },
    OnBackground: {
      backgroundColor: TonalPalletes.neutral[90],
      color: TonalPalletes.neutral[10]
    },
    Surface: {
      backgroundColor: TonalPalletes.neutral[10],
      color: TonalPalletes.neutral[90]
    },
    OnSurface: {
      backgroundColor: TonalPalletes.neutral[80],
      color: TonalPalletes.neutral[20]
    }
  },
  neutral_variants: {
    SurfaceVariant: {
      backgroundColor: TonalPalletes.neutral_variants[30],
      color: TonalPalletes.neutral_variants[70]
    },
    OnSurfaceVariant: {
      backgroundColor: TonalPalletes.neutral_variants[80],
      color: TonalPalletes.neutral_variants[20]
    },
    Outline: {
      backgroundColor: TonalPalletes.neutral_variants[60],
      color: TonalPalletes.neutral_variants[40]
    },

  }
}

export default pallete;
