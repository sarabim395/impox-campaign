import { colors } from './LightColors'
const palette = {
  mode: 'light',
  primary: {
    main: '#ec407a',
  },
  secondary: {
    main: '#efefef',
  },
  info: {
    main: '#1c1b1e',
  },
  error: {
    main: '#ba1a1a',
  },
  colors
}

export default palette;

