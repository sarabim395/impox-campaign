import Typography from '@/shared/Typography'
import Button from '@mui/material/Button'
import { Hidden } from '@mui/material';
import { Swiper, SwiperSlide } from 'swiper/react';
import "swiper/css/pagination";
import 'swiper/css';
import styles from './article.module.css';
import Grid from '@mui/material/Grid';
import Image from 'next/image';
import axios, { PICURL } from '@/utils/axios';
import { useState, useEffect } from "react";
import Link from "next/link";
import CloudDownload from '@mui/icons-material/CloudDownload';
function ArticleList({ men }) {
    const [articleList, setArticleList] = useState([]);
    const fetchArticleList = async () => {
        var menId = "";
        if (men) {
            const catList = await axios.get('article/category/1/10')
            var categoryList = catList?.data.categories;
            categoryList.forEach(item => {
                if (item.title == "سلامت آقایان") {
                    menId = item.id;
                }
            })
        }
        const response = await axios.get(`article/articles/${men ? `category/${menId}/1/10` : "1/10"}`)
        if (response?.data?.articles) {
            const articles = response.data.articles?.slice(0, window.innerWidth <= 1440 ? 3 : 4)
            setArticleList(articles)
        } else {
            const section = document?.getElementById("articlesMen");
            section.style.display = "none";
        }
    }

    useEffect(() => {
        fetchArticleList()
    }, [])
    return (
        <>
            <div className={styles.articles}>
                <Hidden mdDown>
                    <div className={styles.articleList}>
                        <Grid container spacing={3} className={styles.articleContainer}>
                            {articleList ? articleList?.map((item, index) => {
                                return (
                                    <Grid item md={3} xs={12} key={index}>
                                        <div className={styles.article}>
                                            <Link href={item.url} aria-label="link">
                                                <Image src={`${PICURL}${item.imageCover}`} unoptimized="false" width="100" height="100" alt={item.meta} />
                                            </Link>
                                            <div className={styles.details}>
                                                {item.categories?.length ? item.categories.map((cat, index) => {
                                                    return (
                                                        <span key={index} className={styles.catName}><Link href={item.canonicalLink} aria-label="link">{cat.title}</Link></span>
                                                    )
                                                }) : <span className={styles.catName}>تعریف نشده</span>
                                                }
                                                <Link href={item.url} aria-label="link">
                                                    <Typography component="h2" type="title" size="large" colorType="neutral" color="OnBackground" className={styles.titleArticle} >
                                                        {item.title}
                                                    </Typography>
                                                </Link>
                                                <Button variant="text" size="small" aria-label="btnName" className={styles.moreInfo}><Link href={item.url} aria-label="link">مطالعه مقاله</Link></Button>
                                            </div>
                                        </div>
                                    </Grid>
                                )
                            }) : <p className={styles.noResult}>مقاله ای یافت نشد</p>}
                        </Grid>
                    </div>
                </Hidden>
                <Hidden mdUp>
                    <Swiper spaceBetween={30} slidesPerView={"auto"} className="swiperBlog">
                        {articleList ? articleList?.map((item, index) => {
                            return (
                                <SwiperSlide key={index}>
                                    <div className={styles.articleList}>
                                        <div className={styles.article}>
                                            <Link href={item.url} aria-label="link">
                                                <Image src={`${PICURL}${item.imageCover}`} unoptimized="false" width="100" height="100" alt={item.meta} />
                                            </Link>
                                            <div className={styles.details}>
                                                {item.categories.map((cat, i) => {
                                                    return (
                                                        <span key={i} className={styles.catName}><Link aria-label="link" href={item.canonicalLink}>{cat.title}</Link></span>
                                                    )
                                                })
                                                }
                                                <Link href={item.url} aria-label="link">
                                                    <Typography component="h2" type="title" size="large" colorType="neutral" color="OnBackground" className={styles.titleArticle} >
                                                        {item.title}
                                                    </Typography>
                                                </Link>
                                                <Button variant="text" size="small" aria-label="btnName" className={styles.moreInfo}><Link href={item.url} aria-label="link">مطالعه مقاله</Link></Button>
                                            </div>
                                        </div>
                                    </div>
                                </SwiperSlide>
                            )
                        }) : <p className={styles.noResult}>مقاله ای یافت نشد</p>}
                    </Swiper>
                </Hidden>
                <div className={styles.btnBox} style={{ textAlign: "center" }}>
                    <Button className={styles.seeAllBlog} aria-label="btnName" size="medium" variant="primary" classes={{ startIcon: styles.startIcon }} startIcon={<CloudDownload />}>
                        <Link href="/blogs" aria-label="link">
                            مشاهده همه مقالات
                        </Link>
                    </Button>
                </div>
            </div>
        </>
    )
}

export default ArticleList