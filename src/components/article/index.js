import styles from "./article.module.css";
import Image from "next/image";
import Typography from "../../shared/Typography";
import { Grid, Divider, Hidden } from "@mui/material";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";
import { PICURL } from '@/utils/axios';
import { toPersianDate, excerptText } from "@/utils";
import Link from "next/link";
import readingTime from "reading-time";
import { v4 as uuidv4 } from 'uuid';
import { useRouter } from 'next/router';
import userImg from '@/assets/images/user.svg.png';

export default function Article(props) {
  const router = useRouter();
  const redirectAuthor = (item) => {
    localStorage?.removeItem("authorInfo")
    localStorage?.setItem("authorInfo", JSON.stringify(item?.author))
    router.push(`/author/${item?.author?.authorName}`);
  }
  let getArticleContainer = props.ArticleContainer;

  return getArticleContainer.length ? getArticleContainer.map((item) => {
    const readingTimeText = readingTime(item?.body)
    return (
      <div key={uuidv4()}>
        <div className={styles.frameArticle}>
          <Hidden mdDown>
            <Grid container spacing={2}>
              <Grid item md={3}>
                <div className={styles.imageArticle}>
                  <Link href={"/" + item.url} aria-label="url">
                    <Image src={`${PICURL}${item.imageCover}`} className={styles.widthImageArticle} unoptimized="true" width="100" height="100" sizes="(max-width: 768px) auto, (max-width: 1200px) auto, auto" alt={item.meta} />
                  </Link>
                </div>
              </Grid>
              <Grid item md={9} >
                <div className={styles.flexMobileSizeArticle}>
                  <Link href={"/" + item.url} aria-label="url">
                    <Typography type="headline" size="medium" className={styles.titleStyle}>
                      {item.title}
                    </Typography>
                  </Link>
                  <div>
                    <Typography type="body" size="large" className={styles.textArticle}>
                      {excerptText(item.body?.replace(/<[^>]+>/g, ''), 250)}
                    </Typography>
                  </div>
                  <div>
                    <div className={styles.flexlikeDoctorName}>
                      <div className={styles.flexDoctorName} onClick={() => { redirectAuthor(item) }}>
                        {item.author.authorPic ?
                          <Image src={`${PICURL}${item.author.authorPic}`} className={styles.widthDoctorImage} unoptimized="true" width="100" height="100" sizes="(max-width: 768px) auto, (max-width: 1200px) auto, auto" alt={item.meta} />
                          :
                          <Image src={userImg} alt="user" unoptimized="true" width="40" height="40" />
                        }
                        <Typography
                          type="label"
                          size="large"
                          className={styles.minwidthArticle}
                        >
                          {item.author.authorName}
                        </Typography>
                      </div>
                      <div className={styles.flexlikeComment}>
                        <Typography
                          type="label"
                          size="large"
                          className={styles.commentStyleArticle}
                        >
                          {toPersianDate(item.publishTime, { month: 'long', day: 'numeric' })}
                        </Typography>
                        <Typography
                          type="label"
                          size="large"
                          className={styles.commentStyleArticle}
                        >
                          {`${Math.round((readingTimeText.time) / 60000)} دقیقه برای مطالعه `}
                        </Typography>
                        <div className={styles.likeCommentDiv}><ChatBubbleOutlineIcon
                          className={styles.commentStyleArticle} />
                          <span style={{ marginRight: "5px" }}>
                            <Typography
                              type="label"
                              size="large"
                              className={styles.commentStyleArticle}
                            >
                              {item.commentCount}
                            </Typography>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Grid>
            </Grid>
          </Hidden>
          <Hidden mdUp>
            <div className={styles.frameArticleMobile} >
              <Grid container justifyContent={"space-between"}>
                <Grid item sm={9} xs={8}>
                  <Link href={"/" + item.url} aria-label="url">
                    <Typography type="title" size="large" className={styles.styledescriptionArticle}>
                      {item.title}
                    </Typography>
                  </Link>
                </Grid>
                <Grid item sm={3} xs={4} sx={{ textAlign: "left" }}>
                  <Link href={"/" + item.url} aria-label="url">
                    <Image src={`${PICURL}${item.imageCover}`} className={styles.widthImageArticelMobileSize} unoptimized="true" width="100" height="100" sizes="(max-width: 768px) auto, (max-width: 1200px) auto, auto" alt={item.meta} />
                  </Link>
                </Grid>
              </Grid>
              <div className={styles.flexMobileSize}>
                <div className={styles.flexMobileSizeChild}>
                  {item.author.authorPic ?
                    <Image onClick={() => { redirectAuthor(item) }} src={`${PICURL}${item.author.authorPic}`} className={styles.widthDoctorImage} unoptimized="true" width="100" height="100" sizes="(max-width: 768px) auto, (max-width: 1200px) auto, auto" alt={item.meta} />
                    :
                    <Image src={userImg} alt="user" unoptimized="true" width="25" height="25" />
                  }
                  <Typography
                    onClick={() => { redirectAuthor(item) }}
                    type="label"
                    size="large"
                    className={`${styles.minwidthArticle} ${styles.fontSizeMobileSizeArticle}`}
                  >
                    {item.author.authorName}.
                  </Typography>
                  <Typography
                    type="label"
                    size="large"
                    className={`${styles.commentStyleArticle} ${styles.fontSizeMobileSizeArticle} ${styles.textAlineArticle}`}
                  >
                    {`${Math.round((readingTimeText.time) / 60000)} دقیقه برای مطالعه `}.
                  </Typography>
                  <Typography
                    type="label"
                    size="large"
                    className={`${styles.commentStyleArticle} ${styles.fontSizeMobileSizeArticle} ${styles.textAlineArticle}`}
                  >
                    {toPersianDate(item.publishTime, { month: 'long', day: 'numeric' })}
                  </Typography>
                </div>
                <div className={styles.flexLikeComment}>
                  <div className={styles.flexLikeCommentItem}>
                    <ChatBubbleOutlineIcon
                      className={styles.commentStyleArticle}
                      sx={{ fontSize: "18px" }}
                    />
                    <span>
                      <Typography
                        type="label"
                        size="large"
                        className={styles.commentStyleArticle}
                      >
                        {item.commentCount}
                      </Typography></span>
                  </div>
                </div>
              </div>
            </div>
          </Hidden>
        </div >
        <Divider className={styles.stylesDivider} />
      </div >
    );
  }) : "مقاله ای یافت نشد";
}
