import { useState, useEffect } from "react";
import WestIcon from "@mui/icons-material/West";
import { Swiper, SwiperSlide } from "swiper/react";
import useMediaQuery from "@mui/material/useMediaQuery";
import Typography from "../../shared/Typography";
import styles from "./slider.module.css";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Image from "next/image";
import { v4 as uuidv4 } from 'uuid';
export default function slider({
  men = false,
  sliderItemNumber,
  sliderItemCode,
  titleSlider,
  titleDescription,
  btn1,
  btn2,
}) {
  const handleClass = () => {
    if (display) {
      if (men) {
        return styles.buttomStyleSlider1men;
      } else {
        return styles.buttomStyleSlider1;
      }
    } else if (!display) {
      if (men) {
        return styles.buttomStyleSliderMenActive;
      } else {
        return styles.buttomStyleSliderActive;
      }
    }
  };
  const handle = () => {
    if (display) {
      if (men) {
        return styles.buttomStyleSliderMenActive;
      } else {
        return styles.buttomStyleSliderActive;
      }
    } else if (!display) {
      if (men) {
        return styles.buttomStyleSlider1men;
      } else {
        return styles.buttomStyleSlider1;
      }
    }
  };

  const isMobileSizeActive = useMediaQuery("(max-width:428px)");
  const [swiperRef, setSwiperRef] = useState(null);
  const [swiperRefCode, setSwiperRefCode] = useState(null);

  const prevHandler = () => {
    swiperRef.slidePrev();
  };

  const nextHandler = () => {
    swiperRef.slideNext();
  };

  const prevHandlerCode = () => {
    swiperRefCode.slidePrev();
  };

  const nextHandlerCode = () => {
    swiperRefCode.slideNext();
  };

  const [display, setDisplay] = useState(true);
  const [slides, setSlides] = useState(1);
  const setSlidesPerview = () => {
    setSlides(
      window.innerWidth <= 430
        ? 1.3
        : window.innerWidth <= 1024
          ? 2.1
          : window.innerWidth > 1024
            ? 3.1
            : 3.1
    );
  };
  useEffect(() => {
    setSlidesPerview();
    window.addEventListener("resize", setSlidesPerview);
    return () => {
      window.removeEventListener("resize", setSlidesPerview);
    };
  }, []);

  return (
    <>
      <div className={styles.sliderBox}>
        <div className={styles.flexManinSlider}>
          <Typography
            type="display"
            size="medium"
            className={styles.styleMobileSizeSlider}
          >
            {titleSlider}
          </Typography>
          <Typography
            type="title"
            size="large"
            className={`${styles.textZigZagBoxSlider} ${styles.marginTopBottom}`}
          >
            {titleDescription}
          </Typography>
          <div className={styles.flexSliderSympathy}>
            <Button
              aria-label="btnName"
              size="large"
              variant="fill"
              onClick={() => setDisplay(true)}
              className={handleClass()}
            >
              <Typography
                type="title"
                size="large"
                className={styles.Button1}
              >
                {btn1}
              </Typography>

            </Button>
            <Button
              aria-label="btnName"
              size="large"
              variant="fill"
              onClick={() => setDisplay(false)}
              className={handle()}
            >
              <Typography
                type="title"
                size="large"
                className={styles.Button1}
              >
                {btn2}
              </Typography>
            </Button>
            <div className={styles.FlexArrowBtnSlider}>
              <Button
                size="large"
                variant="fill"
                aria-label="prevHandler"
                onClick={display ? prevHandler : prevHandlerCode}
                className={
                  men
                    ? styles.buttomArrowSliderMenPrev
                    : styles.buttomArrowSliderPrev
                }
              >
                <WestIcon sx={{ transform: "rotate(180deg)" }} />
              </Button>
              <Button

                size="large"
                variant="fill"
                aria-label="nextHandler"
                onClick={display ? nextHandler : nextHandlerCode}
                className={
                  men
                    ? styles.buttomArrowSliderMenNext
                    : styles.buttomArrowSliderNext
                }
              >
                <WestIcon />
              </Button>
            </div>
          </div>
        </div>

        <div
          className={styles.PaddingTopSlider}
          style={display ? { display: "block" } : { display: "none" }}
        >
          <Swiper
            onSwiper={(swiper) => setSwiperRef(swiper)}
            slidesPerView={slides}
            spaceBetween={20}
            initialSlide={0}
          >
            {sliderItemNumber.map((slide) => (
              <SwiperSlide key={uuidv4()} className={styles.sliderStyle}>
                <Grid container>
                  <Grid
                    item
                    md={slide.md}
                    sm={slide.sm}
                    xs={slide.xs}
                    order={{
                      xs: slide.orderXS,
                      sm: slide.orderSM,
                      md: slide.orderMD,
                    }}
                  >
                    <div className={styles.alinghImage}>
                      <Image
                        alt={slide.alt}
                        src={slide.image}
                        className={styles.widthImageSlider}
                      />
                    </div>
                  </Grid>

                  <Grid
                    item
                    md={slide.mdText}
                    sm={slide.smText}
                    xs={slide.xsText}
                    order={{
                      xs: slide.orderXSText,
                      sm: slide.orderSMText,
                      md: slide.orderMDText,
                    }}
                  >
                    <div className={styles.flexSliderSwiper}>
                      <div className={styles.minHeightSlider}>
                        <Typography
                          type="title"
                          size="large"
                          className={styles.textSliderLevel}
                        >
                          {slide.levelText}
                        </Typography>
                        <Typography
                          type="body"
                          size="medium"
                          className={styles.textSliderText}
                        >
                          {slide.text}
                        </Typography>
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
        {/* slider code */}
        <div
          className={styles.PaddingTopSlider}
          style={display ? { display: "none" } : { display: "block" }}
        >
          <Swiper
            onSwiper={(swiper) => setSwiperRefCode(swiper)}
            slidesPerView={slides}
            spaceBetween={20}
            className="sympathySlider"
            initialSlide={0}
          >
            {sliderItemCode.map((item) => (
              <SwiperSlide key={uuidv4()} className={styles.sliderStyle}>
                <Grid container>
                  <Grid
                    item
                    md={item.md}
                    sm={item.sm}
                    xs={item.xs}
                    order={{
                      xs: item.orderXS,
                      sm: item.orderSM,
                      md: item.orderMD,
                    }}
                  >
                    <div className={styles.alinghImage}>
                      <Image
                        alt={item.alt}
                        src={item.image}
                        className={styles.widthImageSlider}
                      />
                    </div>
                  </Grid>
                  <Grid
                    item
                    md={item.mdText}
                    sm={item.smText}
                    xs={item.xsText}
                    order={{
                      xs: item.orderXSText,
                      sm: item.orderSMText,
                      md: item.orderMDText,
                    }}
                  >
                    <div className={styles.flexSliderSwiper}>
                      <div className={styles.minHeightSlider}>
                        <Typography
                          type="title"
                          size="large"
                          className={styles.textSliderLevel}
                        >
                          {item.levelText}
                        </Typography>
                        <Typography
                          type="body"
                          size="medium"
                          className={styles.textSliderText}
                        >
                          {isMobileSizeActive ? item.textMobile : item.text}
                        </Typography>
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </SwiperSlide>
            ))}
          </Swiper>
        </div >
      </div >
    </>
  );
}
