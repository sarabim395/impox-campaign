import Typography from '@/shared/Typography'
import style from './comments.module.css';
import Image from 'next/image';


export default function Comments({ avatar, userName, text }) {

    return (
        <>
            <div className={style.comment}>
                <div className={style.userInfo}>
                    <Image src={avatar} alt='user' />
                    <span>{userName}</span>
                </div>
                <Typography component="p" type="body" size="large" color="#1C1B1E" >
                    {text}
                </Typography>
            </div>
        </>
    )
}