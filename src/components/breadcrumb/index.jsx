import Typography from '@/shared/Typography';
import Link from "next/link";
import styles from './breadcrumb.module.css';
export default function Breadcrumb({ categoryLink, categoryName, title }) {
    return (
        <nav className={styles.breadcrumb}>
            <ul>
                <li>
                    <Link href={"/"} aria-label="link">
                        <Typography
                            size="small"
                            type="title"
                            color="#48464A"
                            component="span"
                        >
                            خانه
                        </Typography>
                    </Link>
                </li>
                <li>
                    <span className={styles.seprateBreadcrumb}><svg xmlns="http://www.w3.org/2000/svg" width="11" height="17" viewBox="0 0 11 17" fill="none">
                        <path d="M10.0479 1.576L1.95994 16.192L0.255938 15.208L8.36794 0.592L10.0479 1.576Z" fill="#CAC5CA" />
                    </svg></span>
                    <Link href={"/blogs"} aria-label="link">
                        <Typography
                            size="small"
                            type="title"
                            color="#48464A"
                            component="span"
                        >
                            مقالات
                        </Typography>
                    </Link>
                </li>
                {
                    categoryName ?
                        <li>
                            <span className={styles.seprateBreadcrumb}><svg xmlns="http://www.w3.org/2000/svg" width="11" height="17" viewBox="0 0 11 17" fill="none">
                                <path d="M10.0479 1.576L1.95994 16.192L0.255938 15.208L8.36794 0.592L10.0479 1.576Z" fill="#CAC5CA" />
                            </svg></span>
                            <Typography
                                onClick={categoryLink}
                                className={styles.categoryName}
                                size="small"
                                type="title"
                                color="#48464A"
                                component="span"
                            >
                                {categoryName}
                            </Typography>
                        </li>
                        : undefined
                }
                {
                    title ?
                        <li className={styles.titlePost}>
                            <span className={styles.seprateBreadcrumb}><svg xmlns="http://www.w3.org/2000/svg" width="11" height="17" viewBox="0 0 11 17" fill="none">
                                <path d="M10.0479 1.576L1.95994 16.192L0.255938 15.208L8.36794 0.592L10.0479 1.576Z" fill="#CAC5CA" />
                            </svg></span>
                            <Typography
                                size="small"
                                type="title"
                                color="#EC407A"
                                component="span"
                                className={styles.title}
                            >
                                {title}
                            </Typography>
                        </li>
                        : undefined
                }
            </ul >
        </nav >
    )
}
