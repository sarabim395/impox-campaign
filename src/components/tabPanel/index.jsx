import PropTypes from 'prop-types'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'
import { useState, useEffect, useMemo, useRef } from "react"
import styles from "./tabPanel.module.css"
import value1 from "@/assets/images/tabsHome/tab1.webp"
import value2 from "@/assets/images/tabsHome/tab2.webp"
import value3 from "@/assets/images/tabsHome/tab3.webp"
import value4 from "@/assets/images/tabsHome/tab4.webp"
import value5 from "@/assets/images/tabsHome/tab5.webp"
import value6 from "@/assets/images/tabsHome/tab6.webp"
//  mobile
import value1m from "@/assets/images/tabsHome/mobile/tab1.webp"
import value2m from "@/assets/images/tabsHome/mobile/tab2.webp"
import value3m from "@/assets/images/tabsHome/mobile/tab3.webp"
import value4m from "@/assets/images/tabsHome/mobile/tab4.webp"
import value5m from "@/assets/images/tabsHome/mobile/tab5.webp"
import value6m from "@/assets/images/tabsHome/mobile/tab6.webp"
import Image from 'next/image'
import Typography from '@/shared/Typography'
import { Hidden } from '@mui/material'
var tabControl;
let slidesIndex = 0;
var currentIndex = 0;

var intervalBorder;
var intervalButton;

function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`${tabControl}-tabpanel-${index}`}
            aria-labelledby={`${tabControl}-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <span>{children}</span>
                </Box>
            )}
        </div>
    );
}


TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `${tabControl}-tab-${index}`,
        'aria-controls': `${tabControl}-tabpanel-${index}`,
        className: styles.frameTabTitle
    };
}

export default function VerticalTabsValues() {
    const [value, setValue] = useState(0);
    const [isHover, setIsHover] = useState(false);
    const [nextIndex, setNextIndex] = useState(0);


    const parsedTab = useMemo(() => value, [value]);

    const handleChange = (event, newValue) => {

        setValue(newValue);
    };

    const valuesList = [
        {
            image: value1,
            Mimage: value1m,
            title: "تشخیص پریود نامنظم",
            text: "گزارش‌گیری از دوره‌های قبلی پریود برای تشخیص اختلالات قاعدگی و پریود نامنظم و امکان ارسال برای پزشک"
        },
        {
            image: value2,
            Mimage: value2m,
            title: "فهمیدن حال هر روزت",
            text: "اطلاع از وضعیت جسمانی، احاسی و ذهنی روزانه بر اساس چرخه بیوریتم و دریافت توصیه‌های برای مدیریت هر حالت"
        },


        {
            image: value3,
            Mimage: value3m,
            title: "بهبود رابطه عاطفی و جنسی",
            text: "شناخت و درک بیشتر از سمت پارتنر با آگاهی از چرخه پریود و بیوریتم با هدف هر چه بهتر شدن رابطه عاطفی و جنسی"
        },
        {
            image: value4,
            Mimage: value4m,
            title: "دریافت توصیه‌های تخصصی ",
            text: "دریافت توصیه‌های تخصصی و مرتبط با دوره و نشانه‌هایی که در روزهای مختلف چرخه قاعدگیت تجربه می‌کنی"
        },
        {
            image: value5,
            Mimage: value5m,
            title: "مشاوره آنلاین با متخصصین",
            text: "امکان مشاوره سریع، آسان و به‌صرفه با تیم متخصصین زنان و زایمان، روان‌شناسی و سکس‌تراپی ایمپو            "
        },
        {
            image: value6,
            Mimage: value6m,
            title: "مراقبت هفته به هفته پس از زایمان",
            text: "دریافت توصیه‌های تخصصی برای مراقبت از مادر و نوزاد بر حسب نوع زایمان، علائم و نشانه‌های مادر و نوزاد و جنسیت و سن نوزاد تا ۶ ماه پس از زایمان"
        },
    ]

    const labelTabs = [
        {
            text: "گزارش قاعدگی",
            id: 1
        },
        {
            text: "بیوریتم",
            id: 2
        },
        {
            text: "همدلی",
            id: 3
        },
        {
            text: "توصیه‌های تخصصی",
            id: 4
        },
        {
            text: "کلینیک سلامت",
            id: 5
        },
        {
            text: "مراقبت بعد از زایمان",
            id: 6
        },
    ]
    useEffect(() => {
        const circle = document?.getElementById('rectBorder');
        if (circle) {
            var length = 478.935;
            intervalBorder = setInterval(() => {
                if (length < 954) {
                    circle.style.strokeDasharray = length++;
                }
            }, 9);
            return () => clearInterval(intervalBorder);
        }
    }, [parsedTab]);


    useEffect(() => {
        if (parsedTab !== valuesList.length) {
            intervalButton = setInterval(
                () => {
                    setValue(parsedTab + 1)
                },
                5000,
            );

            return () => clearInterval(intervalButton);
        } else {
            setValue(0)
        }
    }, [parsedTab]);

    useEffect(() => {
        if (window.innerWidth > 768) {
            tabControl = "vertical"
        } else {
            tabControl = "basic"
        }
    }, [])

    // useEffect(() => {
    //     if (isHover) {
    //         clearInterval(intervalBorder);
    //         clearInterval(intervalButton);
    //     }
    // }, [isHover])

    return (
        <div className={styles.valueFrame}>
            <Tabs
                orientation={tabControl}
                value={value}
                onChange={handleChange}
                aria-label={`${tabControl} tabs example`}
                className='frameTabs'
            >
                {labelTabs.map((tab, index) => {
                    const ref = useRef(null)

                    useEffect(() => {
                        const cls = (ref.current?.className);
                        if (nextIndex <= 5) {
                            slidesIndex = cls?.includes('Mui-selected') && (index + 1)
                        }
                        if (nextIndex == 5) {
                            setNextIndex(0)
                            index = 1;
                        }
                        if (slidesIndex) {
                            setNextIndex(slidesIndex)
                        }

                        return () => { slidesIndex = 0; setNextIndex(0) }
                    }, [parsedTab])
                    return (

                        <Tab
                            key={index}
                            ref={ref}
                            onMouseEnter={() => setIsHover(true)}
                            onMouseLeave={() => setIsHover(false)}
                            icon={
                                <svg className="facet-pill-border"
                                    height="52" width="100%" role="presentation" >
                                    <rect height="52" width="100%" id={nextIndex == index - 1 ? 'rectBorder' : null} ry="26" stroke-dasharray="478.935" stroke-dashoffset="478.935" class="next"></rect>
                                </svg>
                            }
                            sx={{ minHeight: "54px !important" }}
                            label={tab.text}
                            {...a11yProps(index + 1)} />
                    )
                })}
            </Tabs>

            {
                valuesList.map((item, i) => {
                    return (
                        <TabPanel value={value} key={i} index={i}>
                            <div className={styles.tabValues}>
                                <Hidden mdDown>
                                    <Image src={item.image} alt="image" width="auto" height="auto" loading={'lazy'} />
                                </Hidden>
                                <Hidden mdUp>
                                    <Image src={item.Mimage} alt="image" width="auto" height="auto" loading={'lazy'} />
                                </Hidden>
                                <div className={styles.description}>
                                    <Typography color="#1C1B1E" type='headline' size='small' component="h3">{item.title}</Typography>
                                    <Typography color="#5B595C" type='body' size='large' component="p">{item.text}</Typography>
                                </div>
                            </div>
                        </TabPanel>
                    )
                })
            }
        </div>
    );
}