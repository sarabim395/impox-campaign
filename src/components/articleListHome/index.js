import Typography from '@/shared/Typography'
import { Swiper, SwiperSlide } from 'swiper/react';
import "swiper/css/pagination";
import 'swiper/css';
import styles from './article.module.css';
import Image from 'next/image';
import axios, { PICURL } from '@/utils/axios';
import { useState, useEffect } from "react";
import Link from "next/link";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Hidden } from '@mui/material';
function ArticleList({ men }) {
    const [articleList, setArticleList] = useState([]);
    const [count, setCount] = useState(20);
    const [page, setPage] = useState(1);
    const fetchArticleList = async () => {
        var menId = "";
        if (men) {
            const catList = await axios.get('article/category/1/10')
            var categoryList = catList?.data.categories;
            categoryList.forEach(item => {
                if (item.title == "سلامت آقایان") {
                    menId = item.id;
                }
            })
        }
        const response = await axios.get(`article/articles/${men ? `category/${menId}/1/10` : `${page}/${count}`}`)
        if (response?.data?.articles) {
            const articles = response.data.articles;
            setArticleList(articles)
        } else {
            const section = document?.getElementById("articlesMen");
            section.style.display = "none";
        }
    }

    const paginationHandler = (event) => {
        console.log(event)
        // if (event.isEnd) {
        //     var pageNumber = page;
        //     var countNumber = count;
        //     setPage(pageNumber++)
        //     setPage(countNumber * 2)
        // }
    }

    useEffect(() => {
        fetchArticleList()
    }, [])
    return (
        <>
            <Hidden mdDown>
                <div className={styles.articles}>
                    <Swiper spaceBetween={35} slidesPerView={4} breakpionts={{ 768: { slidesPerView: 4, loop: false }, 360: { slidesPerView: 1, loop: false } }} onSlideChange={paginationHandler} loop={true} className={styles.swiperBlog}>
                        {articleList ? articleList?.map((item, index) => {
                            return (
                                <SwiperSlide key={index} className={styles.SwiperSlide}>
                                    <div className={styles.articleList}>
                                        <div className={styles.article}>
                                            <Link href={item.url} aria-label="link">
                                                <Image src={`${PICURL}${item.imageCover}`} unoptimized="false" width="100" height="100" alt={item.meta} />
                                            </Link>
                                            <div className={styles.details}>
                                                <Link href={item.url} aria-label="link">
                                                    <Typography component="h2" type="title" size="medium" colorType="neutral" color="OnBackground" className={styles.titleArticle} >
                                                        {item.title}
                                                    </Typography>
                                                </Link>
                                            </div>
                                            <div className={styles.bottomCard}>
                                                {item.categories.map((cat, i) => {
                                                    return (
                                                        <span span key={i} className={styles.catName}><Link aria-label="link" href={cat.slug}>{cat.title}</Link></span>
                                                    )
                                                })
                                                }
                                                <Link href={item.url} className={styles.readMore} aria-label="link">
                                                    <ArrowBackIcon />
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </SwiperSlide>
                            )
                        }) : <p className={styles.noResult}>مقاله ای یافت نشد</p>}
                    </Swiper>
                </div>
            </Hidden>
            <Hidden mdUp>
                <div className={styles.customSlider}>
                    {articleList ? articleList?.map((item, index) => {
                        return (
                            <div className={styles.articleList} key={index}>
                                <div className={styles.article}>
                                    <Link href={item.url} aria-label="link">
                                        <Image src={`${PICURL}${item.imageCover}`} unoptimized="false" width="100" height="100" alt={item.meta} />
                                    </Link>
                                    <div className={styles.details}>
                                        <Link href={item.url} aria-label="link">
                                            <Typography component="h2" type="title" size="medium" colorType="neutral" color="OnBackground" className={styles.titleArticle} >
                                                {item.title}
                                            </Typography>
                                        </Link>
                                    </div>
                                    <div className={styles.bottomCard}>
                                        {item.categories.map((cat, i) => {
                                            return (
                                                <span span key={i} className={styles.catName}><Link aria-label="link" href={cat.slug}>{cat.title}</Link></span>
                                            )
                                        })
                                        }
                                        <Link href={item.url} className={styles.readMore} aria-label="link">
                                            <ArrowBackIcon />
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        )
                    }) : <p className={styles.noResult}>مقاله ای یافت نشد</p>}
                </div>
            </Hidden>
        </>
    )
}

export default ArticleList