import { useState, useEffect } from "react";
import Link from "next/link";
import styles from "./appbar.module.css";
import logo from "@/assets/images/logo.svg";
import darklogo from "@/assets/images/darklogo.webp";
import Image from "next/image";
import { Button, Drawer } from "@mui/material";
import Typography from "@/shared/Typography";
import MenuIcon from "@mui/icons-material/Menu";
import ClearIcon from "@mui/icons-material/Clear";
import { v4 as uuidv4 } from "uuid";
import Script from "next/script";
export default function Appbar({ darkmode = true, index = false }) {
  const [hamburger, setHamburger] = useState(false);
  const [ClientWindowHeight, setClientWindowHeight] = useState();
  const site = "https://impo.app";
  let menulist = [
    {
      id: "1",
      link: "/",
      value: "صفحه اصلی",
    },
    {
      id: "2",
      link: `${site}/blogs`,
      value: "مجله سلامت",
    },
    {
      id: "3",
      link: `${site}/men`,
      value: "ایمپو آقایان ",
    },
    {
      id: "4",
      link: `${site}/sympathy`,
      value: "همدلی",
    },
    {
      id: "5",
      link: `/calendar`,
      value: "تقویم قاعدگی",
    },
    {
      id: "6",
      link: `${site}/careers`,
      value: "فرصت های شغلی",
    },
  ];

  useEffect(() => {
    var lastScrollTop = 0;
    window.addEventListener(
      "scroll",
      function () {
        var st = window.pageYOffset || document?.documentElement.scrollTop;
        if (st > lastScrollTop) {
          setClientWindowHeight(10);
          document?.getElementById("header")?.classList.add("mainDark");
          document
            ?.getElementById("header")
            ?.classList.remove("mainDarkSticky");
        } else if (st < lastScrollTop) {
          document?.getElementById("header")?.classList.add("mainDarkSticky");
          document?.getElementById("header")?.classList.remove("mainDark");
          if (document?.documentElement.scrollTop === 0) {
            setClientWindowHeight(0);
            document?.getElementById("header")?.classList.add("mainDark");
            document
              ?.getElementById("header")
              ?.classList.remove("mainDarkSticky");
          }
        }
        lastScrollTop = st <= 0 ? 0 : st;
      },
      false
    );
  }),
    [];

  const handel = () => {
    if (darkmode) {
      if (index) {
        if (ClientWindowHeight > 5) {
          return styles.justForIndexClientWindow;
        } else {
          return styles.justForIndex;
        }
      } else if (ClientWindowHeight > 5) {
        return styles.darkmodeAndClientWindow;
      } else {
        return styles.darkmode;
      }
    } else if (!darkmode) {
      if (ClientWindowHeight > 5) {
        return styles.notDarkmodeAndClientWindow;
      } else {
        return styles.notDarkmode;
      }
    }
  };

  const handelIndex = () => {
    if (darkmode) {
      if (index) {
        if (ClientWindowHeight > 5) {
          return styles.justindexColorTextClientwindow;
        } else {
          return styles.justindexColorText;
        }
      } else if (ClientWindowHeight > 5) {
        return styles.darkmodeColorTextClientWindow;
      } else {
        return styles.darkmodeColorTextClientWindow;
      }
    } else if (!darkmode) {
      if (ClientWindowHeight > 5) {
        return styles.darkmodeColorTextClientWindow;
      } else {
        return styles.darkmodeColorText;
      }
    }
  };

  const handelMobileSize = () => {
    if (darkmode) {
      if (index) {
        if (ClientWindowHeight > 5) {
          return styles.jsutIndexIconClientwindowMobileSize;
        } else {
          return styles.jsutIndexIconMobileSize;
        }
      } else if (ClientWindowHeight > 5) {
        return styles.darkmodeColorTextMobileSizeClientWindow;
      } else {
        return styles.darkmodeColorTextMobileSize;
      }
    } else if (!darkmode) {
      if (ClientWindowHeight > 5) {
        return styles.notDarkmodeColorClientWindowMobileSize;
      } else {
        return styles.notDarkmodeColorMobileSize;
      }
    }
  };

  const handelBtnMobileSize = () => {
    if (darkmode) {
      if (index) {
        if (ClientWindowHeight > 5) {
          return styles.jsutIndexIconClientwindowMobileSizeBtn;
        } else {
          return styles.jsutIndexIconMobileSizeBtn;
        }
      } else if (ClientWindowHeight > 5) {
        return styles.darkmodeColorTextMobileSizeClientWindowBtn;
      } else {
        return styles.darkmodeColorTextMobileSizeBtn;
      }
    } else if (!darkmode) {
      if (ClientWindowHeight > 5) {
        return styles.notDarkmodeColorClientWindowMobileSizeBtn;
      } else {
        return styles.notDarkmodeColorMobileSizeBtn;
      }
    }
  };

  return (
    <>
      <Script
        async
        src="https://www.googletagmanager.com/gtag/js?id=UA-216376616-1"
      />
      <Script>
        {`
          (function(w,d,s,l,i){w[l] = w[l] || [];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-PT95KXQ');
          `}
      </Script>
      <Script
        type="text/javascript"
        src="https://s1.mediaad.org/serve/80760/retargeting.js"
        async
      ></Script>

      <Script>
        {`
          !function (t, e, n) {
            t.yektanetAnalyticsObject = n, t[n] = t[n] || function () {
                t[n].q.push(arguments)
            }, t[n].q = t[n].q || [];
            var a = new Date, r = a.getFullYear().toString() + "0" + a.getMonth() + "0" + a.getDate() + "0" + a.getHours(),
                c = e.getElementsByTagName("script")[0], s = e.createElement("script");
            s.id = "ua-script-ASatuT3o"; s.dataset.analyticsobject = n;
            s.async = 1; s.type = "text/javascript";
            s.src = "https://cdn.yektanet.com/rg_woebegone/scripts_v3/ASatuT3o/rg.complete.js?v=" + r, c.parentNode.insertBefore(s, c)
        }(window, document, "yektanet");
       `}
      </Script>

      <header id="header" className="mainDark">
        {/* desktop size */}
        <div className={styles.DesktopSize}>
          <Link
            href={"/"}
            aria-label="link"
            style={{
              display: "grid",
            }}
          >
            <Image
              width="48"
              height="48"
              className={styles.logo}
              src={darkmode ? logo : darklogo}
              alt="لوگو"
            />
          </Link>
          <div style={{ flexGrow: "3" }}>
            <ul className={styles.MenuWrapper}>
              {menulist.map((Item) => {
                return (
                  <li key={uuidv4()}>
                    <Link href={Item.link} aria-label="link">
                      <Typography
                        size="small"
                        type="title"
                        sx={{ margin: "0px 32px" }}
                        component="span"
                        className={handelIndex()}
                      >
                        {Item.value}
                      </Typography>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </div>
          <div style={{ position: "relative", top: "0" }}>
            <Link href="#download" aria-label="link">
              <Button
                variant="surface"
                size="large"
                className={handel()}
                aria-label="btnName"
              >
                <Typography size="small" type="title">
                  دانلود ایمپو
                </Typography>
              </Button>
            </Link>
          </div>
        </div>

        {/* mobile Size */}
        <div className={styles.mobileSize}>
          <div className={styles.FlexmobileSizeBoxBtn}>
            <Button
              variant="text"
              onClick={() => setHamburger(!hamburger)}
              className={handelMobileSize()}
              aria-label="menu"
            >
              <MenuIcon style={{ fontSize: "30px" }} />
            </Button>
            <div className={styles.downloadHeader}>
              <Link href="#download" aria-label="link">
                <Button className={handelBtnMobileSize()} aria-label="download">
                  دانلود ایمپو
                </Button>
              </Link>
            </div>
          </div>
          <Drawer
            anchor={"right"}
            open={hamburger}
            variant={"temporary"}
            PaperProps={{ style: { width: "45%" } }}
          >
            <div
              className={styles.NavBarMobileSize}
              style={hamburger ? { display: "block" } : { display: "none" }}
            >
              <Button
                aria-label="btnName"
                variant="text"
                onClick={() => setHamburger(!hamburger)}
                sx={{ color: "#000" }}
                style={hamburger ? { display: "block" } : { display: "none" }}
              >
                <ClearIcon />
              </Button>
              <Image width="auto" height="auto" src={logo} alt="لوگو" />
              {menulist.map((Item) => {
                return (
                  <Link href={Item.link} key={uuidv4()} aria-label="link">
                    <Typography
                      size="large"
                      type="title"
                      className={styles.styleEachTab}
                    >
                      {Item.value}
                    </Typography>
                  </Link>
                );
              })}
            </div>
          </Drawer>
        </div>
      </header>
    </>
  );
}
