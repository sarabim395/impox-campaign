import style from "./popupBanner.module.css"
import closeCircle from "@/assets/images/close-circle.svg"
import Image from "next/image"
import { useState, useEffect } from "react"
import { Hidden } from '@mui/material'
import popup1 from "@/assets/images/banner/popup-banner/popup1.webp"
import popup2 from "@/assets/images/banner/popup-banner/popup2.webp"
import popup3 from "@/assets/images/banner/popup-banner/popup3.webp"
import popup4 from "@/assets/images/banner/popup-banner/popup4.webp"
import popup5 from "@/assets/images/banner/popup-banner/popup5.webp"
import popup6 from "@/assets/images/banner/popup-banner/popup6.webp"
import popup7 from "@/assets/images/banner/popup-banner/popup7.webp"
// 
import popup1m from "@/assets/images/banner/popup-banner/popup1m.webp"
import popup2m from "@/assets/images/banner/popup-banner/popup2m.webp"
import popup3m from "@/assets/images/banner/popup-banner/popup3m.webp"
import popup4m from "@/assets/images/banner/popup-banner/popup4m.webp"
import popup5m from "@/assets/images/banner/popup-banner/popup5m.webp"
import popup6m from "@/assets/images/banner/popup-banner/popup6m.webp"
import popup7m from "@/assets/images/banner/popup-banner/popup7m.webp"
// 
import exit1 from "@/assets/images/banner/exitBanner/exit1.webp"
import exit2 from "@/assets/images/banner/exitBanner/exit2.webp"
import exit3 from "@/assets/images/banner/exitBanner/exit3.webp"
import exit4 from "@/assets/images/banner/exitBanner/exit4.webp"
import exit5 from "@/assets/images/banner/exitBanner/exit5.webp"
import exit6 from "@/assets/images/banner/exitBanner/exit6.webp"
import Link from "next/link"
export default function PopupBanner({ isActive, isExit, categoryInfo }) {
    const [enable, setEnable] = useState(false)
    const [enableExit, setEnableExit] = useState(false)

    const endBlog = [
        {
            text: "برای خوندن مقاله‌های بیشتر در این رابطه و پیش‌بینی پریود و تشخیص پریود نامنظم همین الان اپلیکیشن تقویم قاعدگی ایمپو رو نصب کن",
            textBtn: "نصب اپلیکیشن ایمپو",
            img: popup1,
            imgM: popup1m,
            catName: "پریود و قاعدگی"
        },
        {
            text: "برای خواندن مقاله‌های بیشتر در مورد بارداری و زایمان و مراقبت هفته به هفته در دوران بارداری همین الان اپلیکیشن تقویم بارداری ایمپو رو نصب کن",
            textBtn: "نصب اپلیکیشن ایمپو",
            img: popup2,
            imgM: popup2m,
            catName: "بارداری و زایمان"
        },
        {
            text: "برای خوندن مقاله‌های بیشتر با موضوعات روان‌شناسی و مشاوره آنلاین با روان‌شناس‌های ایمپو همین الان اپلیکیشن ایمپو رو نصب کن",
            textBtn: "نصب اپلیکیشن ایمپو",
            img: popup3,
            imgM: popup3m,
            catName: "روانشناسی"
        },
        {
            text: "برای خوندن مقاله‌های بیشتر در حوزه سکس‌تراپی و مشاوره آنلاین با سکس‌تراپ‌های ایمپو همین الان اپلیکیشن ایمپو رو نصب کن",
            textBtn: "نصب اپلیکیشن ایمپو",
            img: popup4,
            imgM: popup4m,
            catName: "سکس تراپی"
        },
        {
            text: "برای خواندن مقالات بیشتر در این مورد و ایجاد یه رابطه بهتر و لذتبخش تر با همسرت ایمپو رو نصب کن و تو قسمت همدل با همسرت خاطره‌های خوب بساز",
            textBtn: "نصب اپلیکیشن ایمپو",
            img: popup5,
            imgM: popup5m,
            catName: "سلامت آقایان"
        },
        {
            text: "برای خوندن مقالات بیشتر در مورد بیماری‌های زنان، پیش‌بینی پریودت و درمان اختلالات قاعدگی می‌تونی همین الان اپلیکیشن تقویم قاعدگی ایمپو رو نصب کنی",
            textBtn: "نصب اپلیکیشن ایمپو",
            img: popup6,
            imgM: popup6m,
            catName: "سلامت بانوان"
        },
        {
            text: "برای خوندن مقالات بیشتر با موضوع بهبود کیفیت زندگی و مشاهده حال و احوال هر روزت بر اساس بیوریتم اپلیکیشن ایمپو رو نصب کن",
            textBtn: "نصب اپلیکیشن ایمپو",
            img: popup7,
            imgM: popup7m,
            catName: "سبک زندگی"
        },
    ]

    const exitPage = [
        {
            text: "برای پیش‌بینی پریودت و استفاده از همه امکانات اپلیکیشن تقویم قاعدگی ایمپو",
            textBtn: "همین الان نصبش کن",
            img: exit1,
            catName: "پریود و قاعدگی"
        },
        {
            text: "برای مراقبت هفته به هفته در دوران بارداری رو همین الان امتحان کن",
            textBtn: "همین الان نصبش کن",
            img: exit6,
            catName: "بارداری و زایمان"
        },
        {
            text: "برای استفاده از همه امکانات اپلیکیشن ایمپو و مشاوره آنلاین با روان‌شناس‌های ایمپو",
            textBtn: "همین الان نصبش کن",
            img: exit5,
            catName: "روانشناسی"
        },
        {
            text: "برای استفاده از همه امکانات اپلیکیشن ایمپو مشاوره آنلاین با سکس‌تراپ‌های ایمپو",
            textBtn: "همین الان نصبش کن",
            img: exit4,
            catName: "سکس تراپی"
        },
        {
            text: "برای استفاده از همه امکانات اپلیکیشن ایمپو و مشاوره با سکس‌تراپ‌های ایمپو",
            textBtn: "همین الان نصبش کن",
            img: exit4,
            catName: "سلامت آقایان"
        },
        {
            text: "برای پیش‌بینی پریود، تشخیص اختلالات قاعدگی و مشاوره با متخصصین زنان و زایمان ایمپو",
            textBtn: "همین الان نصبش کن",
            img: exit3,
            catName: "سلامت بانوان"
        },
        {
            text: "برای استفاده از بیوریتم تا حال جسمی و روحی هر روزت رو بفهمی و بتونی برنامه ریزی درستی داشته باشی",
            textBtn: "همین الان نصبش کن",
            img: exit2,
            catName: "سبک زندگی"
        },
    ]

    useEffect(() => {
        setEnable(isActive)
    }, [isActive])

    useEffect(() => {
        setEnableExit(isExit)
    }, [isExit])

    return (
        <>
            {enableExit ?
                <Hidden mdDown>
                    <div className={style.popupBanner}>
                        {
                            exitPage.map((item) => {

                                return (
                                    item.catName == categoryInfo.title &&
                                    <div className={style.banner} style={{ backgroundImage: `url(${item.img.src})` }}>
                                        <button onClick={() => setEnableExit(false)} className={style.closePopup}><Image src={closeCircle} alt="closeCircle" /></button>
                                        <div className={style.content}>
                                            <p>
                                                <p>یک هفته اشتراک رایگان</p>
                                                {item.text}
                                            </p>
                                            <Link href={"/download"} className={style.installApp}>{item.textBtn}</Link>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </Hidden>
                :
                enable &&
                <>
                    <Hidden mdDown>
                        <div className={style.popupBanner}>
                            {
                                endBlog.map((item) => {
                                    return (
                                        item.catName == categoryInfo.title &&
                                        <div className={style.banner} style={{ backgroundImage: `url(${item.img.src})` }}>
                                            <button onClick={() => setEnable(false)} className={style.closePopup}><Image src={closeCircle} alt="closeCircle" /></button>
                                            <div className={style.content}>
                                                <p>
                                                    {item.text}
                                                </p>
                                                <Link href={"/download"} className={style.installApp}>{item.textBtn}</Link>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </Hidden>
                    <Hidden mdUp>
                        <div className={style.popupBanner}>
                            {
                                endBlog.map((item) => {
                                    return (
                                        item.catName == categoryInfo.title &&
                                        <div className={`${style.banner} ${style.bannerM}`} style={{ backgroundImage: `url(${item.imgM.src})` }}>
                                            <button onClick={() => setEnable(false)} className={`${style.closePopup} ${style.closePopupM}`}><Image src={closeCircle} width={20} height={20} style={{ objectFit: "contain" }} alt="closeCircle" /></button>
                                            <div className={`${style.content} ${style.contentM}`}>
                                                <p>
                                                    {item.text}
                                                </p>
                                                <Link href={"/download"} className={style.installApp}>{item.textBtn}</Link>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </Hidden>
                </>
            }
        </>
    )
}