import { Hidden } from "@mui/material";
import styles from "./download.module.css";
import womenApp from "@/assets/images/womenApp.webp";
import womenAppMobile from "@/assets/images/womenAppMobile.webp";
import menApp from "@/assets/images/menApp.webp";
import webapp from "@/assets/images/webapp.svg";
import bazar from "@/assets/images/bazar.svg";
import myket from "@/assets/images/myket.svg";
import direct from "@/assets/images/direct.svg";
import Image from "next/image";
import Typography from "@/shared/Typography";
import Button from "@mui/material/Button";
import Lottie from "lottie-react";
import dnApp from "@/assets/images/lottie/dnapp.json";
const Download = ({ women = true }) => {
  return (
    <main className={styles.main}>
      <div className={styles.DonlwoadWrapper}>
        <div className={styles.DownloadItemLinks}>
          {women ? (
            <p className={styles.DownlaodNow}>
              <span className={styles.WomenImpoTextDownload}>۷ روز</span>
              اشتراک رایگان ایمپو
            </p>
          ) : (
            <p className={styles.DownlaodNow}>
              همین الان{" "}
              <span className={styles.MenImpoTextDownload}>ایمپو آقایان</span>{" "}
              رو نصب کن!
            </p>
          )}
          <div
            // type="title"
            // size="large"
            // color="#fff"
            className={styles.DownloadDescription}
            // mobileSize="large"
            // MobileType="headline"
          >
            <Hidden mdDown>
              اگه دونستن تاریخ پریودت برات مهمه و میخوای تغییرات سیکل قاعدگیت رو
              بهتر مدیریت کنی، <span>همین الان ایمپو رو نصب کن!</span>
            </Hidden>
            <Hidden mdUp>
              اگه دونستن تاریخ پریودت برات مهمه و میخوای تغییرات سیکل قاعدگیت رو
              بهتر مدیریت کنی، <div></div>
              <span>همین الان ایمپو رو نصب کن!</span>
            </Hidden>
          </div>
          <div className={styles.DownloDtnWrapper}>
            <a
              href={`http://yun.ir/bazar-${women ? "woman" : "man"}`}
              target="_blank"
              className={styles.DownloadAppBtn}
            >
              <Button
                aria-label="btnName"
                variant="outline"
                size="large"
                id="DownloadBazar"
              >
                <Image
                  width="auto"
                  height="auto"
                  src={bazar}
                  alt="کافه بازار"
                  className={styles.DownloadBtnImage}
                />
                <Typography type="title" size="medium">
                  کافه بازار
                </Typography>
              </Button>
            </a>
            <a
              href={`http://yun.ir/myket-${women ? "woman" : "man"}`}
              target="_blank"
              className={styles.DownloadAppBtn}
            >
              <Button
                aria-label="btnName"
                variant="outline"
                size="large"
                id="DownloadMyket"
              >
                <Image
                  width="auto"
                  height="auto"
                  src={myket}
                  alt="مایکت "
                  className={styles.DownloadBtnImage}
                />
                <Typography type="title" size="medium">
                  مایکت
                </Typography>
              </Button>
            </a>
            <a
              href={`http://yun.ir/web-${women ? "woman" : "man"}`}
              target="_blank"
              className={styles.DownloadAppBtn}
            >
              <Button
                variant="outline"
                size="large"
                aria-label="btnName"
                id="DownloadWebApp"
              >
                <Image
                  width="auto"
                  height="auto"
                  src={webapp}
                  alt=" وب اپلیکیشن "
                  className={styles.DownloadBtnImage}
                />
                <Typography type="title" size="medium">
                  وب اپ ( کاربران IOS )
                </Typography>
              </Button>
            </a>

            <a
              href={`${`http://yun.ir/direct-${women ? "woman" : "man"}`}`}
              download="impo.apk"
              target="_blank"
              className={styles.DownloadAppBtn}
            >
              <Button
                variant="outline"
                size="large"
                aria-label="btnName"
                id="DownloadDirect"
              >
                <Image
                  width="auto"
                  height="auto"
                  src={direct}
                  alt="دانلود مستقیم "
                  className={styles.DownloadBtnImage}
                />
                <Typography type="title" size="medium">
                  دانلود مستقیم
                </Typography>
              </Button>
            </a>
          </div>
        </div>
        <div className={styles.mockap}>
          <Hidden mdDown>
            {women == false ? (
              <Lottie
                animationData={dnApp}
                className={styles.motionMain}
                loop={true}
              />
            ) : (
              <Lottie
                animationData={dnApp}
                className={styles.motionMain}
                loop={true}
              />
            )}
          </Hidden>
          <Hidden mdUp>
            {women == false ? (
              <Image
                width="auto"
                height="auto"
                className={styles.DownloadMockupMen}
                src={menApp}
                alt="دانلود ایمپو آقایان"
              />
            ) : (
              <Lottie
                animationData={dnApp}
                className={styles.motionMain}
                loop={true}
              />
            )}
          </Hidden>
        </div>
      </div>
    </main>
  );
};
export default Download;
