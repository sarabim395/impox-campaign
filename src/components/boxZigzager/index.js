import Typography from "../../shared/Typography";
import styles from "./boxZigzager.module.css";
import Image from "next/image";
import { Grid, Divider } from "@mui/material";
import { v4 as uuidv4 } from 'uuid';
export default function boxZigzager({ boxZigZag }) {
  return (
    <>

      {boxZigZag.map((item) => (
        <Grid container className={styles.containerZig} justifyContent={"center"} key={uuidv4()}>
          <Grid
            md={item.md}
            sm={item.sm}
            order={{ sm: item.orderSM, xs: item.orderXS, md: item.orderMD }}
          >
            <div className={`${styles.Article1ChildPrevention} ${item.class2}`}>
              <div className={styles.flexMobileSizePrevention}>
                <span className={styles.textStyle1Prevention}>
                  {item.labelText}
                </span>
                <Typography className={styles.textZigZagBoxPrevention}>
                  {item.note}
                </Typography>
              </div>
            </div>
            <Divider className={styles.Dividerstyle2} />
          </Grid>
          <Grid
            md={item.mdText}
            sm={item.smText}
            order={{
              sm: item.orderSMText,
              xs: item.orderXSText,
              md: item.orderMDText,
            }}
          >
            <div
              className={`${styles.imageStyle} ${styles.textAlignCenterPrevention}`}
            >
              <Image
                priority
                alt={item.alt}
                src={item.image}
                width={item.sw}
                height={item.sh}
                className={`${styles.ImageStyleFix} ${item.className}`}
              />
            </div>
          </Grid>
        </Grid>
      ))}
    </>
  );
}
