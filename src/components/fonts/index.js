import localFont from 'next/font/local';
const YekanBakh = localFont({
    src: [
        {
            path: '../../../public/fonts/YekanBakh-VF.ttf',
            display: 'swap',
        }
    ], preload: true
})

export default YekanBakh;

