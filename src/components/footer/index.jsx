import styles from "./footer.module.css";
import Image from "next/image";
import logoFooter from "../../assets/images/logoFooter.svg";
import Typography from "../../shared/Typography";
import Link from "next/link";
import samandehi from "@/assets/images/samandehi.webp";
import enamad from "@/assets/images/enamad.webp";
import {
  Divider,
  Grid,
  Hidden,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";
import instagram from "@/assets/images/instagram.svg";
import telegram from "@/assets/images/telegram.svg";
import mail from "@/assets/images/mail.svg";
import twitter from "@/assets/images/twitter.svg";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { v4 as uuidv4 } from "uuid";
export default function Footer() {
  const descriptionFooter =
    "ایمپو بزرگترین پلتفرم حوزه سلامت زنان، متشکل از تیم‌های تخصصی پزشکان متخصص زنان، روان‌شناسی و سکس‌تراپی و همچنین کارشناسان و مهندسین حوزه فناوری اطلاعات است که در سال 1398 با هدف سلامت جسمانی و روانی زنان در دوران قاعدگی و بارداری و همچنین بهبود رابطه عاطفی و سلامت مردان تشکیل شد.";
  const site = "https://impo.app";
  const sitePages = [
    {
      id: 1,
      url: "/",
      value: "صفحه اصلی",
    },
    {
      id: 2,
      url: `${site}/#download`,
      value: "دانلودها",
    },
    {
      id: 3,
      url: `${site}/careers`,
      value: "فرصت های شغلی",
    },
    {
      id: 4,
      url: `${site}/aboutUs`,
      value: "درباره ما",
    },
    {
      id: 5,
      url: `${site}/ContactUs`,
      value: "تماس باما",
    },
    // {
    //     id: 3,
    //     url: "/",
    //     value: "راهنمای اپ"
    // },
  ];
  const impoServices = [
    {
      id: 1,
      url: `/calendar`,
      value: "تقویم قاعدگی",
    },
    {
      id: 2,
      url: `${site}/prevention`,
      value: "پیشگیری از بارداری",
    },
    {
      id: 3,
      url: `${site}/care`,
      value: "مراقبت در دوران بارداری",
    },
    // {
    //     id: 4,
    //     url: "/biorhythm",
    //     value: "بیوریتم"
    // },
  ];
  const otherProduct = [
    {
      id: 1,
      url: `${site}/men`,
      value: "ایمپو آقایان",
    },
  ];
  const socialItems = [
    {
      id: 1,
      alt: "instagram",
      img: instagram,
      url: "https://www.instagram.com/impo.app",
    },
    {
      id: 2,
      alt: "telegram",
      img: telegram,
      url: "https://t.me/impo_app",
    },

    {
      id: 3,
      alt: "twitter",
      img: twitter,
      url: "https://twitter.com/impoapp",
    },

    {
      id: 4,
      alt: "whatsapp",
      img: mail,
      url: "mailto:support@impo.app",
    },
  ];
  const footerLinksMobile = [
    { title: "صفحات سایت", links: sitePages },
    { title: "خدمات ایمپو", links: impoServices },
    { title: "سایر محصولات", links: otherProduct },
  ];
  const copyRight =
    "@ کپی‌رایت 1402؛ تمامی حقوق مادی و معنوی متعلق به هلدینگ تاک است.";

  return (
    <>
      <footer className={styles.footer}>
        <section className="container">
          <Grid container spacing={10}>
            <Grid item xl={4} lg={4} sm={12}>
              <Image
                width="auto"
                height="auto"
                src={logoFooter}
                alt="logoFooter"
              />
              <Typography
                className={styles.textFooter}
                size="small"
                type="title"
                colorType="neutral"
                color="OnBackground"
              >
                {descriptionFooter}
              </Typography>
              <Hidden mdUp>
                <Divider className={styles.dividerMobile} />
              </Hidden>
            </Grid>
            <Hidden mdUp>
              {footerLinksMobile.map((item) => {
                return (
                  <Accordion key={uuidv4()} className={styles.footerAccordion}>
                    <AccordionSummary
                      classes={{
                        root: styles.MuiAccordionSummaryRoot,
                        contentGutters:
                          styles.MuiAccordionSummaryContentGutters,
                      }}
                      expandIcon={<ExpandMoreIcon />}
                    >
                      <Typography
                        className={styles.itemList}
                        size="large"
                        type="body"
                        colorType="neutral_variants"
                        color="OnSurfaceVariant"
                      >
                        {item.title}
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <ul>
                        {item.links.map((link, index) => {
                          return (
                            <li key={uuidv4()}>
                              <Link href={link.url} aria-label="link">
                                <Typography
                                  className={styles.itemList}
                                  component="span"
                                  size="large"
                                  type="body"
                                  colorType="neutral_variants"
                                  color="OnSurfaceVariant"
                                >
                                  {link.value}
                                </Typography>
                              </Link>
                            </li>
                          );
                        })}
                      </ul>
                    </AccordionDetails>
                  </Accordion>
                );
              })}
            </Hidden>
            <Hidden mdDown>
              <Grid item sm={1.5} xs={12}>
                <Typography
                  className={styles.titleItem}
                  component="span"
                  size="small"
                  type="title"
                  colorType="neutral"
                  color="OnBackground"
                >
                  صفحات سایت
                </Typography>
                <ul>
                  {sitePages.map((item, index) => {
                    return (
                      <li key={uuidv4()}>
                        <Link href={item.url} aria-label="link">
                          <Typography
                            className={styles.itemList}
                            component="span"
                            size="large"
                            type="body"
                            colorType="neutral_variants"
                            color="OnSurfaceVariant"
                          >
                            {item.value}
                          </Typography>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </Grid>
              <Grid item sm={1.5} xs={12}>
                <Typography
                  className={styles.titleItem}
                  component="span"
                  size="small"
                  type="title"
                  colorType="neutral"
                  color="OnBackground"
                >
                  خدمات ایمپو
                </Typography>
                <ul>
                  {impoServices.map((item, index) => {
                    return (
                      <li key={uuidv4()}>
                        <Link href={item.url} aria-label="link">
                          <Typography
                            className={styles.itemList}
                            component="span"
                            size="large"
                            type="body"
                            colorType="neutral_variants"
                            color="OnSurfaceVariant"
                          >
                            {item.value}
                          </Typography>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </Grid>
              <Grid item sm={1.5} xs={12}>
                <Typography
                  className={styles.titleItem}
                  component="span"
                  size="small"
                  type="title"
                  colorType="neutral"
                  color="OnBackground"
                >
                  سایر محصولات
                </Typography>
                <ul>
                  {otherProduct.map((item, index) => {
                    return (
                      <li key={uuidv4()}>
                        <Link href={item.url} aria-label="link">
                          <Typography
                            className={styles.itemList}
                            component="span"
                            size="large"
                            type="body"
                            colorType="neutral_variants"
                            color="OnSurfaceVariant"
                          >
                            {item.value}
                          </Typography>
                        </Link>
                      </li>
                    );
                  })}
                </ul>
              </Grid>
            </Hidden>
            <Grid
              item
              sm={12}
              md={3.5}
              xs={12}
              className={styles.symbolsWrapper}
            >
              <Grid container spacing={6} className={styles.symbols}>
                <Grid item sm={6} xs={6}>
                  <Link href="https://logo.samandehi.ir/logo.aspx?id=316386&p=qftinbpdwlbqqftiaqgwwlbq">
                    <Image
                      width="auto"
                      height="auto"
                      src={samandehi}
                      alt="samandehi"
                    />
                  </Link>
                </Grid>
                <Grid item sm={6} xs={6}>
                  <Link href="https://trustseal.enamad.ir/?id=225142&code=FxndcR03u9wEbt07YRbq">
                    <Image
                      width="auto"
                      height="auto"
                      src={enamad}
                      alt="enamad"
                    />
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Divider className={styles.divider} />
          <Grid container spacing={10}>
            <Grid item sm={6} xs={12}>
              <Typography
                component="span"
                className={styles.copyRight}
                size="large"
                type="label"
                colorType="neutral_variants"
                color="Outline"
              >
                {copyRight}
              </Typography>
            </Grid>
            <Grid item sm={6} xs={12} className={styles.socialBox}>
              <ul className={styles.socialItems}>
                {socialItems.map((item, index) => {
                  return (
                    <li key={uuidv4()}>
                      <Link href={item.url} aria-label="link">
                        <span>
                          <Image
                            width="auto"
                            height="auto"
                            src={item.img}
                            alt={item.alt}
                          />
                        </span>
                      </Link>
                    </li>
                  );
                })}
              </ul>
            </Grid>
          </Grid>
        </section>
      </footer>
    </>
  );
}
