import styles from "./sidbarBanner.module.css"
import Image from 'next/image';
import { Button } from "@mui/material";
import Typography from "@/shared/Typography";
import calendarePeriod from "@/assets/images/banner/calendarePeriod.webp";
import biorythm from "@/assets/images/banner/biorythm.webp";
import clinic from "@/assets/images/banner/clinic.webp";
import pregnancy from "@/assets/images/banner/pregnancy.webp";
import sympathy from "@/assets/images/banner/sympathy.webp";
import { useEffect, useState } from "react";
import Link from "next/link";
export default function SidbarBanner({ categoryName }) {
    const [state, setState] = useState({})

    useEffect(() => {
        if (categoryName == "پریود و قاعدگی") {
            setState({
                text: "برای پیش‌بینی پریودهای بعدیت و تشخیص اختلالات قاعدگی، اپلیکیشن تقویم قاعدگی ایمپو رو نصب کن",
                buttonText: "دانلود ایمپو",
                cover: calendarePeriod
            })
        } else if (categoryName == "بارداری و زایمان") {
            setState({
                text: "مراقبت از خودت در دوران بارداری و مشاهده وضعیت جنین در هفته‌های مختلف بارداری رو به اپلیکیشن ایمپو بسپار",
                buttonText: "دانلود ایمپو",
                cover: pregnancy
            })
        } else if (categoryName == "سبک زندگی") {
            setState({
                text: "با بیوریتم ایمپو هیچوقت غافلگیر نمیشی. ما بهت میگیم که هر روز حالت چطوره تا برنامه ریزی درستی داشته باشی",
                buttonText: "دانلود ایمپو",
                cover: biorythm
            })
        } else if (categoryName == "سکس تراپی") {
            setState({
                text: "یه رابطه خوب، ساختنیه. تو قسمت همدل اپلیکیشن ایمپو رابطه عاطفی و جنسیت رو بهبود بده",
                buttonText: "دانلود ایمپو",
                cover: sympathy
            })
        } else if (categoryName == "سلامت آقایان" || categoryName == "سلامت بانوان" || categoryName == "روانشناسی") {
            setState({
                text: "فقط دو تا کلیک تا مشاوره آنلاین برای حل مشکلاتت در اپلیکیشن ایمپو فاصله داری",
                buttonText: "دانلود ایمپو",
                cover: clinic
            })
        }
    }, [])
    return (
        <>
            {categoryName ? <div className={styles.bannerFrame}>
                <div className={styles.details}>
                    <div className={styles.text}>
                        <Typography
                            size="medium"
                            type="title"
                            component="span"
                            className={styles.textHead}
                        >
                            {state.text}
                        </Typography>
                    </div>
                    <Link href="#download" aria-label="link">
                        <Button
                            aria-label="btnName"
                            className={styles.button}
                            variant="primary"
                            size="medium"
                        >

                            <Typography
                                size="small"
                                type="title"
                            >
                                {state.buttonText}
                            </Typography>
                        </Button>
                    </Link>
                </div>
                <Image className={styles.cover} unoptimized="true" width="397" height="624" src={state.cover} alt="بنر" />
            </div>
                : undefined}

        </>
    )
}