import { useState, useEffect } from "react";
import styles from './input.module.css'
import { InputAdornment, TextField } from "@mui/material";
import { toFarsiNumber } from "../../utils";


interface inputProps {
  className?: string;
  value?: string;
  defaultValue?: string;
  onChange?: (event: any) => void;
  icon?: string;
  align?: string;
  iconFun?: (event: any) => void;
  labelInput?: string;
  maxLength?: string;
  placeholder?: string;
  multiline?: boolean;
  rows?: string | undefined;
  type?: string;
}
function Input({
  className,
  value,
  defaultValue,
  maxLength,
  onChange = () => { },
  icon,
  align,
  iconFun,
  labelInput,
  placeholder,
  multiline = false,
  rows = undefined,
  type = "text",
  ...rest
}: inputProps) {
  const [_value, setValue] = useState('');
  const onTextFieldChange = (event) => {
    const innerValue = event.target.value ?? '';
    setValue(innerValue);
    onChange(innerValue);
  };
  useEffect(() => {
    setValue((value));
  }, [value]);
  return (
    <>
      {labelInput ? <p className={styles.Label}>{labelInput}</p> : undefined}
      <TextField value={toFarsiNumber(_value)}
        type={type}
        className={styles.inputImpo + " " + className}
        fullWidth
        placeholder={placeholder}
        multiline={multiline}
        rows={rows}
        variant="filled"
        InputProps={{
          startAdornment: (
            icon ?
              <InputAdornment position={align === "left" ? "start" : "end"} onClick={iconFun}>
                {icon}
              </InputAdornment> : null
          ),
        }}
        inputProps={{
          sx: {
            '&::placeholder': {
              color: '#1D1A22',
              fontSize: '16px',
              opacity: 1,
              letterSpacing: '-0.04em',
              fontVariationSettings: "'wght' 350"

            },
          },
          maxLength
        }}
        onChange={onTextFieldChange}
        {...rest} />
    </>
  );
}

export default Input;
