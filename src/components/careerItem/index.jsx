import styles from "./career.module.css"
import Typography from "@/shared/Typography"
import Link from "next/link"
export default function Career({ jobTitle, jobTime, companyName, url }) {
    return (
        <>
            <div className={styles.careerItem}>
                <div className={styles.texts}>
                    <Typography
                        size="medium"
                        type="title"
                        color="#48464A"
                        component="h4"
                    >
                        {jobTitle}
                    </Typography>
                    <Typography
                        size="large"
                        type="body"
                        color="#48464A"
                        component="p"
                    >
                        {jobTime}
                    </Typography>
                    <Typography
                        size="small"
                        type="body"
                        color="#48464A"
                        component="p"
                    >
                        {companyName}
                    </Typography>
                </div>
                <div className={styles.linkJob}>
                    <Link href={`job/${url}`} className={styles.readMore} aria-label="link">
                        اطلاعات بیشتر
                    </Link>
                </div>
            </div>
        </>
    )
}