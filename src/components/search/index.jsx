import styles from "./search.module.css";
import Typography from "@/shared/Typography";
import Image from 'next/image';
import Link from "next/link";
import Input from '@/components/input';
import { useState } from "react";
import axios from '@/utils/axios';
import loading from "@/assets/images/loading.svg";
import searchLine from "@/assets/images/searchLine.svg";
import { IconButton } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';

export default function search({ blog }) {

    const [resultSearch, setSearchResponse] = useState([]);
    const [isSearch, setIsSearch] = useState(false);
    const [wordSearch, setWordSearch] = useState(undefined);
    const realTimeSearch = async (word) => {
        setWordSearch(word)
        if (wordSearch?.length >= 2) {
            setIsSearch(true)
            const respones = await axios.get(`article/articles/search/${wordSearch}`)
            setSearchResponse(respones?.data.articles)
            setIsSearch(false)
        } else if (wordSearch?.length === 0) {
            setIsSearch(false)
        }
    }
    const resetSearch = () => {
        setWordSearch("")
        setIsSearch(false)
    }
    return (blog ?
        <section className={`${styles.searchBlog} ${styles.searchBlogPage}`}>
            <div>
                <Input placeholder="جستجو مقاله" className={styles.inputSearch} onChange={(word) => { realTimeSearch(word) }} value={wordSearch} />
                {isSearch ?
                    <Image src={loading} alt="لودینگ" width="25" height="auto" className={styles.searchIconBlog} /> : <Image src={searchLine} alt="جستجو" width="24" height="24" className={styles.searchIconBlog} />
                }
                {wordSearch ? <><IconButton onClick={resetSearch} aria-label="reset" classes={{ root: styles.resetSearch }}><CloseIcon color="default" /></IconButton></> : undefined}
            </div>
            {wordSearch?.length >= 2 ?
                <div className={styles.resultSearch}>
                    {resultSearch?.length ?
                        <ul>
                            {resultSearch?.map((item, index) => {
                                return (
                                    <li key={index}>
                                        <Link href={item.url} aria-label="link">
                                            <Typography
                                                size="small"
                                                type="title"
                                                color="#938F94"
                                                component="span"
                                            >
                                                {item.title}
                                            </Typography>
                                        </Link>
                                    </li>
                                )
                            })
                            }
                        </ul> : <span className={styles.noResult}>
                            <Typography
                                size="small"
                                type="title"
                                color="#938F94"
                                component="span"
                            >
                                مقاله ای یافت نشد!
                            </Typography></span>
                    }
                </div> : undefined
            }
        </section > : <section className={`${styles.searchBlog} ${styles.searchBlogSingle}`}>
            <div>
                <Input placeholder="جستجوی مقاله،کلمه،بیماری و..." onChange={(word) => { realTimeSearch(word) }} />
                {isSearch ?
                    <Image src={loading} alt="لودینگ" width="25" height="auto" className={styles.searchIcon} /> : <Image src={searchLine} alt="جستجو" width="24" height="24" className={styles.searchIcon} />
                }
            </div>
            {wordSearch?.length >= 2 ?
                <div className={styles.resultSearch}>
                    {resultSearch?.length ?
                        <ul>
                            {resultSearch?.map((item, index) => {
                                return (
                                    <li key={index}>
                                        <Link href={item.url} aria-label="link">
                                            <Typography
                                                size="small"
                                                type="title"
                                                color="#938F94"
                                                component="span"
                                            >
                                                {item.title}
                                            </Typography>
                                        </Link>
                                    </li>
                                )
                            })
                            }
                        </ul> : <span className={styles.noResult}>
                            <Typography
                                size="small"
                                type="title"
                                color="#938F94"
                                component="span"
                            >
                                مقاله ای یافت نشد!
                            </Typography></span>
                    }
                </div> : undefined
            }
        </section >

    )
}