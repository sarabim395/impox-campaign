import { useEffect } from 'react';

export const usePreventPullDownReload = (id) => {
  useEffect(() => {
    const el = document?.getElementById(id);

    const preventPullToRefresh = (event) => {
      if (event.touches.length > 0) {
        event.preventDefault();
      }
    };

    el?.addEventListener('touchmove', preventPullToRefresh, { passive: false });

    return () => el?.removeEventListener('touchmove', preventPullToRefresh);
  }, []);
};
