import moment from 'jalali-moment';

export const addZero = (n) => {
  return ('0' + n).slice(-2);
};

const date = new Date();

export const getCurrentDate = () => {
  return date.getFullYear() + '-' + addZero(date.getMonth() + 1) + '-' + addZero(date.getDate());
};

export const getCurrentTime = () => {
  return addZero(date.getHours()) + ':' + addZero(date.getMinutes()) + ':' + addZero(date.getSeconds());
};

export const fromDate = () => {
  let fromDate = new Date();
  fromDate.setFullYear(fromDate.getFullYear() - 1);
  return `${fromDate.getFullYear()}-${addZero(fromDate.getMonth() + 1)}-${addZero(fromDate.getDate())}`;
};

export const toDate = () => {
  let toDate = new Date();
  toDate.setFullYear(toDate.getFullYear() + 1);
  return `${toDate.getFullYear()}-${addZero(toDate.getMonth() + 1)}-${addZero(toDate.getDate())}`;
};

export const totalDaysOfMonth = (selectedYear, selectedMonth) => {
  const modifiedDaysList = [...daysList];

  const yearAndMonth = `${selectedYear}/${monthsList.indexOf(selectedMonth) + 1}`;

  if (moment(`${yearAndMonth}/30`, 'jYYYY/jM/jD').isValid()) {
    modifiedDaysList.push('30');
  }

  if (moment(`${yearAndMonth}/31`, 'jYYYY/jM/jD').isValid()) {
    modifiedDaysList.push('31');
  }

  return modifiedDaysList;
};

export const yearsList = [
  '1350',
  '1351',
  '1352',
  '1353',
  '1354',
  '1355',
  '1356',
  '1357',
  '1358',
  '1359',
  '1360',
  '1361',
  '1362',
  '1363',
  '1364',
  '1365',
  '1366',
  '1367',
  '1368',
  '1369',
  '1370',
  '1371',
  '1372',
  '1373',
  '1374',
  '1375',
  '1376',
  '1377',
  '1378',
  '1379',
  '1380',
  '1381',
  '1382',
  '1383',
  '1384',
  '1385',
  '1386',
  '1387',
  '1388',
  '1389',
  '1390',
  '1391',
  '1392',
  '1393',
  '1394',
  '1395',
  '1396',
  '1397',
  '1398',
  '1399',
  '1400',
  '1401',
  '1402',
];

export const monthsList = [
  'فروردین',
  'اردیبهشت',
  'خرداد',
  'تیر',
  'مرداد',
  'شهریور',
  'مهر',
  'آبان',
  'آذر',
  'دی',
  'بهمن',
  'اسفند',
];

export const daysList = [
  '01',
  '02',
  '03',
  '04',
  '05',
  '06',
  '07',
  '08',
  '09',
  '10',
  '11',
  '12',
  '13',
  '14',
  '15',
  '16',
  '17',
  '18',
  '19',
  '20',
  '21',
  '22',
  '23',
  '24',
  '25',
  '26',
  '27',
  '28',
  '29',
];
