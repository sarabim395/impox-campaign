
export function toFarsiNumber(n, useComma = false) {
    const farsiDigits = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    const text = useComma ? addCommas(n) : n;
    const result = text?.toString().replace(/\d/g, (x) => farsiDigits[x]);
    return result;
}

export function separate(Number) {
    Number += '';
    Number = Number.replace(',', '');
    var x = Number.split('.');
    var y = x[0];
    var z = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;

    while (rgx.test(y)) y = y.replace(rgx, '$1' + ',' + '$2');
    return y + z;
}

export const toPersianDate = (date, options) => {
    let isoDate = date;
    var d = new Date(isoDate);

    return (
        d.toLocaleDateString('fa-IR', options)
    )
}

export const excerptText = (text, length = 36, alt = "...") => {
    return (
        text?.substr(0, length) + (text?.length > length ? alt : "")
    )
}

export const convertNumbers2English = (string) => {
    string = String(string)
    return string?.replace(/[\u0660-\u0669]/g, function (c) {
        return c?.charCodeAt(0) - 0x0660;
    })?.replace(/[\u06f0-\u06f9]/g, function (c) {
        return c?.charCodeAt(0) - 0x06f0;
    });
}

export const recognizeCharacters = (text) => {
    const englishPattern = /^[a-zA-Z\s]*$/;
    const persianPattern = /^[\u0600-\u06FF\s]*$/;
    const numbers = text.match(/\d+(\.\d+)?/g);
    if (englishPattern.test(text) || persianPattern.test(text)) {
        return text.replaceAll(text, "")
    } else if (numbers) {
        if (numbers.length > 1) {
            return Number(numbers[0] + numbers[1])
        } else {
            return Number(numbers[0])
        }
    } else {
        return text;
    }
}

export function convertMonth(mounth) {
    if (mounth == 'فروردین') {
        return "01";
    }
    if (mounth == 'اردیبهشت') {
        return "02";
    }
    if (mounth == 'خرداد') {
        return "03";
    }
    if (mounth == 'تیر') {
        return "04";
    }
    if (mounth == 'مرداد') {
        return "05";
    }
    if (mounth === 'شهریور') {
        return "06";
    }
    if (mounth == 'مهر') {
        return "07";
    }
    if (mounth == 'آبان') {
        return "08";
    }
    if (mounth == 'آذر') {
        return "09";
    }
    if (mounth == 'دی') {
        return "10";
    }
    if (mounth == 'بهمن') {
        return "11";
    }
    if (mounth == 'اسفند') {
        return "12";
    }
}
