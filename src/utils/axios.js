import axios from 'axios';

const URL = 'https://newadmin.impo.app/api/v1/';
export const PICURL = `${URL}article/image/`;

const instance = axios.create({
    baseURL: URL,
});
instance.interceptors.response.use(
    (response) => {
        if (response?.status === 200) {
            return response;
        }
    }, (error) => {
        return Promise.reject(error.response.status);
    }
);

export default instance;


