// symphaty Image
import symphatyCycle from "../assets/images/symphatyCycle.webp";
import ImproveSex from "../assets/images/improveSex.webp";
import ImproveRelationship from "../assets/images/improveRelationship.webp";
import PreventionPregnancy from "../assets/images/preventionPregnancy.webp";
import EmpathizeMessage from "../assets/images/empathizeMessage.webp";
import EmpathizeDiary from "../assets/images/empathizeDiary.webp";
import EmpathizePhysical from "../assets/images/empathizePhysical.webp";
import ActivateNumberLevel1 from "../assets/images/activateNumberLevel1.webp";
import ActivateNumberLevel2 from "../assets/images/activateNumberLevel2.webp";
import ActivateNumberLevel3 from "../assets/images/activateNumberLevel3.webp";
import ActivateNumberLevel4 from "../assets/images/activateNumberLevel4.webp";
import ActivateCodeLevel1 from "../assets/images/activateCodeLevel1.webp";
import ActivateCodeLevel2 from "../assets/images/activateCodeLevel2.webp";
import ActivateCodeLevel3 from "../assets/images/activateCodeLevel3.webp";
import ActivateCodeLevel4 from "../assets/images/activateCodeLevel4.webp";
import ActivateCodeLevel5 from "../assets/images/activateCodeLevel5.webp";
import ActivateCodeLevel6 from "../assets/images/ctivateCodeLevel6Slider.webp";
//prevention Image
import preventionForecast from "../assets/images/preventionForecast.webp";
import preventionpossibilityPregnancy from "../assets/images/preventionpossibilityPregnancy.webp";
import preventionReminder from "../assets/images/preventionReminder.webp";
import preventionAdvice from "../assets/images/preventionAdvice.webp";
import preventionNotinstallLevel1 from "../assets/images/preventionNotinstallLevel1.webp";
import preventionNotinstallLevel2 from "../assets/images/preventionNotinstallLevel2.webp";
import preventionNotinstallLevel3 from "../assets/images/preventionNotinstallLevel3.webp";
import preventioninstallLevel1 from "../assets/images/preventioninstallLevel1.webp";
import preventioninstallLevel2 from "../assets/images/preventioninstallLevel2.webp";
import preventioninstallLevel3 from "../assets/images/preventioninstallLevel3.webp";
import preventioninstallLevel4 from "../assets/images/preventioninstallLevel4.webp";
import preventioninstallLevel5 from "../assets/images/preventioninstallLevel5.webp";
import preventionForecast1 from "../assets/images/preventionForecast1.webp";
import preventionAdvice1 from "../assets/images/preventionAdvice1.webp";
// care image
import careGrowth from "../assets/images/careGrowth.webp";
import careGestationalAge from "../assets/images/careGestationalAge.webp";
import careExpertAdvice from "../assets/images/careExpertAdvice.webp";
import careReminder from "../assets/images/careReminder.webp";
import careWomenSpecialist from "../assets/images/careWomenSpecialist.webp";
import careEmpathy from "../assets/images/careEmpathy.webp";
import careExpertAdvice1 from "../assets/images/careExpertAdvice1.webp";
import careReminder1 from "../assets/images/careReminder1.webp";
import careWomenSpecialist1 from "../assets/images/careWomenSpecialist1.webp";
import careEmpathy1 from "../assets/images/careEmpathy1.webp";
import careCalculationPregnancyLevel1 from "../assets/images/careCalculationPregnancyLevel1.webp";
import careCalculationPregnancyLevel2 from "../assets/images/careCalculationPregnancyLevel2.webp";
// calendar image
import calendarDaysPregnancy from "../assets/images/calendarDaysPregnancy.webp";
import calendarPerioDay from "../assets/images/calendarPerioDay.webp";
import calendarPMS from "../assets/images/calendarPMS.webp";
import calendarCycle from "../assets/images/calendarCycle.webp";
import calendarDayOvulation from "../assets/images/calendarDayOvulation.webp";
import calendarPersonalAdvice from "../assets/images/calendarPersonalAdvice.webp";
import calendarSoonerPeriod from "../assets/images/calendarSoonerPeriod.webp";
//  impo men slider
import smsCodeImg from "../assets/images/smsCode.webp";
import installAppImg from "../assets/images/installApp.webp";
import signupAppImg from "../assets/images/signupApp.webp";
import sympathyAppImg from "../assets/images/sympathyApp.webp";
import acceptRequestImg from "../assets/images/acceptRequest.webp";
import accessAcceptImg from "../assets/images/accessAccept.webp";
import subcribtionImg from "../assets/images/subcribtion.webp";
import useAppImg from "../assets/images/useApp.webp";
//
import step1 from "../assets/images/step1.webp";
import step2 from "../assets/images/step2.webp";
import step3 from "../assets/images/step3.webp";
import step4 from "../assets/images/step4.webp";
import step5 from "../assets/images/step5.webp";
import step6 from "../assets/images/step6.webp";
import step7 from "../assets/images/step7.webp";
import step8 from "../assets/images/step8.webp";
import step9 from "../assets/images/step9.webp";
// articleMag
import articleImgMag from "../assets/images/articleImgMag.webp";

const Img = {
  // symphaty Image
  symphatyCycle: symphatyCycle,
  symphatyCycleAlt: "دیدن چرخه و بیوریتم همدیگه",
  ImproveSex: ImproveSex,
  ImproveSexAlt: "بهبود رابطه جنسی",
  ImproveRelationship: ImproveRelationship,
  ImproveRelationshipAlt: "دریافت پیام‌های بهبود رابطه",
  PreventionPregnancy: PreventionPregnancy,
  PreventionPregnancyAlt: "پیشگیری از بارداری یا اقدام به بارداری",
  EmpathizeMessage: EmpathizeMessage,
  EmpathizeMessageAlt: "ارسال پیام به همدل",
  EmpathizeDiary: EmpathizeDiary,
  EmpathizeDiaryAlt:
    "تو قسمت همدل ایمپو،خاطره‌های دو نفره قشنگتون رو با عکس و متن ثبت کنید",
  EmpathizePhysical: EmpathizePhysical,
  EmpathizePhysicalAlt:
    "تو قسمت همدل ایمپو می‌تونید چرخه قاعدگی خانم یا چرخه بیوریتم آقا رو ببینید و حسابی مراقب حال دل همدیگه باشید.",
  ActivateNumberLevel1: ActivateNumberLevel1,
  ActivateNumberLevel1Alt: "شروع همدلی و اضافه کردن پارتنر",
  ActivateNumberLevel2: ActivateNumberLevel2,
  ActivateNumberLevel2Alt:
    "شماره موبایل یا ایمیل پارتنرت رو وارد کادر مشخص شده میکنی",
  ActivateNumberLevel3: ActivateNumberLevel3,
  ActivateNumberLevel3Alt: "منتظر میمونی تا پارتنرت درخواست همدلیت رو قبول کنه",
  ActivateNumberLevel4: ActivateNumberLevel4,
  ActivateNumberLevel4Alt:
    "بعد از تایید همدلی از سمت پارتنرت می‌تونید وضعیت همدیگه رو ببینید",
  ActivateCodeLevel1: ActivateCodeLevel1,
  ActivateCodeLevel1Alt: "شروع همدلی و اضافه کردن پارتنر",
  ActivateCodeLevel2: ActivateCodeLevel2,
  ActivateCodeLevel2Alt:
    "شماره موبایل یا ایمیل پارتنرت رو وارد کادر مشخص شده میکنی",
  ActivateCodeLevel3: ActivateCodeLevel3,
  ActivateCodeLevel3Alt: "منتظر میمونی تا پارتنرت درخواست همدلیت رو قبول کنه",
  ActivateCodeLevel4: ActivateCodeLevel4,
  ActivateCodeLevel4Alt:
    "بعد از تایید همدلی از سمت پارتنرت می‌تونید وضعیت همدیگه رو ببینید",
  ActivateCodeLevel5: ActivateCodeLevel5,
  ActivateCodeLevel5Alt: "منتظر میمونی تا پارتنرت درخواست همدلیت رو قبول کنه",
  ActivateCodeLevel6: ActivateCodeLevel6,
  ActivateCodeLevel6Alt:
    "بعد از تایید همدلی از سمت پارتنرت می‌تونید وضعیت همدیگه رو ببینید",
  //prevention Image
  preventionForecast: preventionForecast,
  preventionForecastAlt: "می تونی پیش‌بینی دوره باروری ، ماه‌های بعدی رو ببینی",
  preventionpossibilityPregnancy: preventionpossibilityPregnancy,
  preventionpossibilityPregnancyAlt:
    "روزی که بیشترین احتمال بارداری وجود داره رو متوجه میشی",
  preventionReminder: preventionReminder,
  preventionReminderAlt:
    "بهت یادآوری میکنیم که دوره باروری و روز تخمک گذاری چه روزیه",
  preventionAdvice: preventionAdvice,
  preventionAdviceAlt: "توصیه‌هایی متناسب با روزهای باروری دریافت میکنی",
  preventionForecast1: preventionForecast1,
  preventionForecast1Alt:
    "بهت یادآوری میکنیم که دوره باروری و روز تخمک گذاری چه روزیه",
  preventionAdvice1: preventionAdvice1,
  preventionAdvice1Alt: "توصیه‌هایی متناسب با روزهای باروری دریافت میکنی",
  preventionNotinstallLevel1: preventionNotinstallLevel1,
  preventionNotinstallLevel1Alt:
    "اگه همیشه قبل یا بعد خونریزی لکه بینی هم داری، روزهای لکه بینی هم جز روزهای پریود حساب میشه",
  preventionNotinstallLevel2: preventionNotinstallLevel2,
  preventionNotinstallLevel2Alt:
    "طول سیکل پریود یعنی فاصله بین شروع دو تا پریود پشت سر هم.  ",
  preventionNotinstallLevel3: preventionNotinstallLevel3,
  preventionNotinstallLevel3Alt:
    "برای پیش‌بینی دوره بعدی پریود، لازمه که بهمون بگی آخرین باری که پریود شدی چه روزی بوده",
  preventioninstallLevel1: preventioninstallLevel1,
  preventioninstallLevel1Alt:
    "با انتخاب پروفایل از قسمت بالا سمت چپ صفحه اصلی، می‌تونی طول دوره و طول پریودی که قبلا انتخاب کردی رو ببینی",
  preventioninstallLevel2: preventioninstallLevel2,
  preventioninstallLevel2Alt:
    "طول دوره یعنی فاصله بین شروع دو تا پریود پشت سر هم. اگه لازمه ویرایشش کن",
  preventioninstallLevel3: preventioninstallLevel3,
  preventioninstallLevel3Alt:
    "اگه زودتر یا دیرتر پریود شدی، از قسمت تغییرات من در صفحه اصلی، دکمه‌های پریود شدم و هنوز پریود نشدم رو انتخاب کن",
  preventioninstallLevel4: preventioninstallLevel4,
  preventioninstallLevel4Alt:
    "اگه فراموش کردی همون روزی که تاریخ پریودت جابجا شده، به اپ سر بزنی می‌تونی بعدا از قسمت تغییرات من، تنظیمات رو انتخاب کنی",
  preventioninstallLevel5: preventioninstallLevel5,
  preventioninstallLevel5Alt:
    "تاریخ شروع و پایان آخرین باری که پریود شدی رو اینجا انتخاب میکنی. اتمام دوره یعنی آخرین روز قبل از شروع پریود بعدی",
  // care Image
  careGrowth: careGrowth,
  careGrowthAlt: "وضعیت رشد جنین در هر هفته",
  careGestationalAge: careGestationalAge,
  careGestationalAgeAlt: "محاسبه سن بارداری",
  careExpertAdvice: careExpertAdvice,
  careExpertAdviceAlt: "توصیه‌های تخصصی",
  careReminder: careReminder,
  careReminderAlt: "یادآوری زمان‌های مهم",
  careWomenSpecialist: careWomenSpecialist,
  careWomenSpecialistAlt: "دسترسی به متخصص زنان، زایمان و نازایی",
  careEmpathy: careEmpathy,
  careEmpathyAlt: "همدلی همسرت در دوران بارداری",
  careExpertAdvice1: careExpertAdvice1,
  careReminder1: careReminder1,
  careWomenSpecialist1: careWomenSpecialist1,
  careEmpathy1: careEmpathy1,
  careCalculationPregnancyLevel1: careCalculationPregnancyLevel1,
  careCalculationPregnancyLevel1Alt:
    "پیش‌بینی پریود، pms و دوران باروری و امکان برنامه ریزی برای هر دوره",
  careCalculationPregnancyLevel2: careCalculationPregnancyLevel2,
  careCalculationPregnancyLevel2Alt:
    "محاسبه و یادآوری زمان تخمک گذاری و دوره باروری جهت برنامه ریزی رابطه جنسی بمنظور پیشگیری یا اقدام به بارداری",
  // calendar Image
  calendarDaysPregnancy: calendarDaysPregnancy,
  calendarDaysPregnancyAlt: "روزهای باروری",
  calendarPerioDay: calendarPerioDay,
  calendarPerioDayAlt: "روز‌های پریود",
  calendarPMS: calendarPMS,
  calendarPMSAlt: "روز‌های پی ام اس",
  calendarCycle: calendarCycle,
  calendarCycleAlt: "می‌تونی پریودهای قبلی و پیش‌بینی پریودهای بعدیت رو ببینی",
  calendarDayOvulation: calendarDayOvulation,
  calendarDayOvulationAlt: "می‌تونی روز تخمک گذاری رو ببینی",
  calendarPersonalAdvice: calendarPersonalAdvice,
  calendarPersonalAdviceAlt: "توصیه‌هایی میگیری که مخصوص خودته",
  calendarSoonerPeriod: calendarSoonerPeriod,
  calendarSoonerPeriodAlt: "اگه دیرتر یا زودتر پریود بشی، می‌تونی ثبتش کنی",
  // impo men slider
  smsCode: smsCodeImg,
  smsCodeAlt: "ارسال کدفعالسازی",
  //
  installApp: installAppImg,
  installAppAlt: "نصب اپلیکیشن",
  //
  signupApp: signupAppImg,
  signupAppAlt: "ثبت نام",
  //
  sympathyApp: sympathyAppImg,
  sympathyAppAlt: "همدلی",
  //
  acceptRequestApp: acceptRequestImg,
  acceptRequestAppAlt: "قبول همدلی",
  //
  accessAccept: accessAcceptImg,
  accessAcceptAlt: "دادن دسترسی به پارتنر",
  //
  subcribtion: subcribtionImg,
  subcribtionAlt: "خرید اشتراک",
  //
  useApp: useAppImg,
  useAppAlt: "خرید اشتراک",
  //
  step1: step1,
  step1Alt: "پیام پارتنر",
  //
  step2: step2,
  step2Alt: "لینک نصبی",
  //
  step3: step3,
  step3Alt: "وارد کردن شماره",
  //
  step4: step4,
  step4Alt: "ثبت نام",
  //
  step5: step5,
  step5Alt: "شروع همدلی",
  //
  step6: step6,
  step6Alt: "کد اختصاصی",
  //
  step7: step7,
  step7Alt: "قبول درخواست",
  //
  step8: step8,
  step8Alt: "خرید اشتراک",
  //
  step9: step9,
  step9Alt: "شروع همدلی",
  // articleMag
  articleImgMag: articleImgMag,
  articleImgMagAlt: "articleImgMag",
};
export default Img;
