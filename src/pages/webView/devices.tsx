import style from "./webView.module.css"
import Image from "next/image"
import devices from "@/assets/images/webView/mobile/devices.webp"
export default function Devices() {
    return (
        <>
            <div className={style.mobile}>
                <Image src={devices} alt="devices" />
            </div>
        </>
    )
}