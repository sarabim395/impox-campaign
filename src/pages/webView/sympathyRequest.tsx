import style from "./webView.module.css"
export default function SympathyRequest() {
    return (
        <>
            <div className={`${style.textTitle} ${style.items}`}>
                <p>اگه برای پارتنرت درخواست همدلی فرستادی ولی اون درخواستت رو نمی‌بینه ، احتمالا اپلیکینش آپدیت نیست
                    با آپدیت کردن ایمپو آقایان و بانوان هر دو نفر به آخرین نسخه، مشکل رفع خواهد شد
                </p>
            </div>
        </>
    )
}