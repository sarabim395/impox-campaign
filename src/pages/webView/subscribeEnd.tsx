import style from "./webView.module.css"
import Image from "next/image"
import end from "@/assets/images/webView/subscribeEnd/end.webp"

export default function SubscribeEnd() {
    return (
        <>
            <div className={`${style.items} ${style.subscribeEnd}`}>
                <h2>
                    اگه اشتراکت تموم شده بعد از بازکردن اپلیکیشن صفحه اشتراک
                    رو می‌بینی:
                </h2>
                <ul>
                    <li>طرحی رو که می‌خوای رو انتخاب کن</li>
                    <li>اشتراکت رو تمدید کن</li>
                </ul>
            </div>

            <Image src={end} alt="help" className={style.image} />
        </>
    )
}