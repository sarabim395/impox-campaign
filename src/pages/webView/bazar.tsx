import style from "./webView.module.css"
import b1 from "@/assets/images/webView/bazar/b1.webp"
import b2 from "@/assets/images/webView/bazar/b2.webp"
import b3 from "@/assets/images/webView/bazar/b3.webp"
import b4 from "@/assets/images/webView/bazar/b4.webp"
import b5 from "@/assets/images/webView/bazar/b5.webp"
import b6 from "@/assets/images/webView/bazar/b6.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function Bazar() {
    const items = [
        {
            title: "بعد از ورود به کافه بازار، رو پروفایلت بزن",
            img: b1
        },
        {
            title: "قسمت کیف پول و پرداخت رو انتخاب کن",
            img: b2
        },
        {
            title: "برو تو قسمت تراکنش‌ها",
            img: b3
        },
        {
            title: "و از اینجا توکن رو کپی کن",
            img: b4
        },
        {
            title: "بعد برگرد تو اپلیکیشن ایمپو",
            img: b5
        },
        {
            title: "و از این قسمت توکنی که کپی کردی رو وارد کن و دکمه ارسال توکن رو بزن.",
            img: b6
        },
    ]
    return (
        <>
            <div className={`${style.bazarItems} ${style.items}`}>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <Image src={item.img} alt="help" />
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
                <h2>با این کار اشتراکت فعال می‌شه</h2>
            </div>
        </>
    )
}