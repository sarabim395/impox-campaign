import style from "./webView.module.css"
import s1 from "@/assets/images/webView/addSympathy/img1.webp"
import s2 from "@/assets/images/webView/addSympathy/img2.webp"
import s3 from "@/assets/images/webView/addSympathy/img3.webp"
import s4 from "@/assets/images/webView/addSympathy/img4.webp"
import s5 from "@/assets/images/webView/addSympathy/img5.webp"
import s6 from "@/assets/images/webView/addSympathy/img6.webp"
import s7 from "@/assets/images/webView/addSympathy/img7.webp"
import s8 from "@/assets/images/webView/addSympathy/img8.webp"
import s9 from "@/assets/images/webView/addSympathy/img9.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function afterDeadline() {
    const items = [
        {
            title: "۱- شماره همراه",
            subTitle: 'از تب "همدل" دکمه "شروع همدلی و اضافه کردن پارتنر" رو انتخاب می‌کنی',
            img: s1
        },
        {
            subTitle: "شماره موبایل یا ایمیل پارتنرت رو وارد می‌کنی",
            img: s2,
        },
        {
            subTitle: "منتظر می‌مونی تا پارتنرت ایمپو آقایان رو نصب و درخواستت رو قبول کنه",
            img: s3,
        },
        {
            subTitle: "بعد از تایید همدلی از سمت پارتنرت، می‌تونید وضعیت همدیگه رو ببینید",
            img: s4,
        },
        {
            title: "۲- کد اختصاصی",
            subTitle: 'از تب "همدل" دکمه "شروع همدلی و اضافه کردن پارتنر" رو انتخاب می‌کنی',
            img: s5,
        },
        {
            subTitle: 'رو دکمه "استفاده از کد اختصاصی" میزنی',
            img: s6,
        },
        {
            subTitle: 'و بعد "اشتراک‌گذاری کد با همدل" رو انتخاب می‌کنی',
            img: s7,
        },
        {
            subTitle: 'کد اختصاصیت رو از طریق پیامک یا شبکه‌های اجتماعی برای پارتنرت می‌فرستی',
            img: s8,
        },
        {
            subTitle: 'بعد از نصب ایمپو آقایان و واردکردن کد اختصاصی تو توسط پارتنرت، درخواست همدلی رو از لیست درخواست‌ها تایید و دسترسی‌هایی که به همدلت می‌دی رو تایید می‌کنی',
            img: s9,
        },

    ]
    return (
        <>
            <div className={`${style.addSympathy} ${style.items}`}>
                <div className={style.textTitle}>
                    <h3>چطور تو همدل به پارتنرم وصل بشم؟</h3>
                    <p>برای وصل شدن ایمپو آقایان و بانوان به همدیگه و دیدن چرخه‎های هم و شورع همدلی دو تا راه وجود داره:</p>
                </div>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <h3>{item.subTitle}</h3>
                                    <Image src={item.img} className={style.image} alt="help" />
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}