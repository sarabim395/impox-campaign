import style from "./webView.module.css"
import Image from "next/image"
import xiaomi from "@/assets/images/webView/mobile/xiaomi.webp"
export default function Xiaomi() {
    return (
        <>
            <div className={style.mobile}>
                <Image src={xiaomi} alt="xiaomi" />

            </div>
        </>
    )
}