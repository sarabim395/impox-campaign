import style from "./webView.module.css"
import s1 from "@/assets/images/webView/subscribe/s1.webp"
import s2 from "@/assets/images/webView/subscribe/s2.webp"
import s3 from "@/assets/images/webView/subscribe/s3.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function Subscribe() {
    const items = [
        {
            title: "اگه هنوز اشتراک داری از اینجا وارد قسمت پروفایل شو",
            img: s1
        },
        {
            title: "روی دکمه تمدید اشتراک بزن",
            img: s2
        },
        {
            title: "طرحی که می‌خوای رو انتخاب کن و اشتراکت رو تمدید کن",
            img: s3
        },
    ]
    return (
        <>
            <div className={`${style.bazarItems} ${style.items}`}>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <Image src={item.img} alt="help" />
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
                <div className={style.textBox}>
                    این رو هم مدنظر داشته باش
                    اشتراک جدید که خریدی
                    بعد از تموم شدن اشتراک فعلیت، فعال می‌شه
                </div>
            </div>
        </>
    )
}