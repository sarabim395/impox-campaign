import style from "./webView.module.css"
import Image from "next/image"
import huawei from "@/assets/images/webView/mobile/huawei.webp"
export default function Huawei() {
    return (
        <>
            <div className={style.mobile}>
                <Image src={huawei} alt="huawei" />

            </div>
        </>
    )
}