import style from "./webView.module.css"
export default function Experiment() {
    return (
        <>
            <div className={`${style.textTitle} ${style.items}`}>
                <h3>نمی‌تونم آزمایش و سونوگرافیم رو برای پزشک بفرستم</h3>
                <p>
                    در این حالت فقط کافیه اپلیکیشنت رو به آخرین نسخه آپدیت کنی تا همتو بتونی برای پزشک فایل ارسال کنی و هم پزشک
                </p>
            </div>
        </>
    )
}