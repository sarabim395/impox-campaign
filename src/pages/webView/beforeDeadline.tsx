import style from "./webView.module.css"
import s1 from "@/assets/images/webView/beforeDeadline/img1.webp"
import s2 from "@/assets/images/webView/beforeDeadline/img2.webp"
import s3 from "@/assets/images/webView/beforeDeadline/img3.webp"
import s4 from "@/assets/images/webView/beforeDeadline/img4.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function BeforeDeadline() {
    const items = [
        {
            title: "اگه زودتر از زمانی که در ایمپو پیش‌بینی شده پریود شدی همون روزی که پریود شدی از صفحه اصلی روی دکمه ویرایش چرخه بزن",
            img: s1
        },
        {
            title: "و دکمه پریود شدم رو انتخاب کن",
            img: s2,
            img2: s3
        },
        {
            title: "در این حالت، چرخه و تقویم به اولین روز پریودت تغییر پیدا می‌کنه",
            img: s4
        },
    ]
    return (
        <>
            <div className={`${style.bazarItems} ${style.items}`}>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <Image src={item.img} className={style.image} alt="help" />
                                    {
                                        item.img2 ?
                                            <Image src={item.img2} className={style.image} alt="help" />
                                            : undefined
                                    }
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}