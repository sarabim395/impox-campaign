import style from "./webView.module.css"
export default function RemoveQuestion() {
    return (
        <>
            <div className={`${style.textTitle} ${style.items}`}>
                <h3>تقویمم بهم ریخته، چه‌کار کنم؟</h3>
                <p>ادرصورتی که منتظر پاسخ پزشک هستی، بلافاصله بعد از پاسخ دادن پزشک، سوال با تمام مراحل رفت و برگشتش برمی‌گرده، اما اگه خودت باید پاسخ پزشک رو بدی از قسمت پشتیبانی یک تیکت ارسال کن تا مشکل رو برات حل کنیم
                </p>
            </div>
        </>
    )
}