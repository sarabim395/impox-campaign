import style from "./webView.module.css"
import s1 from "@/assets/images/webView/changeCyclePeriodLength/img1.webp"
import s2 from "@/assets/images/webView/changeCyclePeriodLength/img2.webp"
import s3 from "@/assets/images/webView/changeCyclePeriodLength/img3.webp"
import s4 from "@/assets/images/webView/changeCyclePeriodLength/img4.webp"
import s5 from "@/assets/images/webView/changeCyclePeriodLength/img5.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function afterDeadline() {
    const items = [
        {
            title: "برای اینکه محاسبات پیش بینی پریود، pms ، دوره باروری و روز تخمک گذاری در ایمپو دقیق و باشه لازمه که طول دوره و طول پریودی که تو ایمپو وارد میکنی هم درست و دقیق باشه برای تغییر طول دوره و طول پریود ،از قسمت پروفایل:",
            img: s1
        },
        {
            title: "من و ایمپو رو انتخاب کن",
            img: s2,
        },
        {
            title: "حالا با انتخاب گزینه طول دوره، فاصله بین شروع دو پریودت رو انتخاب کن در حقیقت طول دوره نشون میده که هر چند وقت یک بار پریود میشی",
            img: s3,
            img2: s4
        },
        {
            title: "از این قسمت هم می‌تونی طول پریودت رو انتخاب کنی یا تغییر بدی",
            img: s5
        },
    ]
    return (
        <>
            <div className={`${style.bazarItems} ${style.items}`}>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <Image src={item.img} className={style.image} alt="help" />
                                    {
                                        item.img2 ?
                                            <Image src={item.img2} className={style.image} alt="help" />
                                            : undefined
                                    }
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}