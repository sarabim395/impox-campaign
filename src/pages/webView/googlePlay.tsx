import style from "./webView.module.css"
import googleLogo from "@/assets/images/webView/googlePlay/google.png"
import Image from "next/image"

export default function GooglePlay() {
    return (
        <>
            <div className={style.googlePlayItems}>
                <Image src={googleLogo} alt="logo" />
                <p>
                    اگه اپلیکیشنت رو از گوگل پلی دانلود و نصب کردی
                    و تراکنش انجام دادی و اشتراکت فعال نشده
                    اپلیکیشنت رو ببند
                    و بعد از چند دقیقه دوباره وارد شو
                    اگه باز هم اشتراک برات فعال نشده‌بود، مبلغ کسر شده تا حداکثر 72 ساعت بعد به حسابت برمی‌گرده و دوباره باید مراحل خرید اشتراک رو از اول انجام بدی

                </p>
            </div>
        </>
    )
}