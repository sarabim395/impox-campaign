import style from "./webView.module.css"
import m1 from "@/assets/images/webView/myket/m1.webp"
import m2 from "@/assets/images/webView/myket/m2.webp"
import m3 from "@/assets/images/webView/myket/m3.webp"
import m4 from "@/assets/images/webView/myket/m4.webp"
import m5 from "@/assets/images/webView/myket/m5.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function Bazar() {
    const items = [
        {
            title: "بعد از ورود به مایکت، رو پروفایلت بزن",
            img: m1
        },
        {
            title: "تراکنش‌های پرداخت رو انتخاب کن",
            img: m2
        },
        {
            title: "تراکنش مربوط به خرید اشتراک از ایمپو رو پیدا کن شماره توکن رو بخاطر بسپار",
            img: m3
        },
        {
            title: "بعد برگرد تو اپلیکیشن ایمپو",
            img: m4
        },
        {
            title: "و از این قسمت توکنی که کپی کردی رو وارد کن و دکمه ارسال توکن رو بزن.",
            img: m5
        },
    ]
    return (
        <>
            <div className={`${style.bazarItems} ${style.items}`}>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <Image src={item.img} alt="help" />
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
                <h2>با این کار اشتراکت فعال می‌شه</h2>
            </div>
        </>
    )
}