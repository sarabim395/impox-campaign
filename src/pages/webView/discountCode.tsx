import style from "./webView.module.css"
import s1 from "@/assets/images/webView/discountCode/dis1.webp"
import s2 from "@/assets/images/webView/discountCode/dis2.webp"
import s3 from "@/assets/images/webView/discountCode/dis3.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function DiscountCode() {
    const items = [
        {
            title: "تو صفحه خرید اشتراک از اینجا روی قسمت ثبت کد تخفیف برن",
            img: s1
        },
        {
            title: "کد تخفیفت رو تایپ کن روی دکمه اعمال کد تخفیف بزن",
            img: s2
        },
        {
            title: "و بعد از اعمال تخفیف رو دکمه پرداخت بزن",
            img: s3
        },
    ]
    return (
        <>
            <div className={`${style.bazarItems} ${style.items}`}>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <Image src={item.img} className={style.image} alt="help" />
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}