import style from "./webView.module.css"
import s1 from "@/assets/images/webView/awaitPayment/img1.webp"
import s2 from "@/assets/images/webView/awaitPayment/img2.webp"
import s3 from "@/assets/images/webView/awaitPayment/img3.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function AwaitPayment() {
    const items = [

        {
            subTitle: 'اگر پرداخت کامل شده، برو داخل برنامه، بخش کلینیک،  اگر سوال در حالت در انتظار پرداخته:',
            img: s1,
        },
        {
            subTitle: 'برو داخل قسمت "مشاهده همه" اگه علاوه بر سوال "در انتظار پرداخت" سوالی وجود داره که "در حال بررسی" هست، سوال فرستاده شده و نیاز به پرداخت دوباره نیست. ',
            img: s2,
        },
        {
            subTitle: 'سوال "درحال بررسی توسط پزشک پاسخ داده‌می‌شه و سوال "در انتظار پرداخت" بعد از 24 ساعت حذف می‌شه. اما اگه سوال "در حال بررسی" در قسمت "مشاهده همه" وجود نداشت، فرآیند کامل نشده و مبلغ کسر شده حداکثر بعد از 72 ساعت به حسابت برمی‌گرده',
            img: s3,
        },

    ]
    return (
        <>
            <div className={`${style.awaitPayment} ${style.items}`}>
                <div className={style.textTitle}>
                    <h3>سوال رو پرسیدم و مبلغش رو پرداخت کردم، اما هنوز در حالت در انتظار پرداخته</h3>
                </div>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.subTitle}</h2>
                                    <Image src={item.img} className={style.image} alt="help" />
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}