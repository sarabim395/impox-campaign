import style from "./webView.module.css"
import s1 from "@/assets/images/webView/afterDeadline/img1.webp"
import s2 from "@/assets/images/webView/afterDeadline/img2.webp"
import s3 from "@/assets/images/webView/afterDeadline/img3.webp"
import s4 from "@/assets/images/webView/afterDeadline/img4.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function afterDeadline() {
    const items = [
        {
            title: "در صورتی که به هر دلیلی پریودت عقب افتاد و دیرتر از زمانی که ایمپو پیش بینی کرده پریود شدی،  از صفحه اصلی روی دکمه ویرایش چرخه بزن",
            img: s1
        },
        {
            title: " و دکمه هنوز پریود نشدم رو انتخاب کن",
            img: s2,
            img2: s3
        },
        {
            title: "همینطور که می‌بینی خیلی سریع و راحت چرخه وضعیتی رو نشون می‌ده که هنوز پریود نشدی در صورتیکه روز بعد هم پریود نشدی، باز هم همین مراحل رو تا روزی که پریود شدی ادامه بده تا محاسبات دقیق و درست باشه",
            img: s4
        },
    ]
    return (
        <>
            <div className={`${style.bazarItems} ${style.items}`}>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <Image src={item.img} className={style.image} alt="help" />
                                    {
                                        item.img2 ?
                                            <Image src={item.img2} className={style.image} alt="help" />
                                            : undefined
                                    }
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}