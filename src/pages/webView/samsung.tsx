import style from "./webView.module.css"
import Image from "next/image"
import samsung from "@/assets/images/webView/mobile/samsung.webp"
export default function Samsung() {
    return (
        <>
            <div className={style.mobile}>
                <Image src={samsung} alt="samsung" />
            </div>
        </>
    )
}