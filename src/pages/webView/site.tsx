import style from "./webView.module.css"
import impoLogo from "@/assets/images/webView/site/impo.png"
import Image from "next/image"

export default function Site() {
    return (
        <>
            <div className={style.siteItems}>
                <Image src={impoLogo} alt="logo" />
                <p>
                    اگه اپلیکیشنت رو از سایت ایمپو و لینک مستقیم دانلود و نصب کردی
                    و تراکنش انجام دادی و اشتراکت فعال نشده
                    اپلیکیشنت رو ببند و بعد از چند دقیقه دوباره وارد شو
                    اگه باز هم اشتراک برات فعال نشده‌بود، مبلغ کسر شده تا حداکثر 72 ساعت بعد به حسابت برمی‌گرده و دوباره باید مراحل خرید اشتراک رو از اول انجام بدی.

                </p>
            </div>
        </>
    )
}