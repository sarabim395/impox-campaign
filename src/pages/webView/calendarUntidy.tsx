import style from "./webView.module.css"
export default function CalendarUntidy() {
    return (
        <>
            <div className={`${style.textTitle} ${style.items}`}>
                <h3>تقویمم بهم ریخته، چه‌کار کنم؟</h3>
                <p>اگر تاریخ پریودهات در تقویم بهم ریخته و مطابق تاریخی که قبلا ثبت کردی نیست، لازمه حداقل اطلاعات و تاریخ آخرین پریودت رو در تیکت پشتیبانی برامون ارسال کنی و یا در ساعات اداری با شماره پشتیبانی تماس بگیری تا مشکلت رو حل کنیم.</p>
            </div>
        </>
    )
}