import style from "./webView.module.css"
import s1 from "@/assets/images/webView/changeLastPeriod/img1.webp"
import s2 from "@/assets/images/webView/changeLastPeriod/img2.webp"
import s3 from "@/assets/images/webView/changeLastPeriod/img3.webp"
import s4 from "@/assets/images/webView/changeLastPeriod/img4.webp"
import s5 from "@/assets/images/webView/changeLastPeriod/img5.webp"
import Image from "next/image"
import Divider from '@mui/material/Divider';

export default function afterDeadline() {
    const items = [
        {
            title: "اگه مدت زیادیه که وارد ایمپو نشدی و تاریخ پریودت طبق پیش بینی ایمپو پیش نرفته  از صفحه اصلی روی دکمه ویرایش چرخه",
            img: s1
        },
        {
            title: " و بعد روی دکمه تنظیمات بزن",
            img: s2,
        },
        {
            title: "تاریخ شروع آخرین باری که پریود شدی",
            img: s3
        },
        {
            title: "و همینطور تاریخ پایان آخرین پریودت رو انتخاب کن و دکمه ویرایش چرخه رو بزن",
            img: s4
        },
        {
            title: "همینطور که می بینی چرخه و تقویم با تاریخ جدیدی که وارد کردی هماهنگ شده",
            img: s5
        },
    ]
    return (
        <>
            <div className={`${style.bazarItems} ${style.items}`}>
                {
                    items.map((item, i) => {
                        return (
                            <>
                                <div className={style.item} key={i}>
                                    <h2>{item.title}</h2>
                                    <Image src={item.img} className={style.image} alt="help" />
                                </div>
                                <Divider sx={{ borderWidth: 2, borderColor: "#F9F9F9" }} />
                            </>
                        )
                    })
                }
            </div>
        </>
    )
}