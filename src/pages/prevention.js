import Typography from "../shared/Typography";
import Head from "next/head";
import Appbar from "@/components/appbar";
import styles from "@/styles/prevention.module.css";
import { Grid, Divider, Hidden } from "@mui/material";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import Img from "@/image/image";
import Footer from "@/components/footer";
import Download from "@/components/download";
import Slider from "@/components/slider";
import BoxZigzager from "@/components/boxZigzager";

export default function prevention() {
  const sliderItemNumber = [
    {
      id: 1,
      levelText: "مرحله اول",
      text: `اگه همیشه قبل یا بعد خونریزی لکه بینی هم داری، روزهای لکه بینی هم جز روزهای پریود حساب میشه`,
      image: Img.preventionNotinstallLevel1,
      alt: Img.preventioninstallLevel1Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 2,
      levelText: "مرحله دوم",
      text: "طول سیکل پریود یعنی فاصله \n شروع دو تا پریود پشت سر هم.",
      image: Img.preventionNotinstallLevel2,
      alt: Img.preventionNotinstallLevel2Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 3,
      levelText: "مرحله سوم",
      text: `برای پیش‌بینی دوره بعدی پریود، لازمه که بهمون بگی آخرین باری که پریود شدی چه روزی بوده,`,
      image: Img.preventionNotinstallLevel3,
      alt: Img.preventionNotinstallLevel3Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
  ];

  const sliderItemCode = [
    {
      id: 1,
      levelText: "مرحله اول",
      text: `با انتخاب پروفایل از قسمت بالا سمت چپ صفحه اصلی، می‌تونی طول دوره و طول پریودی که قبلا انتخاب کردی رو ببینی,`,
      textMobile: `از تب همدل دکمه "شروع همدلی و اضافه کردن پارتنر" رو انتخاب میکنی`,
      image: Img.preventioninstallLevel1,
      alt: Img.preventioninstallLevel1Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 2,
      levelText: "مرحله دوم",
      text: `طول دوره یعنی فاصله بین شروع دو تا پریود پشت سر هم. اگه لازمه ویرایشش کن,`,
      textMobile: `در این قسمت،"اشتراک‌گذاری کد با همدل" رو انتخاب میکنی`,
      image: Img.preventioninstallLevel2,
      alt: Img.preventioninstallLevel2Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 3,
      levelText: "مرحله سوم",
      text: `اگه زودتر یا دیرتر پریود شدی، از قسمت تغییرات من در صفحه اصلی، دکمه‌های پریود شدم و هنوز پریود نشدم رو انتخاب کن`,
      textMobile: `و کد اختصاصیت رو با پیامک یا شبکه‌های اجتماعی براش میفرستی`,
      image: Img.preventioninstallLevel3,
      alt: Img.preventioninstallLevel3Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 4,
      levelText: "مرحله چهارم",
      text: `اگه فراموش کردی همون روزی که تاریخ پریودت جابجا شده، به اپ سر بزنی می‌تونی بعدا از قسمت تغییرات من، تنظیمات رو انتخاب کنی`,
      textMobile: `در بعد از واردکردن کد توسط پارتنرت باید درخواست همدلیش رو قبول کنی`,
      image: Img.preventioninstallLevel4,
      alt: Img.preventioninstallLevel4Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 5,
      levelText: "مرحله پنجم",
      text: "`تاریخ شروع و پایان آخرین باری که پریود شدی رو اینجا انتخاب میکنی. اتمام دوره یعنی آخرین روز قبل از شروع پریود بعدی",
      textMobile: `و دسترسی‌هایی که به همدلت میدی رو تایید کنی`,
      image: Img.preventioninstallLevel5,
      alt: Img.preventioninstallLevel5Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
  ];

  const boxZigZag = [
    {
      id: 1,
      labelText: `پیش‌بینی دوره باروری ماه‌های بعدی`,
      note: `چه قصد بارداری داری و چه پیشگیری از بارداری، با پیش‌بینی دوره باروری می‌تونی رابطه جنسی رو در زمان مناسبش انجام بدی `,
      image: Img.preventionForecast,
      alt: Img.preventionForecastAlt,
      md: 7,
      sm: 6,
      orderSM: 1,
      orderXS: 2,
      orderMD: 1,
      mdText: 5,
      smText: 6,
      orderSMText: 2,
      orderXSText: 1,
      orderMDText: 2,
      Style: "left",

    },
    {
      id: 2,
      labelText: `برنامه‌ریزی برای بارداری یا پیشگیری از آن`,
      note: `تو ایمپو می‌تونی روز تخمک گذاری رو ببینی و اگه قصد بارداری داری تو اون روز اقدام کنی`,
      image: Img.preventionpossibilityPregnancy,
      alt: Img.preventionpossibilityPregnancyAlt,
      md: 7,
      sm: 6,
      orderSM: 4,
      orderXS: 4,
      orderMD: 4,
      mdText: 5,
      smText: 6,
      orderSMText: 3,
      orderXSText: 3,
      orderMDText: 3,
      Style: "right",
      sw: "430",
      sh: "430"
    },
    {
      id: 3,
      labelText: `یادآوری دوره بارداری و روز تخمک‌گذاری`,
      note: `با اعلان‌های ایمپو ، از روز قبل بهت دوره باروری و روز تخمک گذاری رو یادآوری می کنیم تا حواست بیشتر جمع باشه`,
      image: Img.preventionReminder,
      alt: Img.preventionReminderAlt,
      md: 7,
      sm: 6,
      orderSM: 5,
      orderXS: 6,
      orderMD: 5,
      mdText: 5,
      smText: 6,
      orderSMText: 6,
      orderXSText: 5,
      orderMDText: 6,
      Style: "left",
    },
    {
      id: 4,
      labelText: `توصیه‌هایی متناسب با روزهای باروری`,
      note: `احتمال بارداری و کارهایی که باید در این دوره انجام بدی رو هر روز در قالب توصیه و پیام بهت میگیم`,
      image: Img.preventionAdvice,
      alt: Img.preventionAdviceAlt,
      md: 7,
      sm: 6,
      orderSM: 8,
      orderXS: 8,
      orderMD: 8,
      mdText: 5,
      smText: 6,
      orderSMText: 7,
      orderXSText: 7,
      orderMDText: 7,
      Style: "right",

    },
  ];

  const boxZigZagMobileSize = [
    {
      id: 1,
      labelText: `پیش‌بینی دوره باروری ماه‌های بعدی`,
      note: `چه قصد بارداری داری و چه پیشگیری از بارداری، با پیش‌بینی دوره باروری می‌تونی رابطه جنسی رو در زمان مناسبش انجام بدی `,
      image: Img.preventionForecast,
      alt: Img.preventionForecastAlt,
      md: 7,
      sm: 6,
      orderSM: 1,
      orderXS: 2,
      orderMD: 1,
      mdText: 5,
      smText: 6,
      orderSMText: 2,
      orderXSText: 1,
      orderMDText: 2,
      Style: "left",
    },
    {
      id: 2,
      labelText: `برنامه‌ریزی برای بارداری یا پیشگیری از آن`,
      note: `تو ایمپو می‌تونی روز تخمک گذاری رو ببینی و اگه قصد بارداری داری تو اون روز اقدام کنی`,
      image: Img.preventionpossibilityPregnancy,
      alt: Img.preventionpossibilityPregnancyAlt,
      md: 7,
      sm: 6,
      orderSM: 4,
      orderXS: 4,
      orderMD: 4,
      mdText: 5,
      smText: 6,
      orderSMText: 3,
      orderXSText: 3,
      orderMDText: 3,
      Style: "right",
    },
    {
      id: 3,
      labelText: `یادآوری دوره بارداری و روز تخمک‌گذاری`,
      note: `با اعلان‌های ایمپو ، از روز قبل بهت دوره باروری و روز تخمک گذاری رو یادآوری می کنیم تا حواست بیشتر جمع باشه`,
      image: Img.preventionForecast1,
      alt: Img.preventionForecast1Alt,
      md: 7,
      sm: 6,
      orderSM: 5,
      orderXS: 6,
      orderMD: 5,
      mdText: 5,
      smText: 6,
      orderSMText: 6,
      orderXSText: 5,
      orderMDText: 6,
      Style: "left",
      className: "customSpace",
      class2: "customSpace2"
    },
    {
      id: 4,
      labelText: `توصیه‌هایی متناسب با روزهای باروری`,
      note: `احتمال بارداری و کارهایی که باید در این دوره انجام بدی رو هر روز در قالب توصیه و پیام بهت میگیم`,
      image: Img.preventionAdvice1,
      alt: Img.preventionAdvice1Alt,
      md: 7,
      sm: 6,
      orderSM: 8,
      orderXS: 8,
      orderMD: 8,
      mdText: 5,
      smText: 6,
      orderSMText: 7,
      orderXSText: 7,
      orderMDText: 7,
      Style: "right",
      className: "customSpace",
      class2: "customSpace2"
    },
  ];

  const titleSlider = "چه کار کنیم که ایمپو دوره باروری و روز تخمک گذاری رو دقیق پیش‌بینی کنه؟";
  const titleDescription = "برای پیش‌بینی دقیق دوره باروری، لازمه که اطلاعات اولیه ای که به ایمپو میدی ، درست و دقیق باشه";
  const btn1 = "هنوز ایمپو رو نصب نکردم";
  const btn2 = "کاربر ایمپو هستم";
  return (
    <>
      <Head>
        <title>پیشگیری از بارداری یا اقدام به بارداری</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Appbar darkmode={true} />
      {/* box zig zag */}
      <article className={styles.paddingTop}>
        <Grid container justifyContent={"center"}>
          <Grid item md={12} className={`${styles.textAlignCenterPrevention} `}>
            <Divider className={styles.Dividerstyle1} />
            <div className={styles.paddingBottomPreventionPrevention}>
              <Typography
                type="display"
                size="medium"
                className={styles.styleTex1Prevention}
              >
                پیشگیری از بارداری یا اقدام به بارداری؟
              </Typography>
              <Typography className={styles.styleMobileSizePrevention}>
                ایمپو بهترین زمان برای رابطه جنسی رو بهت میگه
              </Typography>
            </div>
          </Grid>
          <Divider className={styles.Dividerstyle2} />

        </Grid>
        <Hidden smDown>
          <BoxZigzager boxZigZag={boxZigZag} />
        </Hidden>
        <Hidden smUp>
          <BoxZigzager boxZigZag={boxZigZagMobileSize} />
        </Hidden>
      </article>
      <Slider
        sliderItemNumber={sliderItemNumber}
        sliderItemCode={sliderItemCode}
        titleSlider={titleSlider}
        titleDescription={titleDescription}
        btn1={btn1}
        btn2={btn2}
      />
      <span id='download'></span>

      <Download />
      <Footer />
    </>
  );
}
