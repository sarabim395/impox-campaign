import Appbar from "@/components/appbarMain"
import Footer from "@/components/footer"
import Head from "next/head"
import style from "@/styles/singleJob.module.css"
import Typography from "@/shared/Typography"
import { JobInfo } from "@/constancData/jobs"
import { useRouter } from 'next/router'
import Input from '@/components/input'
import Button from "@mui/material/Button"
import axios from '@/utils/axios'
import { useState } from "react"
import CircularProgress from '@mui/material/CircularProgress'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import { enqueueSnackbar } from 'notistack';
import { convertNumbers2English } from "@/utils/index"
export default function jobUrl() {
    const [name, setName] = useState("")
    const [phone, setPhone] = useState("")
    const [email, setEmail] = useState("")
    const [fileNameBtn, setFileNameBtn] = useState("")
    const [fileName, setFileName] = useState("")
    const [loading, setLoading] = useState(false)
    const [open, setOpen] = useState(false)
    const router = useRouter()
    const info = JobInfo.filter((item) => item.url == router.query.url)[0]
    const sendResume = async () => {
        const firstTwoChars = convertNumbers2English(phone?.slice(0, 2));
        if (name && phone && email && fileName) {
            console.log(firstTwoChars)
            if (firstTwoChars == "09") {
                const data = {
                    userName: name,
                    phoneNumber: phone,
                    email: email,
                    titleJob: info?.title,
                    fileName: fileName
                }
                const response = await axios.post(`resume`, data)
                if (response.data.valid) {
                    setOpen(true)
                } else {
                    enqueueSnackbar("عملیات با خطا مواجه گردید", { variant: 'error' })
                }
            } else {
                enqueueSnackbar("فرمت شماره موبایل اشتباه است", { variant: 'error' })
            }
        } else {
            enqueueSnackbar("لطفا فیلدهای مورد نیاز را کامل نمایید", { variant: 'error' })
        }
    }
    const uploadFile = async (value) => {
        setLoading(true)
        const fileName = value.target.files[0];
        setFileNameBtn(fileName.name)
        const formData = new FormData();
        formData.append("files", fileName);
        const response = await axios({
            method: "post",
            url: "resume/file",
            data: formData,
            headers: { "Content-Type": "multipart/form-data" },
        });
        try {
            if (response.data.valid) {
                setLoading(false)
                setFileName(response.data.name)
            }
        } catch (error) {
            console.log(error)
        }
    }

    const handleClose = () => {
        setOpen(false)
        window.location.reload()
    }
    return (
        <>
            <Head>
                <title>{info?.title}</title>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
                <meta name="robots" content="noindex,nofollow" />
            </Head>
            <Appbar />
            <div className={style.mainJob}>
                <Typography
                    size="small"
                    type="headline"
                    color="#1C1B1E"
                    component="h3"
                    className={style.titlePage}
                >
                    فرصت های شغلی ایمپو
                </Typography>
                <section className={`${style.infoJob} ${style.backgroundSection}`}>
                    <div className={style.title}>
                        <Typography
                            size="medium"
                            type="title"
                            color="#1C1B1E"
                            component="h3"
                            className={style.generalTitle}
                        >
                            {info?.title}
                        </Typography>
                        <Typography
                            size="large"
                            type="body"
                            color="#1C1B1E"
                            component="p"
                        >
                            {info?.describtion}
                        </Typography>
                    </div>
                    <div className={style.skills}>
                        <ul>
                            <li>
                                <span>مدرک تحصیلی</span>
                                <span>{info?.fieldStudy}</span>
                            </li>
                            <li>
                                <span>مقطع تحصیلی</span>
                                <span>{info?.grade}</span>
                            </li>
                            <li>
                                <span>جنسیت</span>
                                <span>{info?.gender}</span>
                            </li>
                            <li>
                                <span>سابقه کار</span>
                                <span>{info?.workExperience}</span>
                            </li>
                        </ul>
                    </div>
                </section>
                <section className={`${style.tasks} ${style.backgroundSection}`}>
                    <Typography
                        size="small"
                        type="headline"
                        color="#1C1B1E"
                        component="h3"
                        className={style.generalTitle}
                    >
                        مسئولیت های اصلی
                    </Typography>
                    <ul>
                        {
                            info?.tasks.map((item) => {
                                return (
                                    <li>
                                        <Typography
                                            size="large"
                                            type="body"
                                            color="#1C1B1E"
                                            component="h3"
                                            className={style.task}
                                        >
                                            {item}
                                        </Typography>

                                    </li>
                                )
                            })
                        }
                    </ul>
                </section>
                {
                    info?.benefits &&
                    <section className={`${style.tasks} ${style.backgroundSection}`} style={{ marginTop: "2rem" }}>
                        <Typography
                            size="small"
                            type="headline"
                            color="#1C1B1E"
                            component="h3"
                            className={style.generalTitle}
                        >
                            موارد ذیل مزیت محسوب می‌شوند:
                        </Typography>
                        <ul>
                            {
                                info?.benefits?.map((item) => {
                                    return (
                                        <li>
                                            <Typography
                                                size="large"
                                                type="body"
                                                color="#1C1B1E"
                                                component="h3"
                                                className={style.task}
                                            >
                                                {item}
                                            </Typography>

                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </section>
                }

                <section className={`${style.requestForm} ${style.backgroundSection}`}>
                    <Typography
                        size="small"
                        type="headline"
                        color="#1C1B1E"
                        component="h3"
                        className={style.generalTitle}
                    >
                        فرم درخواست همکاری
                    </Typography>
                    <div className={style.form}>
                        <Input type="text" onChange={(value) => { setName(value) }} name="name" placeholder="نام‌ و‌ نام‌ خانوادگی" />
                        <Input type="text" onChange={(value) => { setPhone(value) }} name="phoneNumber" placeholder="شماره تماس" />
                        <Input type="email" onChange={(value) => { setEmail(value) }} name="email" placeholder="ایمیل" />
                        <div className={style.fileImport}>
                            <input type="file" id="resumeFile" onChange={(value) => { uploadFile(value) }} style={{ display: "none" }} />
                            <label for="resumeFile" className={style.resumeFile}>
                                {
                                    loading ?
                                        <CircularProgress color="primary" size={25} /> :
                                        fileNameBtn ? fileNameBtn :
                                            <>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
                                                    <path d="M4 16.5V17.5C4 18.2956 4.31607 19.0587 4.87868 19.6213C5.44129 20.1839 6.20435 20.5 7 20.5H17C17.7956 20.5 18.5587 20.1839 19.1213 19.6213C19.6839 19.0587 20 18.2956 20 17.5V16.5M16 8.5L12 4.5M12 4.5L8 8.5M12 4.5V16.5" stroke="#EC407A" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                </svg>
                                                <span>
                                                    فایل رزومه
                                                </span>
                                            </>
                                }
                            </label>
                        </div>
                        <div className={style.btnBox}>
                            <Button variant="contained" aria-label="btnName" onClick={sendResume}>ارســـال</Button>
                        </div>
                    </div>
                </section>
            </div >
            <Footer />

            <Dialog open={open} className={style.acceptModal}>
                <div className={style.modalFrame}>
                    <Typography
                        size="large"
                        type="body"
                        color="#1C1B1E"
                        component="p"
                    >
                        درخواست شما با موفقیت ثبت شد.
                        همکاران ما در اسرع وقت با شما تماس خواهند گرفت.
                    </Typography>
                </div>
                <DialogActions>
                    <Button variant="outlined" onClick={handleClose}>متوجه شدم</Button>
                </DialogActions>
            </Dialog>
        </>
    )
}