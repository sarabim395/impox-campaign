import { useState } from "react";
import Typography from "../shared/Typography";
import Head from "next/head";
import styles from "@/styles/care.module.css";
import Image from "next/image";
import Img from "@/image/image";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { Grid, Divider, Hidden } from "@mui/material";
import Footer from "@/components/footer";
import Download from "@/components/download";
import Appbar from "@/components/appbar";
import BoxZigzager from "@/components/boxZigzager";

export default function care() {
  const [mockap, setState] = useState(2);
  const changeMockap = (item) => {
    setState(item);
  };
  const sliderItem = [
    {
      id: 1,
      levelText: `بر اساس تاریخ آخرین پریود`,
      text: `تاریخ اولین روز آخرین باری که پریود شدی رو وارد میکنی و ایمپو برات هفته بارداری و روز زایمان رو مشخص میکنه`,
      textMobile: `از تب همدل دکمه "شروع همدلی و اضافه کردن پارتنر" رو انتخاب میکنی`,
      image: Img.careCalculationPregnancyLevel1,
      alt: Img.careCalculationPregnancyLevel1Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 2,
      levelText: "بر اساس تاریخ زایمان",
      text: `اگه پزشک یا سونوگرافی بهت تاریخ زایمان داده باشه، تاریخش رو تو این قسمت وارد میکنی تا هفته بارداریت رو مشخص کنیم`,
      textMobile: `در این قسمت،"اشتراک‌گذاری کد با همدل" رو انتخاب میکنی`,
      image: Img.careCalculationPregnancyLevel2,
      alt: Img.careCalculationPregnancyLevel2Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
  ];

  const boxZigZag = [
    {
      id: 1,
      labelText: `وضعیت رشد جنین در هر هفته`,
      note: `شکل ظاهری، قد، وزن و وضعیت رشد جنین رو در هر هفته از بارداری میبینی   `,
      image: Img.careGrowth,
      alt: Img.careGrowthAlt,
      md: 7,
      sm: 6,
      orderSM: 1,
      orderXS: 2,
      orderMD: 1,
      mdText: 5,
      smText: 6,
      orderSMText: 2,
      orderXSText: 1,
      orderMDText: 2,
      Style: "left",
    },
    {
      id: 2,
      labelText: `محاسبه سن بارداری`,
      note: `بر اساس تاریخ آخرین پریودت، سن بارداری و روز زایمان رو برات مشخص میکنیم`,
      image: Img.careGestationalAge,
      alt: Img.careGestationalAgeAlt,
      md: 7,
      sm: 6,
      orderSM: 4,
      orderXS: 4,
      orderMD: 4,
      mdText: 5,
      smText: 6,
      orderSMText: 3,
      orderXSText: 3,
      orderMDText: 3,
      Style: "right",
    },
    {
      id: 3,
      labelText: `توصیه‌های تخصصی`,
      note: `بر حسب اینکه تو کدوم هفته بارداری هستی و چه علائمی داری، توصیه‌های تخصصی مخصوص خودت رو دریافت میکنی`,
      image: Img.careExpertAdvice,
      alt: Img.careExpertAdviceAlt,
      md: 7,
      sm: 6,
      orderSM: 5,
      orderXS: 6,
      orderMD: 5,
      mdText: 5,
      smText: 6,
      orderSMText: 6,
      orderXSText: 5,
      orderMDText: 6,
      Style: "left",
    },
    {
      id: 4,
      labelText: `یادآوری زمان‌های مهم`,
      note: `زمان‌هایی مثل غربالگری‌ها، سونوگرافی‌ها و چکاپ‌ها بر حسب هفته بارداری که در اون قرار داری بهت یادآوری میشه`,
      image: Img.careReminder,
      alt: Img.careReminderAlt,
      md: 7,
      sm: 6,
      orderSM: 8,
      orderXS: 8,
      orderMD: 8,
      mdText: 5,
      smText: 6,
      orderSMText: 7,
      orderXSText: 7,
      orderMDText: 7,
      Style: "right",
    },
    {
      id: 5,
      labelText: `دسترسی به متخصص زنان، زایمان و نازایی`,
      note: `هر مشکل و سوالی که در هر ساعت شبانه روز در دوران بارداری داری رو می‌تونی از پزشک‌های کلینیک ایمپو بپرسی`,
      image: Img.careWomenSpecialist,
      alt: Img.careWomenSpecialistAlt,
      md: 7,
      sm: 6,
      orderSM: 8,
      orderXS: 10,
      orderMD: 8,
      mdText: 5,
      smText: 6,
      orderSMText: 9,
      orderXSText: 9,
      orderMDText: 9,
      Style: "right",
    },
    {
      id: 6,
      labelText: `همدلی همسرت در دوران بارداری`,
      note: `در قسمت همدل ایمپو آقایان، همسرت میتونه وضعیت خودت و جنین رو ببینه و توصیه‌هایی برای مراقبت ازتون دریافت کنه`,
      image: Img.careEmpathy,
      alt: Img.careEmpathyAlt,
      md: 7,
      sm: 6,
      orderSM: 11,
      orderXS: 12,
      orderMD: 11,
      mdText: 5,
      smText: 6,
      orderSMText: 10,
      orderXSText: 11,
      orderMDText: 10,
      Style: "right",
    },
  ];

  const boxZigZagMobileSize = [
    {
      id: 1,
      labelText: `وضعیت رشد جنین در هر هفته`,
      note: `شکل ظاهری، قد، وزن و وضعیت رشد جنین رو در هر هفته از بارداری میبینی   `,
      image: Img.careGrowth,
      alt: Img.careGrowthAlt,
      md: 7,
      sm: 6,
      orderSM: 1,
      orderXS: 2,
      orderMD: 1,
      mdText: 5,
      smText: 6,
      orderSMText: 2,
      orderXSText: 1,
      orderMDText: 2,
      Style: "left",
    },
    {
      id: 2,
      labelText: `محاسبه سن بارداری`,
      note: `بر اساس تاریخ آخرین پریودت، سن بارداری و روز زایمان رو برات مشخص میکنیم`,
      image: Img.careGestationalAge,
      alt: Img.careGestationalAgeAlt,
      md: 7,
      sm: 6,
      orderSM: 4,
      orderXS: 4,
      orderMD: 4,
      mdText: 5,
      smText: 6,
      orderSMText: 3,
      orderXSText: 3,
      orderMDText: 3,
      Style: "right",
    },
    {
      id: 3,
      labelText: `توصیه‌های تخصصی`,
      note: `بر حسب اینکه تو کدوم هفته بارداری هستی و چه علائمی داری، توصیه‌های تخصصی مخصوص خودت رو دریافت میکنی`,
      image: Img.careExpertAdvice1,
      alt: Img.careExpertAdviceAlt,
      md: 7,
      sm: 6,
      orderSM: 5,
      orderXS: 6,
      orderMD: 5,
      mdText: 5,
      smText: 6,
      orderSMText: 6,
      orderXSText: 5,
      orderMDText: 6,
      Style: "left",
    },
    {
      id: 4,
      labelText: `یادآوری زمان‌های مهم`,
      note: `زمان‌هایی مثل غربالگری‌ها، سونوگرافی‌ها و چکاپ‌ها بر حسب هفته بارداری که در اون قرار داری بهت یادآوری میشه`,
      image: Img.careReminder1,
      alt: Img.careReminderAlt,
      md: 7,
      sm: 6,
      orderSM: 8,
      orderXS: 8,
      orderMD: 8,
      mdText: 5,
      smText: 6,
      orderSMText: 7,
      orderXSText: 7,
      orderMDText: 7,
      Style: "right",
    },
    {
      id: 5,
      labelText: `دسترسی به متخصص زنان، زایمان و نازایی`,
      note: `هر مشکل و سوالی که در هر ساعت شبانه روز در دوران بارداری داری رو می‌تونی از پزشک‌های کلینیک ایمپو بپرسی`,
      image: Img.careWomenSpecialist1,
      alt: Img.careWomenSpecialistAlt,
      md: 7,
      sm: 6,
      orderSM: 8,
      orderXS: 10,
      orderMD: 8,
      mdText: 5,
      smText: 6,
      orderSMText: 9,
      orderXSText: 9,
      orderMDText: 9,
      Style: "right",
    },
    {
      id: 6,
      labelText: `همدلی همسرت در دوران بارداری`,
      note: `در قسمت همدل ایمپو آقایان، همسرت میتونه وضعیت خودت و جنین رو ببینه و توصیه‌هایی برای مراقبت ازتون دریافت کنه`,
      image: Img.careEmpathy1,
      alt: Img.careEmpathyAlt,
      md: 7,
      sm: 6,
      orderSM: 11,
      orderXS: 12,
      orderMD: 11,
      mdText: 5,
      smText: 6,
      orderSMText: 10,
      orderXSText: 11,
      orderMDText: 10,
      Style: "right",
    },
  ];

  return (
    <>
      <Head>
        <title>مراقبت هفته به هفته در دوران بارداری </title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Appbar />
      {/* box zig zag */}
      <article className={styles.paddingTop}>
        <Grid container justifyContent={"center"}>
          <Grid item md={12} className={styles.textAlignCenterCare}>
            <Divider className={styles.Dividerstyle2} />

            <div className={`${styles.paddingBottomCareCare} ${styles.border}`}>
              <Typography
                type="display"
                size="medium"
                className={styles.styleTex1Care}
              >
                مراقبت هفته به هفته در دوران بارداری
              </Typography>
              <Typography className={styles.styleMobileSizeCare}>
                از هفته اول بارداری تا روز زایمان، در کنارتیم
              </Typography>
            </div>
          </Grid>
          <Divider className={styles.Dividerstyle2} />
        </Grid>

        <Hidden smDown>
          <BoxZigzager boxZigZag={boxZigZag} />
        </Hidden>
        <Hidden smUp>
          <BoxZigzager boxZigZag={boxZigZagMobileSize} />
        </Hidden>
      </article>

      <Typography
        type="headline"
        size="medium"
        className={[
          styles.textAlignCenterCare +
          " " +
          styles.styleTextCare +
          " " +
          styles.paddingTopText,
        ]}
      >
        چه کار کنیم ایمپو سن باردرای و روز زایمان رو دقیق محاسبه کنه؟
      </Typography>

      <Hidden smDown>
        <div className={styles.flexCareSlider}>
          <div className={styles.flexCare}>
            <div
              className={`${styles.paddingTextCare} ${mockap === 1 ? styles.item1 : undefined}`}
              onMouseEnter={() => changeMockap(1)}
            >
              <Typography
                type="display"
                size="small"
                className={styles.textCareSort}
              >
                بر اساس تاریخ آخرین پریود
              </Typography>

              <Typography
                type="title"
                size="small"
                className={styles.textCareSortTabletSize}
              >
                تاریخ اولین روز آخرین باری که پریود شدی رو وارد میکنی و ایمپو برات هفته بارداری و روز زایمان رو مشخص میکنه
              </Typography>
            </div>
            <div
              className={`${styles.paddingTextCare} ${mockap === 2 ? styles.item2 : undefined}`}
              onMouseEnter={() => changeMockap(2)}
            >
              <Typography
                type="display"
                size="small"
                className={styles.textCareSort}
              >
                بر اساس تاریخ زایمان
              </Typography>
              <Typography
                type="title"
                size="small"
                className={styles.textCareSortTabletSize}
              >
                اگه پزشک یا سونوگرافی بهت تاریخ زایمان داده باشه، تاریخش رو تو این قسمت وارد میکنی تا هفته بارداریت رو مشخص کنیم
              </Typography>
            </div>
          </div>
          <div>
            <Image
              alt="نظرات اپلیکیشن ایمپو"
              src={
                mockap == 1
                  ? Img.careCalculationPregnancyLevel2
                  : Img.careCalculationPregnancyLevel1
              }
              className={styles.ImageStyleFixCareInCareMouseEnter}
            />
          </div>
        </div>
      </Hidden>
      <Hidden smUp>
        <div
          className={styles.PaddingTopCare}
        >
          <Swiper
            pagination={{
              clickable: true,
            }}
            slidesPerView={1}
            centeredSlides={true}
            spaceBetween={20}
            modules={[Pagination]}
            className="sympathySlider"
          >
            {sliderItem.map((item, index) => (
              <SwiperSlide key={index}>
                <Grid container>
                  <Grid
                    item
                    md={item.md}
                    sm={item.sm}
                    xs={item.xs}
                    order={{
                      xs: item.orderXS,
                      sm: item.orderSM,
                      md: item.orderMD,
                    }}
                    className={styles.gridTextAlignh}
                  >
                    <Image
                      alt={item.alt}
                      src={item.image}
                      className={styles.widthImageSliderCare}
                    />
                  </Grid>

                  <Grid
                    item
                    md={item.mdText}
                    sm={item.smText}
                    xs={item.xsText}
                    order={{
                      xs: item.orderXSText,
                      sm: item.orderSMText,
                      md: item.orderMDText,
                    }}
                  >
                    <div className={styles.flexSliderSwiperCare}>
                      <div>
                        <Typography
                          type="title"
                          size="large"
                          className={styles.styletextinslider}
                        >
                          {item.levelText}
                        </Typography>
                        <Typography
                          type="body"
                          size="medium"
                          className={styles.textSliderTextCare}
                        >
                          {item.text}
                        </Typography>
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </SwiperSlide>
            ))}
          </Swiper>
          <span id='download'></span>
        </div>
      </Hidden>
      <div className={styles.titleSection}>
        <Typography
          className={styles.title}
          size="medium"
          type="headline"
          color="#1C1B1E"
          component="span"
        >
          هنوز ایمپو رو نصب نکردی؟ وقتشه که به خودت اهمیت بدی.
        </Typography>
        <Download />
      </div>
      <Footer />
    </>
  );
}
