import Appbar from "@/components/appbar"
import Footer from "@/components/footer"
import style from "@/styles/download.module.css"
import Typography from '@/shared/Typography'
import downloadPage from '@/assets/images/downloadMockapPage.webp'
import Image from 'next/image'
import Button from '@mui/material/Button'
import SimCardDownloadIcon from '@mui/icons-material/SimCardDownload'
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import download1 from "@/assets/images/download1.webp"
import download2 from "@/assets/images/download2.webp"
import download3 from "@/assets/images/download3.webp"
import download4 from "@/assets/images/download4.webp"
import download5 from "@/assets/images/download5.webp"
import download6 from "@/assets/images/download6.webp"
import download7 from "@/assets/images/download7.webp"
import Link from "next/link"
import Head from "next/head"
import DownloadSection from "@/components/download"
export default function Download() {
    const copyToClipboard = (link, id) => {
        navigator.clipboard.writeText(link)
        document.getElementById(id).innerText = "کپی شد"
    }
    const images = [download1, download2, download3, download4, download5, download6, download7]

    function GlobalIcon() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40" fill="none">
                <path d="M12.7477 34.8496C12.6977 34.8496 12.631 34.8829 12.581 34.8829C9.34766 33.2829 6.71432 30.6329 5.09766 27.3996C5.09766 27.3496 5.13099 27.2829 5.13099 27.2329C7.16432 27.8329 9.26432 28.2829 11.3477 28.6329C11.7143 30.7329 12.1477 32.8162 12.7477 34.8496Z" fill="#515151" />
                <path d="M34.8984 27.4162C33.2484 30.7329 30.4984 33.4162 27.1484 35.0329C27.7818 32.9162 28.3151 30.7829 28.6651 28.6329C30.7651 28.2829 32.8318 27.8329 34.8651 27.2329C34.8484 27.2996 34.8984 27.3662 34.8984 27.4162Z" fill="#515151" />
                <path d="M35.0318 12.8501C32.9318 12.2168 30.8151 11.7001 28.6651 11.3335C28.3151 9.18346 27.7984 7.05013 27.1484 4.9668C30.5984 6.6168 33.3818 9.40013 35.0318 12.8501Z" fill="#515151" />
                <path d="M12.7521 5.1501C12.1521 7.18343 11.7188 9.2501 11.3688 11.3501C9.21875 11.6834 7.08542 12.2168 4.96875 12.8501C6.58542 9.5001 9.26875 6.7501 12.5854 5.1001C12.6354 5.1001 12.7021 5.1501 12.7521 5.1501Z" fill="#515151" />
                <path d="M25.8208 10.9835C21.9542 10.5502 18.0542 10.5502 14.1875 10.9835C14.6042 8.70016 15.1375 6.41683 15.8875 4.21683C15.9208 4.0835 15.9042 3.9835 15.9208 3.85016C17.2375 3.5335 18.5875 3.3335 20.0042 3.3335C21.4042 3.3335 22.7708 3.5335 24.0708 3.85016C24.0875 3.9835 24.0875 4.0835 24.1208 4.21683C24.8708 6.4335 25.4042 8.70016 25.8208 10.9835Z" fill="#515151" />
                <path d="M10.982 25.8164C8.68203 25.3998 6.41536 24.8664 4.21536 24.1164C4.08203 24.0831 3.98203 24.0998 3.8487 24.0831C3.53203 22.7664 3.33203 21.4164 3.33203 19.9998C3.33203 18.5998 3.53203 17.2331 3.8487 15.9331C3.98203 15.9164 4.08203 15.9164 4.21536 15.8831C6.43203 15.1498 8.68203 14.5998 10.982 14.1831C10.5654 18.0498 10.5654 21.9498 10.982 25.8164Z" fill="#515151" />
                <path d="M36.6656 19.9998C36.6656 21.4164 36.4656 22.7664 36.149 24.0831C36.0156 24.0998 35.9156 24.0831 35.7823 24.1164C33.5656 24.8498 31.299 25.3998 29.0156 25.8164C29.449 21.9498 29.449 18.0498 29.0156 14.1831C31.299 14.5998 33.5823 15.1331 35.7823 15.8831C35.9156 15.9164 36.0156 15.9331 36.149 15.9331C36.4656 17.2498 36.6656 18.5998 36.6656 19.9998Z" fill="#515151" />
                <path d="M25.8208 29.0171C25.4042 31.3171 24.8708 33.5838 24.1208 35.7838C24.0875 35.9171 24.0875 36.0171 24.0708 36.1504C22.7708 36.4671 21.4042 36.6671 20.0042 36.6671C18.5875 36.6671 17.2375 36.4671 15.9208 36.1504C15.9042 36.0171 15.9208 35.9171 15.8875 35.7838C15.1542 33.5671 14.6042 31.3171 14.1875 29.0171C16.1208 29.2338 18.0542 29.3838 20.0042 29.3838C21.9542 29.3838 23.9042 29.2338 25.8208 29.0171Z" fill="#515151" />
                <path d="M26.2709 26.2724C22.1024 26.7983 17.895 26.7983 13.7265 26.2724C13.2005 22.1039 13.2005 17.8964 13.7265 13.7279C17.895 13.202 22.1024 13.202 26.2709 13.7279C26.7968 17.8964 26.7968 22.1039 26.2709 26.2724Z" fill="#515151" />
            </svg>
        )
    }
    function CafeBazar() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40" fill="none">
                <path d="M39.6829 13.0854C39.0689 12.6613 37.8771 12.0839 35.587 11.5629C34.9021 11.407 34.1168 11.2574 33.2214 11.1235C32.0937 10.9479 30.7852 10.792 29.2738 10.6623L29.0614 10.6443C28.7555 9.94787 28.4378 9.23622 28.1025 8.48681C27.9244 8.08488 27.7531 7.68085 27.5727 7.25234C27.5522 7.20171 27.5292 7.14896 27.5064 7.09832C26.8466 5.52333 26.1365 3.90593 24.9398 2.46935C23.6019 0.876425 21.8439 0 19.9923 0C18.1403 0 16.3844 0.878535 15.0443 2.46935C13.8388 3.90383 13.1401 5.5208 12.4804 7.09389L12.4116 7.25234C12.2339 7.68085 12.0624 8.08488 11.8823 8.48681C11.5463 9.23622 11.2313 9.94787 10.9232 10.6443L10.713 10.6623C9.19696 10.7918 7.88846 10.9479 6.75856 11.1258C5.86339 11.2619 5.08249 11.4134 4.39973 11.5653C2.11649 12.0858 0.915419 12.6636 0.303387 13.0877C0.195303 13.1633 0.110869 13.266 0.0587055 13.3842C0.00653527 13.5027 -0.0113295 13.6324 0.00698254 13.7597L0.205688 15.1811C0.506952 17.308 0.828775 19.8059 1.35858 22.3809C1.74914 24.3631 2.29619 26.3141 2.99575 28.2161C3.27646 28.9651 3.58922 29.7012 3.93889 30.413C5.43305 33.312 7.74458 35.7484 10.6093 37.4432C13.4739 39.1385 16.7765 40.0244 20.139 39.9995C23.5022 39.9748 26.7901 39.0406 29.6274 37.3033C32.4646 35.5661 34.7374 33.0963 36.1852 30.1757C36.4548 29.6043 36.7035 29.0199 36.932 28.418C38.5621 24.1996 39.19 19.5442 39.7084 15.847L39.9959 13.7401C40.0098 13.614 39.9881 13.4866 39.9329 13.3713C39.8782 13.2562 39.7919 13.1573 39.6832 13.0852L39.6829 13.0854ZM19.9781 10.3129C18.1197 10.3129 16.428 10.3458 14.8911 10.4049C14.9731 10.2183 15.0578 10.0314 15.1444 9.83837C15.3385 9.39889 15.5257 8.95941 15.7063 8.51993C16.0818 7.5627 16.5217 6.62973 17.0239 5.72757C17.2518 5.33593 17.5129 4.96272 17.8045 4.61167C18.179 4.1722 18.9623 3.41413 19.9779 3.41413C20.9942 3.41413 21.7773 4.16355 22.1542 4.61167C22.4455 4.96254 22.7068 5.33598 22.9349 5.72757C23.4357 6.63015 23.8758 7.56312 24.2524 8.51993C24.4328 8.95941 24.6178 9.39889 24.8138 9.83837C24.9009 10.0314 24.9831 10.2183 25.0676 10.4049C23.5305 10.3458 21.8388 10.3129 19.9777 10.3129H19.9781Z" fill="url(#paint0_linear_4860_7891)" />
                <defs>
                    <linearGradient id="paint0_linear_4860_7891" x1="20.0007" y1="0.000210983" x2="20.0007" y2="39.9761" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#97D700" />
                        <stop offset="0.631" stop-color="#50BC25" />
                        <stop offset="1" stop-color="#009F64" />
                    </linearGradient>
                </defs>
            </svg>
        )
    }
    function Myket() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40" fill="none">
                <g id="XMLID_46_">
                    <g id="XMLID_45_">
                        <g id="XMLID_58_">
                            <path id="XMLID_62_" d="M20.0085 39.1255C30.5714 39.1255 39.1342 30.5626 39.1342 19.9997C39.1342 9.4369 30.5714 0.874023 20.0085 0.874023C9.44569 0.874023 0.882812 9.4369 0.882812 19.9997C0.882812 30.5626 9.44569 39.1255 20.0085 39.1255Z" fill="#0091EA" />
                            <g id="XMLID_59_">
                                <path id="XMLID_61_" d="M31.7396 17.7369C31.5796 14.114 28.6025 11.2397 24.9453 11.2397C22.9167 11.2397 21.0882 12.1369 19.8425 13.5426C18.5967 12.1369 16.7682 11.2397 14.7396 11.2397C11.1567 11.2397 8.2196 13.9997 7.95674 17.5369C7.94531 17.6112 7.94531 17.7083 7.94531 17.7997V25.474C7.94531 26.4169 8.70531 27.1769 9.64817 27.1769H9.68817C10.631 27.1769 11.391 26.4169 11.391 25.474V18.0455H11.4025C11.4025 16.1769 12.9225 14.6626 14.7853 14.6626C16.6539 14.6626 18.1682 16.1826 18.1682 18.0455H18.1796V24.314C18.1796 25.2569 18.9396 26.0169 19.8825 26.0169H19.9225C20.8653 26.0169 21.6253 25.2569 21.6253 24.314V18.0283C21.6367 16.1597 23.1453 14.6683 25.0082 14.6683C26.871 14.6683 28.4082 16.194 28.4082 18.0569H28.4196V25.4969C28.4196 26.4283 29.1796 27.1769 30.0996 27.1769H30.1396C31.071 27.1769 31.8196 26.4169 31.8196 25.4969V17.9083C31.7396 17.8226 31.7396 17.7826 31.7396 17.7369Z" fill="white" />
                                <path id="XMLID_60_" d="M25.3092 27.8399C25.235 27.7142 24.9835 27.9656 24.9835 27.9656C24.0407 28.9885 22.0864 29.6456 20.0578 29.6456C18.0292 29.6456 16.0864 28.9885 15.1321 27.9656C15.1321 27.9656 14.8807 27.7028 14.8064 27.8399C14.7092 27.9885 14.8578 28.2285 14.8578 28.2285C16.0635 29.9713 17.9035 30.7542 20.0578 30.7542C22.2121 30.7542 24.0521 29.9713 25.2578 28.2285C25.2692 28.2228 25.4064 27.9885 25.3092 27.8399Z" fill="white" />
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        )
    }

    return (
        <>
            <Head>
                <title>راهنمای نصب اپلیکیشن ایمپو</title>
                <meta name="description" content="راهنمای نصب" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
                <meta name="robots" content="noindex,nofollow" />
            </Head>
            <Appbar />
            <div className={`${style.main} container`}>
                <Typography type="headline" size="medium" className={style.titlePage} component="h2" mobileSize="large" MobileType="headline">
                    راهنمای نصب اپلیکیشن ایمپو
                </Typography>
                <section className={style.downloadBox}>
                    <div className={style.downloads}>
                        <div className={style.womanDownload}>
                            <Typography type="headline" color={"#EC407A"} size="small" className={style.title} component="h5">
                                ایمپو بانوان
                            </Typography>
                            <div className={style.buttons}>
                                <Link href="http://yun.ir/direct-woman">
                                    <Button aria-label="btnName" variant="text" id="DownloadWomanDirect" size="large" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={<SimCardDownloadIcon fontSize="35px" />}>
                                        <Typography type="headline" className={style.textBtn} color={"#EC407A"} size="small" component="h5">
                                            دانلود مستقیم
                                        </Typography>
                                    </Button>
                                </Link>
                                <Link href="http://yun.ir/web-woman">
                                    <Button aria-label="btnName" variant="text" id="DownloadWomanWebApp" size="large" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={GlobalIcon()}>
                                        <Typography type="headline" className={style.textBtn} color={"#EC407A"} size="small" component="h5">
                                            وب اپلیکیشن(IOS)
                                        </Typography>
                                    </Button>
                                </Link>
                                <Link href="http://yun.ir/bazar-woman">
                                    <Button aria-label="btnName" variant="text" size="large" id="DownloadWomanBazar" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={CafeBazar()}>
                                        <Typography type="headline" className={style.textBtn} color={"#EC407A"} size="small" component="h5">
                                            دانلود از کافه بازار
                                        </Typography>
                                    </Button>
                                </Link>
                                <Link href="http://yun.ir/myket-woman">
                                    <Button aria-label="btnName" variant="text" size="large" id="DownloadWomanMyket" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={Myket()}>
                                        <Typography type="headline" className={style.textBtn} color={"#EC407A"} size="small" component="h5">
                                            دانلود از مایکت
                                        </Typography>
                                    </Button>
                                </Link>
                            </div>
                        </div>
                        <div className={style.manDownload}>
                            <Typography type="headline" color={"#515151"} size="small" className={style.title} component="h5">
                                ایمپو آقایان
                            </Typography>
                            <div className={style.buttons}>
                                <a href="http://yun.ir/direct-man" download="impo.apk">
                                    <Button aria-label="btnName" variant="text" size="large" id="DownloadManDirect" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={<SimCardDownloadIcon color="action" fontSize="35px" />}>
                                        <Typography type="headline" className={style.textBtn} color={"#515151"} size="small" component="h5">
                                            دانلود مستقیم
                                        </Typography>
                                    </Button>
                                </a>
                                <Link href="http://yun.ir/web-man">
                                    <Button aria-label="btnName" variant="text" size="large" id="DownloadManWebApp" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={GlobalIcon()}>
                                        <Typography type="headline" className={style.textBtn} color={"#515151"} size="small" component="h5">
                                            وب اپلیکیشن(IOS)
                                        </Typography>
                                    </Button>
                                </Link>
                                <Link href="http://yun.ir/bazar-man">
                                    <Button aria-label="btnName" variant="text" size="large" id="DownloadManBazar" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={CafeBazar()}>
                                        <Typography type="headline" className={style.textBtn} color={"#515151"} size="small" component="h5">
                                            دانلود از کافه بازار
                                        </Typography>
                                    </Button>
                                </Link>
                                <Link href="http://yun.ir/myket-man">
                                    <Button aria-label="btnName" variant="text" size="large" id="DownloadManMyket" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={Myket()}>
                                        <Typography type="headline" className={style.textBtn} color={"#515151"} size="small" component="h5">
                                            دانلود از مایکت
                                        </Typography>
                                    </Button>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className={style.imageFrame}>
                        <Image src={downloadPage} className={style.downloadPage} alt="دانلود اپ" width="455" height="723" />
                    </div>
                </section>
                <Typography type="body" size="large" className={style.helpText} component="h2">
                    درصورت وجود مشکل در لینک‌های دانلود بالا، مراحل زیر رو انجام بده.
                </Typography>
                <div className={style.copyLink}>
                    <Typography type="title" size="small" className={style.copyTitle} component="h2">
                        برای کپی لینک دانلود اپلیکیشن ایمپو بانوان و آقایان، روی دکمه‌ی موردنظر کلیک کن.
                    </Typography>
                    <div className={style.copyBox}>
                        <Typography type="title" color={"#EC407A"} size="small" component="h2">
                            ایمپو بانوان
                        </Typography>
                        <div className={style.copy}>
                            <span className={style.link}>
                                yun.ir/direct-woman
                            </span>
                            <Button onClick={() => { copyToClipboard("http://yun.ir/direct-woman", "woman") }} aria-label="btnName" variant="text" size="small" sx={{ backgroundColor: "#fff", color: "#5B595C", marginRight: "16px" }} classes={{ startIcon: style.copyIcon }} startIcon={<ContentCopyIcon />}>
                                <Typography type="label" color={"#5B595C"} size="medium" id="woman" component="h5">
                                    کپی کردن
                                </Typography>
                            </Button>
                        </div>
                    </div>
                    <div className={style.copyBox}>
                        <Typography type="title" color={"#515151"} size="small" component="h2">
                            ایمپو آقایان
                        </Typography>
                        <div className={style.copy}>
                            <span className={style.link}>
                                yun.ir/direct-man
                            </span>
                            <Button onClick={() => { copyToClipboard("http://yun.ir/direct-man", "man") }} aria-label="btnName" variant="text" size="small" sx={{ backgroundColor: "#fff", color: "#5B595C", marginRight: "16px" }} classes={{ startIcon: style.copyIcon }} startIcon={<ContentCopyIcon />}>
                                <Typography type="label" color={"#5B595C"} size="medium" id="man" component="h5">
                                    کپی کردن
                                </Typography>
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
            <div className={style.helpImage}>
                <Typography type="title" size="small" className={style.copyTitle2} component="h2">
                    مرورگر (کروم) خودت رو باز کن.
                </Typography>
                <div className={style.images}>
                    {
                        images.map(img => {
                            return (
                                <Image src={img} alt="عکس دانلود" width={"auto"} height={"auto"} />
                            )
                        })
                    }
                </div>
            </div>
            <span id='download'></span>
            <DownloadSection />
            <Footer />
        </>
    )
}