import style from "@/styles/notFound.module.css"
import Image from "next/image";
import vector404 from "@/assets/images/vector404.webp"
import Link from "next/link";
export default function NotFound() {
    return (
        <>
            <div className={style.NotFound}>
                <Image src={vector404} alt="vector404" width={591} height={423} />
                <div className={style.action}>
                    <h3>صفحه مورد نظرت پیدا نشد</h3>
                    <p>اما می‌تونی برای خوندن مقاله‌های بیشتر، به مجله سلامت ایمپو یه سر بزنی</p>
                    <Link href={"/blogs"}>
                        خوندن مقاله‌های ایمپو
                    </Link>
                </div>
            </div>
        </>
    );
}
