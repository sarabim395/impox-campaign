import Appbar from "@/components/appbar";
import Head from "next/head";
import style from "@/styles/doctor.module.css";
import { Grid } from '@mui/material';
import Typography from "@/shared/Typography";
import Breadcrumb from "@/components/breadcrumb";
import Image from 'next/image';
import Download from "../../components/download";
import Footer from "@/components/footer";
import profilePattern from '@/assets/images/profilePattern.webp';
import Article from '@/components/article';
import axios, { PICURL } from '@/utils/axios';
import { useEffect, useState } from "react";
import { Pagination, PaginationItem } from '@mui/material';
import Skeleton from '@mui/material/Skeleton';
import logoFooter from "@/assets/images/logoFooter.svg";
import { v4 as uuidv4 } from 'uuid';
import Link from "next/link";
import myket from '@/assets/images/myket.svg';
import direct from '@/assets/images/direct.svg';
import webapp from '@/assets/images/webapp.svg';
import mockapDownload from '@/assets/images/mockapDownload.webp';
import userImg from '@/assets/images/user.svg.png';
import { toFarsiNumber } from "@/utils";
export default function Author() {
    const [authorInfo, setauthorInfo] = useState({ catId: "", catName: "" })
    const [articles, setArticleByAuthor] = useState([])
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);
    const [count, setCount] = useState(1);
    const skeleton = ["", "", "", ""];
    const fetchArticleList = async () => {
        const authorInfo = JSON?.parse(localStorage?.getItem("authorInfo"));
        setauthorInfo(authorInfo)
        const response = await axios.get(`article/articles/author/${authorInfo.id}/${page}/6`)
        setArticleByAuthor(response.data.articles)
        setCount(Math.ceil(response.data.totalCount / 10))
        setLoading(false)
    }

    const handleChange = (event, value) => {
        setLoading(true)
        setPage(value);
    };

    useEffect(() => {
        window.scrollTo({ top: "0", behavior: "smooth" })
        fetchArticleList()
    }, [page])
    return (
        <>
            <Head>
                <title>{authorInfo.authorName}</title>
                <meta name="description" content="صفحه نویسنده" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Appbar darkmode={true} />
            <main className={style.doctorPage}>
                <div className="container">
                    <Grid container spacing={2}>
                        <Grid item md={9} xs={12} className={style.contentDivider}>
                            <Breadcrumb categoryName="نویسنده" />
                            <Typography
                                type="display"
                                size="medium"
                                className={style.titlePage}
                            >
                                {`مقالات مربوط به ${authorInfo.authorName || ""}`}
                            </Typography>
                            {loading ?
                                skeleton.map(() => {
                                    return (
                                        <div className={style.skeleton} key={uuidv4()}>
                                            <Skeleton animation="wave" variant="rectangular" width={300} height={150} sx={{ bgcolor: '#F9F9F9' }} />
                                            <div className={style.texts}>
                                                <Skeleton variant="rectangular" animation="wave" width={400} height={30} sx={{ bgcolor: '#F9F9F9' }} />
                                                <Skeleton variant="rectangular" animation="wave" width={800} height={15} sx={{ bgcolor: '#F9F9F9' }} />
                                                <Skeleton variant="rectangular" animation="wave" width={800} height={15} sx={{ bgcolor: '#F9F9F9' }} />
                                                <br />
                                                <Skeleton variant="rectangular" animation="wave" width={100} height={30} sx={{ bgcolor: '#F9F9F9' }} />
                                            </div>
                                        </div>
                                    )
                                }) : <Article ArticleContainer={articles} />
                            }
                            <div className={style.pagination}>
                                {count ? <Pagination count={count} onChange={handleChange} page={page} color="primary" size="large" renderItem={(item) => (
                                    <PaginationItem
                                        {...item}
                                        page={toFarsiNumber(item.page)}
                                    />
                                )} /> : undefined}
                            </div>
                        </Grid>
                        <Grid item md={3} xs={12}>
                            <aside className={style.doctorSidbar}>
                                <section className={style.doctorInfo}>
                                    <div className={style.images}>
                                        <Image width="auto" height="auto" src={profilePattern} alt="پس زمینه پروفایل" />
                                        <Image src={authorInfo.authorPic ? `${PICURL}${authorInfo.authorPic}` : userImg} className={style.profileImage} unoptimized="false" width="100" height="100" alt={authorInfo.authorName} />
                                    </div>
                                    <Typography
                                        size="small"
                                        type="headline"
                                        color="#1C1B1E"
                                        component="h4"
                                    >
                                        {authorInfo.authorName}
                                    </Typography>
                                    <Typography
                                        size="medium"
                                        type="body"
                                        color="#605D62"
                                        component="span"
                                    >
                                        {authorInfo.bio}
                                    </Typography>
                                    <Typography
                                        size="small"
                                        type="body"
                                        color="#545156"
                                        component="p"
                                    >
                                        {authorInfo.description}
                                    </Typography>
                                </section>
                                <section className={style.downloadApp}>
                                    <Typography
                                        size="large"
                                        type="body"
                                        color="#1C1B1E"
                                        component="span"
                                    >
                                        هنوز ایمپو رو نصب نکردی؟ الان وقتشه...
                                    </Typography>
                                    <div className={style.frame}>
                                        <div>
                                            <Image width="auto" height="auto" className={style.logoType} src={logoFooter} alt="لوگو" />
                                        </div>
                                        <Typography
                                            size="large"
                                            type="body"
                                            color="#1C1B1E"
                                            component="span"
                                        >
                                            بزرگترین پلتفرم سلامت حوزه زنان
                                        </Typography>
                                        <div className={style.links}>
                                            <Link href={"http://yun.ir/myket-woman"} aria-label="link"><Image width="auto" height="auto" src={myket} alt="مایکت " className={style.DownloadBtnImage} />مایکت</Link>
                                            <a href={"http://yun.ir/direct-woman"} download="impo.apk" aria-label="link"><Image width="auto" height="auto" src={direct} alt="دانلود مستقیم " className={style.DownloadBtnImage} /> دانلود مستقیم </a>
                                            <Link href={"http://yun.ir/web-woman"} aria-label="link"><Image width="auto" height="auto" src={webapp} alt=" وب اپلیکیشن " className={style.DownloadBtnImage} /> وب اپ ( کاربران IOS ) </Link>
                                        </div>
                                        <Image width="auto" height="auto" className={style.mockap} src={mockapDownload} alt="دانلود اپ" />
                                    </div>
                                </section>
                            </aside>
                        </Grid>
                    </Grid >
                </div >
                <span id='download'></span>

            </main >
            <div className={style.titleSection}>
                <Typography
                    className={style.title}
                    size="medium"
                    type="headline"
                    color="#1C1B1E"
                    component="span"
                >
                    هنوز ایمپو رو نصب نکردی؟ وقتشه که به خودت اهمیت بدی.
                </Typography>
                <Download />
            </div>
            <Footer />
        </>
    )
}
