import Appbar from "@/components/appbar"
import Footer from "@/components/footer"
import style from "@/styles/instagramDownload.module.css"
import Typography from '@/shared/Typography'
import downloadPage from '@/assets/images/downloadMockapPage.webp'
import Image from 'next/image'
import Button from '@mui/material/Button'
import SimCardDownloadIcon from '@mui/icons-material/SimCardDownload'
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import download1 from "@/assets/images/download1.webp"
import download2 from "@/assets/images/download2.webp"
import download3 from "@/assets/images/download3.webp"
import download4 from "@/assets/images/download4.webp"
import download5 from "@/assets/images/download5.webp"
import download6 from "@/assets/images/download6.webp"
import download7 from "@/assets/images/download7.webp"
import Link from "next/link"
import Head from "next/head"
export default function Download() {
    const copyToClipboard = (link, id) => {
        navigator.clipboard.writeText(link)
        document.getElementById(id).innerText = "کپی شد"
    }
    const images = [download1, download2, download3, download4, download5, download6, download7]

    function GlobalIcon() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40" fill="none">
                <path d="M12.7477 34.8496C12.6977 34.8496 12.631 34.8829 12.581 34.8829C9.34766 33.2829 6.71432 30.6329 5.09766 27.3996C5.09766 27.3496 5.13099 27.2829 5.13099 27.2329C7.16432 27.8329 9.26432 28.2829 11.3477 28.6329C11.7143 30.7329 12.1477 32.8162 12.7477 34.8496Z" fill="#515151" />
                <path d="M34.8984 27.4162C33.2484 30.7329 30.4984 33.4162 27.1484 35.0329C27.7818 32.9162 28.3151 30.7829 28.6651 28.6329C30.7651 28.2829 32.8318 27.8329 34.8651 27.2329C34.8484 27.2996 34.8984 27.3662 34.8984 27.4162Z" fill="#515151" />
                <path d="M35.0318 12.8501C32.9318 12.2168 30.8151 11.7001 28.6651 11.3335C28.3151 9.18346 27.7984 7.05013 27.1484 4.9668C30.5984 6.6168 33.3818 9.40013 35.0318 12.8501Z" fill="#515151" />
                <path d="M12.7521 5.1501C12.1521 7.18343 11.7188 9.2501 11.3688 11.3501C9.21875 11.6834 7.08542 12.2168 4.96875 12.8501C6.58542 9.5001 9.26875 6.7501 12.5854 5.1001C12.6354 5.1001 12.7021 5.1501 12.7521 5.1501Z" fill="#515151" />
                <path d="M25.8208 10.9835C21.9542 10.5502 18.0542 10.5502 14.1875 10.9835C14.6042 8.70016 15.1375 6.41683 15.8875 4.21683C15.9208 4.0835 15.9042 3.9835 15.9208 3.85016C17.2375 3.5335 18.5875 3.3335 20.0042 3.3335C21.4042 3.3335 22.7708 3.5335 24.0708 3.85016C24.0875 3.9835 24.0875 4.0835 24.1208 4.21683C24.8708 6.4335 25.4042 8.70016 25.8208 10.9835Z" fill="#515151" />
                <path d="M10.982 25.8164C8.68203 25.3998 6.41536 24.8664 4.21536 24.1164C4.08203 24.0831 3.98203 24.0998 3.8487 24.0831C3.53203 22.7664 3.33203 21.4164 3.33203 19.9998C3.33203 18.5998 3.53203 17.2331 3.8487 15.9331C3.98203 15.9164 4.08203 15.9164 4.21536 15.8831C6.43203 15.1498 8.68203 14.5998 10.982 14.1831C10.5654 18.0498 10.5654 21.9498 10.982 25.8164Z" fill="#515151" />
                <path d="M36.6656 19.9998C36.6656 21.4164 36.4656 22.7664 36.149 24.0831C36.0156 24.0998 35.9156 24.0831 35.7823 24.1164C33.5656 24.8498 31.299 25.3998 29.0156 25.8164C29.449 21.9498 29.449 18.0498 29.0156 14.1831C31.299 14.5998 33.5823 15.1331 35.7823 15.8831C35.9156 15.9164 36.0156 15.9331 36.149 15.9331C36.4656 17.2498 36.6656 18.5998 36.6656 19.9998Z" fill="#515151" />
                <path d="M25.8208 29.0171C25.4042 31.3171 24.8708 33.5838 24.1208 35.7838C24.0875 35.9171 24.0875 36.0171 24.0708 36.1504C22.7708 36.4671 21.4042 36.6671 20.0042 36.6671C18.5875 36.6671 17.2375 36.4671 15.9208 36.1504C15.9042 36.0171 15.9208 35.9171 15.8875 35.7838C15.1542 33.5671 14.6042 31.3171 14.1875 29.0171C16.1208 29.2338 18.0542 29.3838 20.0042 29.3838C21.9542 29.3838 23.9042 29.2338 25.8208 29.0171Z" fill="#515151" />
                <path d="M26.2709 26.2724C22.1024 26.7983 17.895 26.7983 13.7265 26.2724C13.2005 22.1039 13.2005 17.8964 13.7265 13.7279C17.895 13.202 22.1024 13.202 26.2709 13.7279C26.7968 17.8964 26.7968 22.1039 26.2709 26.2724Z" fill="#515151" />
            </svg>
        )
    }

    return (
        <>
            <Head>
                <title>راهنمای نصب اپلیکیشن ایمپو</title>
                <meta name="description" content="راهنمای نصب" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
                <meta name="robots" content="noindex,nofollow" />
            </Head>
            <Appbar />
            <div className={`${style.main} container`}>
                <Typography type="headline" size="medium" className={style.titlePage} component="h2" mobileSize="large" MobileType="headline">
                    راهنمای نصب اپلیکیشن ایمپو
                </Typography>
                <section className={style.downloadBox}>
                    <div className={style.downloads}>
                        <div className={style.womanDownload}>
                            <Typography type="headline" color={"#EC407A"} size="small" className={style.title} component="h5">
                                ایمپو بانوان
                            </Typography>
                            <div className={style.buttons}>
                                <Link href="http://yun.ir/direct-woman">
                                    <Button aria-label="btnName" variant="text" size="large" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={<SimCardDownloadIcon fontSize="35px" />}>
                                        <Typography type="headline" className={style.textBtn} color={"#EC407A"} size="small" component="h5">
                                            دانلود مستقیم
                                        </Typography>
                                    </Button>
                                </Link>
                                <Link href="http://yun.ir/web-woman">
                                    <Button aria-label="btnName" variant="text" size="large" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={GlobalIcon()}>
                                        <Typography type="headline" className={style.textBtn} color={"#EC407A"} size="small" component="h5">
                                            وب اپلیکیشن(IOS)
                                        </Typography>
                                    </Button>
                                </Link>
                            </div>
                        </div>
                        <div className={style.manDownload}>
                            <Typography type="headline" color={"#515151"} size="small" className={style.title} component="h5">
                                ایمپو آقایان
                            </Typography>
                            <div className={style.buttons}>
                                <a href="http://yun.ir/direct-man" download="impo.apk">
                                    <Button aria-label="btnName" variant="text" size="large" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={<SimCardDownloadIcon color="action" fontSize="35px" />}>
                                        <Typography type="headline" className={style.textBtn} color={"#515151"} size="small" component="h5">
                                            دانلود مستقیم
                                        </Typography>
                                    </Button>
                                </a>
                                <Link href="http://yun.ir/web-man">
                                    <Button aria-label="btnName" variant="text" size="large" className={style.button} sx={{ backgroundColor: "#fff" }} classes={{ endIcon: style.endIcon }} endIcon={GlobalIcon()}>
                                        <Typography type="headline" className={style.textBtn} color={"#515151"} size="small" component="h5">
                                            وب اپلیکیشن(IOS)
                                        </Typography>
                                    </Button>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className={style.imageFrame}>
                        <Image src={downloadPage} className={style.downloadPage} alt="دانلود اپ" width="455" height="723" />
                    </div>
                </section>
                <Typography type="body" size="large" className={style.helpText} component="h2">
                    درصورت وجود مشکل در لینک‌های دانلود بالا، مراحل زیر رو انجام بده.
                </Typography>
                <div className={style.copyLink}>
                    <Typography type="title" size="small" className={style.copyTitle} component="h2">
                        برای کپی لینک دانلود اپلیکیشن ایمپو بانوان و آقایان، روی دکمه‌ی موردنظر کلیک کن.
                    </Typography>
                    <div className={style.copyBox}>
                        <Typography type="title" color={"#EC407A"} size="small" component="h2">
                            ایمپو بانوان
                        </Typography>
                        <div className={style.copy}>
                            <span className={style.link}>
                                yun.ir/direct-woman
                            </span>
                            <Button onClick={() => { copyToClipboard("http://yun.ir/direct-woman", "woman") }} aria-label="btnName" variant="text" size="small" sx={{ backgroundColor: "#fff", color: "#5B595C", marginRight: "16px" }} classes={{ startIcon: style.copyIcon }} startIcon={<ContentCopyIcon />}>
                                <Typography type="label" color={"#5B595C"} size="medium" id="woman" component="h5">
                                    کپی کردن
                                </Typography>
                            </Button>
                        </div>
                    </div>
                    <div className={style.copyBox}>
                        <Typography type="title" color={"#515151"} size="small" component="h2">
                            ایمپو آقایان
                        </Typography>
                        <div className={style.copy}>
                            <span className={style.link}>
                                yun.ir/direct-man
                            </span>
                            <Button onClick={() => { copyToClipboard("http://yun.ir/direct-man", "man") }} aria-label="btnName" variant="text" size="small" sx={{ backgroundColor: "#fff", color: "#5B595C", marginRight: "16px" }} classes={{ startIcon: style.copyIcon }} startIcon={<ContentCopyIcon />}>
                                <Typography type="label" color={"#5B595C"} size="medium" id="man" component="h5">
                                    کپی کردن
                                </Typography>
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
            <div className={style.helpImage}>
                <Typography type="title" size="small" className={style.copyTitle2} component="h2">
                    مرورگر (کروم) خودت رو باز کن.
                </Typography>
                <div className={style.images}>
                    {
                        images.map(img => {
                            return (
                                <Image src={img} alt="عکس دانلود" width={"auto"} height={"auto"} />
                            )
                        })
                    }
                </div>
            </div>
            <Footer />
        </>
    )
}