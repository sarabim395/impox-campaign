import Typography from "../shared/Typography";
import Head from "next/head";
import styles from "@/styles/calendar.module.css";
import Image from "next/image";
import Img from "@/image/image";
import Slider from "@/components/slider";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import AppbarMain from "@/components/appbarMain";
import Download from "@/components/download";
import Footer from "@/components/footer";
import { Grid, Hidden, Divider } from "@mui/material";
import BoxZigzager from "@/components/boxZigzager";
import { useState, useEffect } from "react";
import horizontal from "../assets/images/horizontal.webp"
import heroCalen from "../assets/images/heroCalen.webp"

export default function calendar() {
  const [isPlay, setIsPlay] = useState(false)

  useEffect(() => {
    if (isPlay) {
      document.getElementById("videoMarket").play();
    }
  }, [isPlay])

  const sliderItemNumber = [
    {
      id: 1,
      levelText: "مرحله اول",
      text: `اگه همیشه قبل یا بعد خونریزی لکه بینی هم داری، روزهای لکه بینی هم جز روزهای پریود حساب میشه`,
      image: Img.preventionNotinstallLevel1,
      alt: Img.preventioninstallLevel1Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 2,
      levelText: "مرحله دوم",
      text: "طول سیکل پریود یعنی فاصله \n شروع دو تا پریود پشت سر هم.",
      image: Img.preventionNotinstallLevel2,
      alt: Img.preventionNotinstallLevel2Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 3,
      levelText: "مرحله سوم",
      text: `برای پیش‌بینی دوره بعدی پریود، لازمه که بهمون بگی آخرین باری که پریود شدی چه روزی بوده,`,
      image: Img.preventionNotinstallLevel3,
      alt: Img.preventionNotinstallLevel3Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
  ];

  const sliderItemCode = [
    {
      id: 1,
      levelText: "مرحله اول",
      text: `با انتخاب پروفایل از قسمت بالا سمت چپ صفحه اصلی، می‌تونی طول دوره و طول پریودی که قبلا انتخاب کردی رو ببینی,`,
      textMobile: `از تب همدل دکمه "شروع همدلی و اضافه کردن پارتنر" رو انتخاب میکنی`,
      image: Img.preventioninstallLevel1,
      alt: Img.preventioninstallLevel1Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 2,
      levelText: "مرحله دوم",
      text: `طول دوره یعنی فاصله بین شروع دو تا پریود پشت سر هم. اگه لازمه ویرایشش کن,`,
      textMobile: `در این قسمت،"اشتراک‌گذاری کد با همدل" رو انتخاب میکنی`,
      image: Img.preventioninstallLevel2,
      alt: Img.preventioninstallLevel2Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 3,
      levelText: "مرحله سوم",
      text: `اگه زودتر یا دیرتر پریود شدی، از قسمت تغییرات من در صفحه اصلی، دکمه‌های پریود شدم و هنوز پریود نشدم رو انتخاب کن`,
      textMobile: `و کد اختصاصیت رو با پیامک یا شبکه‌های اجتماعی براش میفرستی`,
      image: Img.preventioninstallLevel3,
      alt: Img.preventioninstallLevel3Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 4,
      levelText: "مرحله چهارم",
      text: `اگه فراموش کردی همون روزی که تاریخ پریودت جابجا شده، به اپ سر بزنی می‌تونی بعدا از قسمت تغییرات من، تنظیمات رو انتخاب کنی`,
      textMobile: `در بعد از واردکردن کد توسط پارتنرت باید درخواست همدلیش رو قبول کنی`,
      image: Img.preventioninstallLevel4,
      alt: Img.preventioninstallLevel4Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
    {
      id: 5,
      levelText: "مرحله پنجم",
      text: "`تاریخ شروع و پایان آخرین باری که پریود شدی رو اینجا انتخاب میکنی. اتمام دوره یعنی آخرین روز قبل از شروع پریود بعدی",
      textMobile: `و دسترسی‌هایی که به همدلت میدی رو تایید کنی`,
      image: Img.preventioninstallLevel5,
      alt: Img.preventioninstallLevel5Alt,
      md: 6,
      sm: 6,
      xs: 12,
      orderMD: 1,
      orderSM: 1,
      orderXS: 2,
      mdText: 6,
      smText: 6,
      xsText: 12,
      orderMDText: 2,
      orderSMText: 2,
      orderXSText: 1,
    },
  ];

  const boxZigZag = [
    {
      id: 1,
      labelText: `می‌تونی پریودهای قبلی و پیش‌بینی پریودهای بعدیت رو ببینی`,
      note: `اینجوری هم امکان برنامه ریزی برای آینده رو داری و هم امکان تشخیص اختلالات قاعدگی`,
      image: Img.calendarCycle,
      alt: Img.calendarCycleAlt,
      md: 7,
      sm: 6,
      orderSM: 1,
      orderXS: 2,
      orderMD: 1,
      mdText: 5,
      smText: 6,
      orderSMText: 2,
      orderXSText: 1,
      orderMDText: 2,
      Style: "left",
    },
    {
      id: 2,
      labelText: `می‌تونی روز تخمک گذاری رو ببینی`,
      note: `با پیش‌بینی دوره باروری و روز تخمک گذاری، می‌تونی برای پیشگیری یا اقدام به بارداری برنامه ریزی کنی`,
      image: Img.calendarDayOvulation,
      alt: Img.calendarDayOvulationAlt,
      md: 7,
      sm: 6,
      orderSM: 4,
      orderXS: 4,
      orderMD: 4,
      mdText: 5,
      smText: 6,
      orderSMText: 3,
      orderXSText: 3,
      orderMDText: 3,
      Style: "right",
    },
    {
      id: 3,
      labelText: `توصیه‌هایی  میگیری که مخصوص خودته`,
      note: `با تکمیل قسمت نشانه ها، دقیقا همون توصیه ای رو میگیری که مخصوص خودته`,
      image: Img.calendarPersonalAdvice,
      alt: Img.calendarPersonalAdviceAlt,
      md: 7,
      sm: 6,
      orderSM: 5,
      orderXS: 6,
      orderMD: 5,
      mdText: 5,
      smText: 6,
      orderSMText: 6,
      orderXSText: 5,
      orderMDText: 6,
      Style: "left",
    },
    {
      id: 4,
      labelText: `اگه دیرتر یا زودتر پریود بشی، می‌تونی ثبتش کنی`,
      note: `اگه پریودت نامنظمه، یا بهر دلیلی زمان پریودت جابجا بشه می‌تونی براحتی تو اپ ثبتش کنی`,
      image: Img.calendarSoonerPeriod,
      alt: Img.calendarSoonerPeriodAlt,
      md: 7,
      sm: 6,
      orderSM: 8,
      orderXS: 8,
      orderMD: 8,
      mdText: 5,
      smText: 6,
      orderSMText: 7,
      orderXSText: 7,
      orderMDText: 7,
      Style: "right",
    },
  ];

  const calender = [
    {
      id: 1,
      label: "روزهای باروری",
      text: "بر اساس تاریخ پریودت ، روزهای باروریت رو پیش‌بینی میکنیم تا احتمال بارداری در روزهای مختلف برات مشخص بشه",
      image: Img.calendarDaysPregnancy,
      alt: Img.calendarDaysPregnancyAlt,
      color: "#01B1A4",
    },
    {
      id: 2,
      label: "روز‌های پریود",
      text: "بر اساس اطلاعاتی که به ما دادی روزهای پریودت رو بهت نمایش میدیم و بهت میگیم که تو روزهای مختلف پریود چه کارهایی انجام بدی",
      image: Img.calendarPerioDay,
      alt: Img.calendarPerioDayAlt,
      color: "#C32F91",
    },
    {
      id: 3,
      label: "روز‌های پی ام اس",
      text: "ممکنه قبل از پریود حالت زیاد میزون نباشه، ما روزهای pms رو برات پیش‌بینی میکنیم و بهت میگیم که برای بهتر شدن حالت چکار کنی",
      image: Img.calendarPMS,
      alt: Img.calendarPMSAlt,
      color: "#7F47BF",
    },
  ];

  const titleSlider = "چه کار کنیم که ایمپو پریودمون رو دقیق پیش‌بینی کنه؟";
  const titleDescription =
    "برای پیش‌بینی دقیق پریودت، لازمه که اطلاعات اولیه ای که به ایمپو میدی ، درست و دقیق باشه";
  const btn1 = "هنوز ایمپو رو نصب نکردم";
  const btn2 = "کاربر ایمپو هستم";

  return (
    <>
      <Head>
        <title>تقویم قاعدگی در ایمپو</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AppbarMain darkmode={true} />
      <article className={styles.styleArticle}>
        <Hidden smDown>
          <section className={styles.calendarHero}>
            <div className={styles.contentHero}>
              <h1>ایمپو یک تقویم ساده قاعدگی <span>نیست!</span></h1>
              <p>ایمپو نه تنها یک تقویم هوشمند برای قاعدگی، PMS و تخمک گذاریه بلکه <div></div>
                مثل یک دوست صمیمی همراهته تا بیشتر مراقب خودت باشی و خودتو <div></div> اونجوری که هستی بپذیری</p>
            </div>
            <div className={styles.mockupHero}>
              <Image
                alt="heroCalen"
                src={heroCalen}
                className={styles.imageHero}
              />
            </div>
          </section>

          <div className={styles.flex_Calendar}>
            {calender.map((item, index) => (
              <div className={styles.flexCalender_flex} key={index}>
                <div className={styles.textAlignCenterCalendar}>
                  <Image
                    alt={item.alt}
                    src={item.image}
                    className={styles.ImageStyleHeaderCalender}

                  />
                </div>
                <Typography
                  type="headline"
                  size="medium"
                  className={`${styles.textAlignCenterCalendar} ${styles.styletextHeaderCalendar}`}
                  style={{ color: item.color }}
                >
                  {item.label}
                </Typography>
                <Typography
                  type="body"
                  size="large"
                  className={styles.textAlignCenterCalendar}
                >
                  {item.text}
                </Typography>
              </div>
            ))}
          </div>
          <Download />
        </Hidden>

        <Hidden smUp>
          <div >
            {/* <Divider className={styles.Dividerstyle2} /> */}
            <section className={styles.calendarHero}>
              <div className={styles.contentHero}>
                <h1>ایمپو یک تقویم ساده قاعدگی <span>نیست!</span></h1>
                <p>
                  ایمپو نه تنها یک تقویم هوشمند برای قاعدگی،<div></div>
                  PMS و تخمک‌گذاری بلکه مثل یک دوست صمیمی <div></div> همراهته تا بیشتر مراقب خودت باشی<div></div>
                  و خودتو اونجوری که هستی بپذیری
                </p>
              </div>
              <div className={styles.mockupHero}>
                <Image
                  alt="heroCalen"
                  src={heroCalen}
                  className={styles.imageHero}
                />
              </div>
            </section>
            <Divider className={styles.Dividerstyle1} />
            <Swiper
              pagination={{
                clickable: true,
              }}
              slidesPerView={1}
              centeredSlides={true}
              spaceBetween={20}
              modules={[Pagination]}
            >
              {calender.map((item, index) => (
                <SwiperSlide key={index}>
                  <Grid container>
                    <Grid
                      item
                      md={item.md}
                      sm={item.sm}
                      xs={12}
                      order={{
                        xs: item.orderXS,
                        sm: item.orderSM,
                        md: item.orderMD,
                      }}
                    >
                      <div
                        className={`${styles.textAlignCenterCalendar} ${styles.paddingTop}`}
                      >
                        <Image
                          alt="نظرات اپلیکیشن ایمپو"
                          src={item.image}
                          className={styles.ImageStyleHeader}
                        />
                      </div>
                    </Grid>

                    <Grid
                      item
                      md={item.mdText}
                      sm={item.smText}
                      xs={item.xsText}
                      order={{
                        xs: item.orderXSText,
                        sm: item.orderSMText,
                        md: item.orderMDText,
                      }}
                    >
                      <div className={styles.flexSliderSwiperCare}>
                        <div>
                          <Typography
                            type="title"
                            size="large"
                            className={`${styles.styletextinslider} ${styles.textAlignCenterCalendar} ${styles.styletextHeaderCalendar}`}
                            style={{ color: item.color }}
                          >
                            {item.label}
                          </Typography>
                          <Typography
                            type="body"
                            size="medium"
                            className={`${styles.textSliderTextCare} ${styles.textAlignCenterCalendar} ${styles.PaddingTopCare} `}
                          >
                            {item.text}
                          </Typography>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                </SwiperSlide>
              ))}
            </Swiper>
            <Divider className={styles.Dividerstyle1} />
          </div>
          <Download />
        </Hidden>
        <BoxZigzager boxZigZag={boxZigZag} />
      </article>
      <section className={`${styles.videoAparat} container`}>
        <div className={styles.contentVideo}>
          <h2>
            <span> پیش‌بینی پریود </span>
            با اپلیکیشن ایمپو
          </h2>
          <Hidden mdDown>
            <p>
              برای پیش‌بینی دقیق تاریخ پریود، دوره PMS و دوره باروری
              لازمه که اطلاعاتت رو بصورت درست و دقیق وارد کنی تا
              اپلیکیشن ایمپو بتونه بر اساس اطلاعات درست، چرخه
              قاعدگیت رو محاسبه کنه.  برای اطلاع از جزییات و پیش‌بینی
              دقیق تاریخ پریود حتما ویدئو رو ببین.
            </p>
          </Hidden>
          <Hidden mdUp>
            <div className={styles.frameTxt}>
              <p>
                برای پیش‌بینی دقیق تاریخ پریود، دوره PMS و دوره باروری
              </p>
              <p>
                لازمه که اطلاعاتت رو بصورت درست و دقیق وارد کنی تا
              </p>
              <p>
                اپلیکیشن ایمپو بتونه بر اساس اطلاعات درست، چرخه
              </p>
              <p>
                قاعدگیت رو محاسبه کنه.  برای اطلاع از جزییات و پیش‌بینی
              </p>
              <p>
                دقیق تاریخ پریود حتما ویدئو رو ببین.

              </p>
            </div>
          </Hidden>

        </div>
        <div className={styles.loadingBox}>
          <Image className={!isPlay ? "showIframe" : "notShowIframe"} src={horizontal} alt="image" onClick={() => setIsPlay(true)} />
          <div className={isPlay ? "showIframe" : "notShowIframe"} >
            <video id="videoMarket" controls>
              <source src="https://s3.ir-tbz-sh1.arvanstorage.ir/impo/Marketing/b4a4bd093585ea18a0b804f4d005b07d55888020-1080p.mp4?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=4876f63c-65d6-4de7-b11f-7e09df5dbbf0%2F20231127%2Fir-tbz-sh1%2Fs3%2Faws4_request&X-Amz-Date=20231127T045803Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&versionId=&X-Amz-Signature=9ce9f416299501ec5fd384487e3a8555a6c9ca900a8903324e64b7d4c27da0d0" type="video/mp4" />
            </video>
          </div>
        </div>
      </section >
      <Slider
        sliderItemNumber={sliderItemNumber}
        sliderItemCode={sliderItemCode}
        titleSlider={titleSlider}
        titleDescription={titleDescription}
        btn1={btn1}
        btn2={btn2}
      />
      <span id='download'></span>

      <Download />
      <Footer />
    </>
  );
}
