import Head from "next/head";
import { useEffect } from "react";
import style from "@/styles/downloader.module.css"
import { Button } from "@mui/material";
import Link from "next/link";

export default function downloadWoman() {
    useEffect(() => {
        window.location.href = 'https://impo.app/app/Impo_4.9.12_118_direct.apk';
        const interval = setInterval(
            () => {
                window.location.href = window.location.origin
            },
            20000,
        );

        return () => clearInterval(interval);
    }, [])

    return (
        <>
            <Head>
                <title>دانلود</title>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
                <meta name="robots" content="noindex,nofollow" />
            </Head>
            <div className={style.downloader}>
                <p>
                    در حال دانلود اپلیکیشن ...
                </p>
                <Link href="/">
                    <Button aria-label="btnName" size="medium">بازگشت به صفحه اصلی</Button>
                </Link>
            </div>
        </>

    )
}