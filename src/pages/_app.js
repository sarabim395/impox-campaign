import { ThemeProvider as MuiThemeProvider } from '@mui/material/styles';
import { SnackbarProvider } from 'notistack';
import Layout from '@/components/Layout';
import { lightTheme } from '@/themes/index';
import '../styles/globals.css';
import YekanBakh from '@/components/fonts/index'

export default function App({ Component, pageProps }) {

  return <main className={YekanBakh.className}>
    <MuiThemeProvider theme={lightTheme}>
      <SnackbarProvider maxSnack={3} dense anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </SnackbarProvider>
    </MuiThemeProvider>
  </main>
}

export async function getServerSideProps({ res }) {

  res.setHeader(
    'Cache-Control',
    'public, max-age=31536000, immutable',
  );

  return {
    props: {},
  };
}