import styles from "@/styles/article.module.css";
import Head from "next/head";
import Download from "@/components/download";
import Footer from "@/components/footer";
import Typography from "@/shared/Typography";
import Appbar from "@/components/appbar";
import { Grid, Button } from "@mui/material";
import Search from "@/components/search";
import Article from "@/components/article";
import Breadcrumb from "@/components/breadcrumb";
import Image from 'next/image';
import logoFooter from "@/assets/images/logoFooter.svg";
import Link from "next/link";
import myket from '@/assets/images/myket.svg';
import direct from '@/assets/images/direct.svg';
import webapp from '@/assets/images/webapp.svg';
import mockapDownload from '@/assets/images/mockapDownload.webp';
import axios from '@/utils/axios';
import { useEffect, useState } from "react";
import { useRouter } from 'next/router';
import Skeleton from '@mui/material/Skeleton';
import { v4 as uuidv4 } from 'uuid';
import { Pagination, PaginationItem } from '@mui/material';
import { TroubleshootSharp } from "@mui/icons-material";
import { toFarsiNumber } from "@/utils";
export default function article() {
  const router = useRouter();
  const [categoryInfo, setCategoryInfo] = useState({ catId: "", catName: "" })
  const [articles, setArticleByCategory] = useState([])
  const [categoryList, setCategory] = useState(undefined);
  const [loading, setLoading] = useState(true);
  const [loadingCategory, setLoadingCategory] = useState(true);
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(1);
  const skeleton = ["", "", "", ""];
  const skeletonCat = ["", "", "", "", "", "", ""];
  const fetchArticleList = async () => {
    const categoryInfo = { catId: localStorage?.getItem("categoryId"), catName: localStorage?.getItem("categoryName") };
    const response = await axios.get(`article/articles/category/${categoryInfo.catId}/${page}/10`)
    setArticleByCategory(response.data.articles)
    setCount(Math.ceil(response.data.totalCount / 10))
    setLoading(false)
  }

  const fetchCategoryList = async () => {
    const response = await axios.get('article/category/1/500')
    setCategory(response?.data?.categories)
    setLoadingCategory(false)
  }
  const redirectCategory = (id, title, slug) => {
    localStorage.removeItem("categoryId")
    localStorage.removeItem("categoryName")
    localStorage.setItem("categoryId", id)
    localStorage.setItem("categoryName", title)
    router.replace(`/category/${slug}`);
    setCategoryInfo({ catId: localStorage?.getItem("categoryId"), catName: localStorage?.getItem("categoryName") })
    fetchArticleList()
  }
  
  const handleChange = (event, value) => {
    setLoading(TroubleshootSharp)
    setPage(value);
  };

  const setCanonical = () => {
    if (categoryInfo.catName == "پریود و قاعدگی") {
      return "https://impo.app/menstrual-cycle/"
    } else if (categoryInfo.catName == "روان‌شناسی") {
      return "https://impo.app/what-is-psychology/"
    } else if (categoryInfo.catName == "سبک زندگی") {
      return "https://impo.app/healthy-lifestyle/"
    }
  }
  useEffect(() => {
    setCategoryInfo({ catId: localStorage?.getItem("categoryId"), catName: localStorage?.getItem("categoryName") })
    fetchCategoryList()
    setCanonical()
  }, [])

  useEffect(() => {
    window.scrollTo({ top: "0", behavior: "smooth" })
    fetchArticleList()
  }, [page])

  return (
    <>
      <Head>
        <title>مقالات {categoryInfo.catName}</title>
        <meta name="description" content="آرشیو دسته بندی" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="canonical" href={setCanonical()} />
      </Head>
      <Appbar />
      <main className={styles.totalDistance}>
        <Grid container spacing={4} className={styles.containerUi}>
          <Grid item md={9} sm={12} className={styles.borderContaner}>
            <Grid container className={styles.paddingTopArticle}>
              <Grid item md={12}>
                <Breadcrumb categoryName={categoryInfo.catName} />
                <Typography
                  type="display"
                  size="large"
                  className={styles.fontSizeTitleArticle}
                >
                  {`مقالات مربوط به ${categoryInfo.catName}`}
                </Typography>
              </Grid>
            </Grid>
            {loading ?
              skeleton.map(() => {
                return (
                  <div className={styles.skeleton} key={uuidv4()}>
                    <Skeleton animation="wave" variant="rectangular" width={300} height={150} sx={{ bgcolor: '#F9F9F9' }} />
                    <div className={styles.texts}>
                      <Skeleton variant="rectangular" animation="wave" width={400} height={30} sx={{ bgcolor: '#F9F9F9' }} />
                      <Skeleton variant="rectangular" animation="wave" width={800} height={15} sx={{ bgcolor: '#F9F9F9' }} />
                      <Skeleton variant="rectangular" animation="wave" width={800} height={15} sx={{ bgcolor: '#F9F9F9' }} />
                      <br />
                      <Skeleton variant="rectangular" animation="wave" width={100} height={30} sx={{ bgcolor: '#F9F9F9' }} />
                    </div>
                  </div>
                )
              }) : <Article ArticleContainer={articles} />
            }
            <div className={styles.pagination}>
              {count ? <Pagination count={count} onChange={handleChange} page={page} color="primary" size="large" renderItem={(item) => (
                <PaginationItem
                  {...item}
                  page={toFarsiNumber(item.page)}
                />
              )} /> : undefined}
            </div>
          </Grid>
          <Grid
            item
            md={3}
            sm={12}
            className={`${styles.paddingTopArticle} ${styles.paddingRightArticle} `}
          >
            <aside className={styles.AsideArticel}>
              <Search />
              <section style={{ paddingTop: "50px" }}>
                <Typography>سایر دسته بندی ها</Typography>
                <div style={{ display: "flex", flexWrap: "wrap" }}>
                  {loadingCategory ?
                    skeletonCat.map(() => {
                      return (
                        <Skeleton key={uuidv4()} animation="wave" variant="rectangular" width={123} height={34} sx={{ bgcolor: '#F9F9F9' }} className={styles.skeletonCat} />
                      )
                    })
                    : categoryList?.map((item) => {
                      return (
                        <Button
                          aria-label="btnName"
                          onClick={() => { redirectCategory(item?.id, item?.title, item?.slug) }}
                          key={uuidv4()}
                          variant="outlined"
                          className={styles.BtnCategoriArticel}
                        >
                          {item.title}
                        </Button>
                      );
                    })}
                </div>
              </section>
              <section className={styles.downloadApp}>
                <Typography
                  size="large"
                  type="body"
                  color="#1C1B1E"
                  component="span"
                >
                  هنوز ایمپو رو نصب نکردی؟ الان وقتشه...
                </Typography>
                <div className={styles.frame}>
                  <div>
                    <Image width="auto" height="auto" className={styles.logoType} src={logoFooter} alt="لوگو" />
                  </div>
                  <Typography
                    size="large"
                    type="body"
                    color="#1C1B1E"
                    component="span"
                  >
                    بزرگترین پلتفرم سلامت حوزه زنان
                  </Typography>
                  <div className={styles.links}>
                    <Link href={"http://yun.ir/myket-woman"} aria-label="link"><Image width="auto" height="auto" src={myket} alt="مایکت " className={styles.DownloadBtnImage} />مایکت</Link>
                    <a href={"http://yun.ir/direct-woman"} download="impo.apk" aria-label="link"><Image width="auto" height="auto" src={direct} alt="دانلود مستقیم " className={styles.DownloadBtnImage} /> دانلود مستقیم </a>
                    <Link href={"http://yun.ir/web-woman"} aria-label="link"><Image width="auto" height="auto" src={webapp} alt=" وب اپلیکیشن " className={styles.DownloadBtnImage} /> وب اپ ( کاربران IOS ) </Link>
                  </div>
                  <Image width="auto" height="auto" className={styles.mockap} src={mockapDownload} alt="دانلود اپ" />
                </div>
              </section>
            </aside>
          </Grid >
        </Grid >
        <span id='download'></span>
      </main >
      <Download />
      <Footer />
    </>
  );
}