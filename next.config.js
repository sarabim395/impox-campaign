/** @type {import('next').NextConfig} */
const nextConfig = {
  pageExtensions: ['mdx', 'md', 'jsx', 'js', 'tsx', 'ts'],
  reactStrictMode: false,
  compress: true,
  images: {
    formats: ['image/jpeg', 'image/png', 'image/svg+xml', 'image/webp', 'image/avif'],
    remotePatterns: [
      {
        protocol: 'http',
        hostname: '185.252.28.158',
      },
    ],
  },
  typescript: {
    ignoreBuildErrors: true,
  },
  experimental: {
    typedRoutes: true,
  },
}

module.exports = nextConfig
